package httprequestsender;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpRequestSender {

    private static CloseableHttpClient httpClient;
    ObjectMapper mapper = new ObjectMapper();
    private static final Logger LOGGER = LogManager.getLogger(HttpRequestSender.class);


    public HttpRequestSender() {
        httpClient = HttpClientBuilder.create().build();
    }

    public CloseableHttpResponse makeHttpCall(String url){
        try {
            return httpClient.execute(gethttpRequest(url));
        } catch (Exception e) {
            LOGGER.error("Execption while making HTTP call ", e);
            throw new RuntimeException(e);
        }
    }

    public CloseableHttpResponse makeHttpCallWithUserNamePassword(String url,String userName, String password){
        try {
            return HttpClientFactory.getHttpClient(userName, password).execute(gethttpRequest(url));
        } catch (Exception e) {
            LOGGER.error("Execption while making HTTP call ", e);
            throw new RuntimeException(e);
        }
    }

    private HttpUriRequest gethttpRequest(String url) {
        HttpGet httpGet = new HttpGet(url);
        return httpGet;
    }
}
