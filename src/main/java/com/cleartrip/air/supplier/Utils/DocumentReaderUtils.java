package com.cleartrip.air.supplier.Utils;

import com.cleartrip.air.supplier.Ctxmlreader;
import com.cleartrip.air.supplier.customexceptions.XMLParsingException;
import com.cleartrip.air.supplier.enums.ErrorCode;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Map;

public class DocumentReaderUtils {

    public final static Logger LOGGER = LogManager.getLogger(DocumentReaderUtils.class);

    public static Document readDocumentForStep(Map<String, String> stepResponseBodyMap, String stepName){
        Document document ;
        try {
            document = DOMHelper.createDocument(stepResponseBodyMap.get(stepName));
        } catch (ParserConfigurationException e) {
            throw new XMLParsingException(e, ErrorCode.PARSING_ERROR, "Error while creating document from " + stepName);
        } catch (IOException e) {
            throw new XMLParsingException(e, ErrorCode.PARSING_ERROR, "Error while creating document from " + stepName);
        } catch (SAXException e) {
            throw new XMLParsingException(e, ErrorCode.PARSING_ERROR, "Error while creating document from " + stepName);
        }
        return document;
    }

    public static Document readDocumentForApiResponseBody(String apiResponseBody){
        Document document ;
        try {
            document = DOMHelper.createDocument(apiResponseBody);
        } catch (ParserConfigurationException e) {
            throw new XMLParsingException(e, ErrorCode.PARSING_ERROR, "Error while creating document from " + apiResponseBody);
        } catch (IOException e) {
            throw new XMLParsingException(e, ErrorCode.PARSING_ERROR, "Error while creating document from " + apiResponseBody);
        } catch (SAXException e) {
            throw new XMLParsingException(e, ErrorCode.PARSING_ERROR, "Error while creating document from " + apiResponseBody);
        }
        return document;
    }
}
