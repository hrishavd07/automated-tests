package com.cleartrip.air.supplier.Utils;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
    public static String prepareKey(String name, String from, String to) {
        return name + "_" + from + "_" + to;
    }

    public static String prepareKey(String name) {
        return name;
    }

    public static Date convertStringToDate(String strDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date convertStringToDateTimeWithoutColon(String strDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hhmmss");
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date convertToDate(String strDate) {
        DateFormat dobInDate = new SimpleDateFormat("ddMMyy'T'hhmmss");
        String time = strDate.split("T")[1];
        if (StringUtils.isNotEmpty(strDate)) {
            try {
                Date parse = dobInDate.parse(strDate);
                strDate = (parse.getYear() + 1900 + "-" + (parse.getMonth() + 1) + "-" + parse.getDate());
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
        return convertStringToDateTimeWithoutColon(strDate + 'T' + time);
    }

    public static Date convertStringToDateNavitaireFormat(String strDate){
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy-hh:mm");
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;

    }


    public static ZonedDateTime getZonedDateTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return ((GregorianCalendar) calendar).toZonedDateTime();
    }

    public static String convertDateToString(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        String strDate = dateFormat.format(date);
        return strDate;
    }

}
