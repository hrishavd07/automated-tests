package com.cleartrip.air.supplier.Utils;

import com.cleartrip.air.supplier.enums.AirSupplier;

public class TestUtils {
    public static String getSupplierNameFromCode(String airline) {
        String carrierName = null;
        for (AirSupplier value : AirSupplier.values()) {
            if (value.getCarrierName().equalsIgnoreCase(airline)) {
                carrierName = value.getDisplayName();
            }
        }
        return carrierName;
    }

    public static String getAirlineCodeFromSupplier(String airline){
        String airlineCode = null;
        for (AirSupplier value : AirSupplier.values()) {
            if (value.getDisplayName().equalsIgnoreCase(airline)) {
                airlineCode = value.getCarrierName();
            }
        }
        return airlineCode;
    }
}
