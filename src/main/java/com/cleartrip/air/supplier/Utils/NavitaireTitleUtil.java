package com.cleartrip.air.supplier.Utils;

import com.cleartrip.air.supplier.enums.Gender;
import com.cleartrip.air.supplier.enums.PassengerType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class NavitaireTitleUtil {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static final Map<String, Map<String, Map<String, String>>> airlineTitleMap = new HashMap<>();
    private static final String titleJson ="{\n" +
            "  \"default\": {\n" +
            "    \"ADT\": {\n" +
            "      \"MALE\": \"MR\",\n" +
            "      \"FEMALE\": \"MRS\"\n" +
            "    },\n" +
            "    \"CHD\": {\n" +
            "      \"MALE\": \"MSTR\",\n" +
            "      \"FEMALE\": \"MISS\"\n" +
            "    },\n" +
            "    \"INF\": {\n" +
            "      \"MALE\": \"MSTR\",\n" +
            "      \"FEMALE\": \"MISS\"\n" +
            "    }\n" +
            "  }\n" +
            "}";
    static {
        try {
            airlineTitleMap.putAll(mapper.readValue(titleJson, Map.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getTitle(String airline, Gender gender, PassengerType  travellerType){
        if(airlineTitleMap.containsKey(airline)){
            return airlineTitleMap.get(airline).get(travellerType.toString()).get(gender.name());
        }
        return getTitle(gender, travellerType);
    }

    public static String getTitle(Gender gender,PassengerType travellerType) {
        return airlineTitleMap.get("default").get(travellerType.toString()).get(gender.name());
    }

}

