package com.cleartrip.air.supplier.validatordata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Created by kcrob.in on 29/06/18.
 */
public class Airport {
    private static final String DEFAULT_TERMINAL="DEFAULT";

    public final String airportCode;
    public final String terminal;

    public Airport(@JsonProperty("airportCode")String airportCode) {
        this.airportCode = airportCode;
        this.terminal = DEFAULT_TERMINAL;
    }

    @JsonCreator
    public Airport(@JsonProperty("airportCode")String airportCode, @JsonProperty("terminal")String terminal) {
        this.airportCode = airportCode;
        this.terminal = terminal;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public String getTerminal() {
        return terminal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airport that = (Airport) o;
        return Objects.equals(airportCode, that.airportCode) &&
                Objects.equals(terminal, that.terminal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(airportCode, terminal);
    }
}