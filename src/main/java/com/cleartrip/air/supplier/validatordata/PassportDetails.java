package com.cleartrip.air.supplier.validatordata;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PassportDetails {
    String passportNumber;
    Date passportExpiryDate;
    String passportIssuingCountry;

    public PassportDetails(String passportNumber, String passportExpiryDate, String passportIssuingCountry) {
        this.passportNumber = passportNumber;
        this.passportExpiryDate =getDate(passportExpiryDate);
        this.passportIssuingCountry = passportIssuingCountry;
    }

    private Date getDate(String passportExpiryDate) {
        if (StringUtils.isEmpty(passportExpiryDate)) {
            /*LOGGER.error("As dob string is empty hence returning new date");*/
            return new Date();
        }
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(passportExpiryDate);
        } catch (ParseException e) {
            /*LOGGER.error("Error while parsing dob hence returning new date", e);*/
            return new Date();
        }
    }

    @Override
    public String toString() {
        return "PassportDetails{" +
                "passportNumber='" + passportNumber + '\'' +
                ", passportExpiryDate='" + passportExpiryDate + '\'' +
                ", passportIssuingCountry='" + passportIssuingCountry + '\'' +
                '}';
    }
}
