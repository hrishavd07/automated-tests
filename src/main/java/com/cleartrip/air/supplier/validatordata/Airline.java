package com.cleartrip.air.supplier.validatordata;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Optional;

public class Airline {
    private final String airlineCode;
    private final String flightNumber;
    private final Optional<String> name;

    public Airline(String airlineCode, String flightNumber) {
        this.airlineCode = airlineCode;
        this.flightNumber = flightNumber;
        this.name = Optional.empty();
    }

    public Airline(@JsonProperty("airlineCode") String airlineCode, @JsonProperty("flightNumber") String flightNumber, @JsonProperty("name")String name) {
        this.airlineCode = airlineCode;
        this.name = Optional.ofNullable(name);
        this.flightNumber = flightNumber;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public Optional<String> getName() {
        return name;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    @Override
    public String toString() {
        return "Airline{" +
                "airlineCode='" + airlineCode + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                ", name=" + name +
                '}';
    }


}


