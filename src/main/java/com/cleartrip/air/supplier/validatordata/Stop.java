package com.cleartrip.air.supplier.validatordata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class Stop {

    private final long duration;
    private final Airport airport;
    private final ZonedDateTime arrivalDate;
    private final ZonedDateTime departDate;


    @JsonCreator
    public Stop(@JsonProperty("airport") Airport airport, @JsonProperty("arrivalDate") ZonedDateTime arrivalDate, @JsonProperty("departDate") ZonedDateTime departDate) {
        this.airport = airport;
        this.arrivalDate = arrivalDate;
        this.departDate = departDate;
        this.duration = ChronoUnit.MINUTES.between(arrivalDate, departDate);
    }

    public Airport getAirport() {
        return airport;
    }

    public ZonedDateTime getArrivalDate() {
        return arrivalDate;
    }

    public ZonedDateTime getDepartDate() {
        return departDate;
    }

    public long getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return "Stop{" +
                "duration=" + duration +
                ", airport=" + airport +
                ", arrivalDate=" + arrivalDate +
                ", departDate=" + departDate +
                '}';
    }
}
