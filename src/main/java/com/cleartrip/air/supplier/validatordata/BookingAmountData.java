package com.cleartrip.air.supplier.validatordata;

public class BookingAmountData {

    double quotedAmount;
    double threshold;

    public double getQuotedAmount() {
        return quotedAmount;
    }

    public void setQuotedAmount(double quotedAmount) {
        this.quotedAmount = quotedAmount;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public BookingAmountData(double quotedAmount, double threshold) {
        this.quotedAmount = quotedAmount;
        this.threshold = threshold;
    }

    public String toString(){
        return "Quoted-Amount:"+quotedAmount+"Threshold:"+threshold;
    }
}

