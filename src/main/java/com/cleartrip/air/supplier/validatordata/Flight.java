package com.cleartrip.air.supplier.validatordata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.Optional;

public class Flight {
    public final String flightNumber;
    public final String airlineCode;
    public final Optional<String> aircraftType;
    private Optional<String> equipmentCode;

    @JsonCreator
    public Flight(@JsonProperty("flightNumber")String flightNumber, @JsonProperty("airlineCode")String airlineCode, @JsonProperty("aircraftType")String aircraftType, @JsonProperty("equipmentCode") String equipmentCode) {
        this.flightNumber = flightNumber.trim();
        this.airlineCode = airlineCode.trim();
        this.aircraftType = Optional.ofNullable(aircraftType);
        this.equipmentCode = Optional.ofNullable(equipmentCode);
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public Optional<String> getAircraftType() {
        return aircraftType;
    }

    public Optional<String> getEquipmentCode() {
        return equipmentCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return Objects.equals(flightNumber, flight.flightNumber) &&
                Objects.equals(airlineCode, flight.airlineCode) &&
                Objects.equals(aircraftType, flight.aircraftType) &&
                Objects.equals(equipmentCode, flight.equipmentCode);
    }

    public boolean equalsIgnoreEquipment(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return Objects.equals(flightNumber, flight.flightNumber) &&
                Objects.equals(airlineCode, flight.airlineCode);
    }



    @Override
    public int hashCode() {
        return Objects.hash(flightNumber, airlineCode, aircraftType, equipmentCode);
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightNumber='" + flightNumber + '\'' +
                ", airlineCode='" + airlineCode + '\'' +
                ", aircraftType='" + aircraftType + '\'' +
                ", equipmentCode='" + equipmentCode + '\'' +
                '}';
    }
}
