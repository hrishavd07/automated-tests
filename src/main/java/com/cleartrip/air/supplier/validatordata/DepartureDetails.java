package com.cleartrip.air.supplier.validatordata;

import com.cleartrip.air.supplier.validatordata.interfaces.DateConstants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

public class DepartureDetails {

    private final Airport departingAirport;

    @JsonFormat(pattern = DateConstants.FORMAT)
    private final ZonedDateTime departTime;

    public DepartureDetails(@JsonProperty("departingAirport")Airport departingAirport, @JsonProperty("departTime") @JsonFormat(pattern = DateConstants.FORMAT) ZonedDateTime departTime) {
        this.departingAirport = departingAirport;
        this.departTime = departTime;
    }

    public Airport getDepartingAirport() {
        return departingAirport;
    }

    public ZonedDateTime getDepartTime() {
        return departTime;
    }

    @Override
    public String toString() {
        return "DepartureDetails{" +
                "departingAirport=" + departingAirport +
                ", departTime=" + departTime +
                '}';
    }

    @JsonIgnore
    public boolean isScheduleChange(DepartureDetails other) {
        if(null == other) {
            return true;
        }
        return !other.getDepartTime().isEqual(departTime);
    }

//    public static void main(String[] args) throws IOException {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.registerModule(new GuavaModule());
//        mapper.registerModule(new Jdk8Module());
//        //mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
//        mapper.setTimeZone(TimeZone.getDefault());
//        mapper.setVisibility(mapper
//                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
//                .getSerializationConfig()
//                .getDefaultVisibilityChecker()
//                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
//                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
//                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
//                .withCreatorVisibility(JsonAutoDetect.Visibility.ANY));
//        DepartureDetails departureDetails = new DepartureDetails(new Airport("DEL","1"), new Date());
//        String data = "{\"departingAirport\":{\"airportCode\":\"DEL\",\"terminal\":\"1\"},\"departTime\":1545915357241}";
//        System.out.println(mapper.writeValueAsString(departureDetails));
//        final DepartureDetails departureDetails1 = mapper.readValue(data, DepartureDetails.class);
//        System.out.println(departureDetails1);
//    }
}
