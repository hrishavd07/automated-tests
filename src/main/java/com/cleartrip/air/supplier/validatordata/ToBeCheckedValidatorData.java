package com.cleartrip.air.supplier.validatordata;

import com.cleartrip.air.supplier.testframework.TestResult;

public class ToBeCheckedValidatorData {
    private String name;
    private TestResult.Status valid;
    private String message;

    public String getName() {
        return name;
    }

    public TestResult.Status getValidStatus() {
        return valid;
    }

    public String getMessage() {
        return message;
    }

    public ToBeCheckedValidatorData(String name, TestResult.Status valid, String message){
        this.name = name;
        this.valid = valid;
        this.message = message;
    }
}
