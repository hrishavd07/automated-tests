package com.cleartrip.air.supplier.validatordata;

import com.cleartrip.air.supplier.enums.CustomerDetailsParams;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.javers.core.diff.ListCompareAlgorithm.LEVENSHTEIN_DISTANCE;

public class CustomerData {

    String emailId;
    String firstName;
    String lastName;
    String mobileNumber;
    String title;
    private final List<String> parameters;

    public CustomerData(String emailId, String firstName, String lastName, String mobileNumber, String title) {
        this.emailId = emailId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        this.title = title;
        parameters = getListOfParameters();
    }

    private List<String> getListOfParameters() {
        return Arrays.stream(CustomerDetailsParams.values()).map(r -> r.getDisplayName()).collect(Collectors.toList());
    }

    public List<String> compare(Object o, List<String> ignorableList) {
        List<String> differedParameter;
        Javers javers = JaversBuilder.javers().withListCompareAlgorithm(LEVENSHTEIN_DISTANCE)
                .build();
        Diff difference = javers.compare(this, o);
        differedParameter = parameters.stream()
                .filter(p -> !ignorableList.contains(p))
                .filter(p -> !difference.getPropertyChanges(p).isEmpty())
                .collect(Collectors.toList());
        return differedParameter;
    }

    @Override
    public String toString() {
        return "Title:" + title + "First-Name:" + firstName + "Last-Name:" + lastName + "email-id:" + emailId;
    }

}
