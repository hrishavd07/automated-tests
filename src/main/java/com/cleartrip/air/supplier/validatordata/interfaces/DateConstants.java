package com.cleartrip.air.supplier.validatordata.interfaces;

public interface DateConstants {
    String FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
}
