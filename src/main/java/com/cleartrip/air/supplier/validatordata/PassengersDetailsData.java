package com.cleartrip.air.supplier.validatordata;

import com.cleartrip.air.supplier.enums.Gender;
import com.cleartrip.air.supplier.enums.PassengerDetailsParams;
import com.cleartrip.air.supplier.enums.PassengerType;
import org.apache.commons.lang3.StringUtils;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.javers.core.diff.ListCompareAlgorithm.LEVENSHTEIN_DISTANCE;

public class PassengersDetailsData {

    /*private final Logger LOGGER = LogManager.getLogger(PassengersDetailsData.class);*/
    private String firstName;
    private String lastName;
    private String title;
    private Gender gender;
    private PassengerType travellerType;
    private Date dob;
    private Optional<PassportDetails> passport;
    private final List<String> parameters;

    public PassengersDetailsData(String firstName, String lastName, PassengerType travellerType, String dobStr, PassportDetails passportDetails, String title, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.travellerType = travellerType;
        this.dob = getDOB(dobStr);
        this.title = title;
        this.passport = Optional.ofNullable(passportDetails);
        this.gender = gender;
        parameters = getListOfParameters();
    }

    public PassengersDetailsData(String firstName, String lastName, PassengerType travellerType, Date dob, PassportDetails passportDetails, String title, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.travellerType = travellerType;
        this.dob = dob;
        this.title = title;
        this.passport = Optional.ofNullable(passportDetails);
        this.gender = gender;
        parameters = getListOfParameters();
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getTitle() {
        return title;
    }

    public Gender getGender() {
        return gender;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDob() {
        return dob;
    }

    public Optional<PassportDetails> getPassport() {
        return passport;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public PassengerType getTravellerType() {
        return travellerType;
    }

    private List<String> getListOfParameters() {
        return Arrays.stream(PassengerDetailsParams.values()).map(r -> r.getDisplayName()).collect(Collectors.toList());
    }

    private Date getDOB(String dobStr) {
        if (StringUtils.isEmpty(dobStr)) {
            /*LOGGER.error("As dob string is empty hence returning new date");*/
            return new Date();
        }
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(dobStr);
        } catch (ParseException e) {
            /*LOGGER.error("Error while parsing dob hence returning new date", e);*/
            return new Date();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PassengersDetailsData that = (PassengersDetailsData) o;
        return firstName.equals(that.firstName) &&
                lastName.equals(that.lastName) &&
                travellerType == that.travellerType &&
                dob.equals(that.dob) &&
                title.equalsIgnoreCase(that.title) &&
                passport.equals(that.passport);
    }

    public List<String> compare(Object o, List<String> ignorableList) {
        List<String> differedParameter;
        Javers javers = JaversBuilder.javers().withListCompareAlgorithm(LEVENSHTEIN_DISTANCE)
                .build();
        Diff difference = javers.compare(this, o);
        differedParameter = parameters.stream()
                .filter(p -> !ignorableList.contains(p))
                .filter(p -> !difference.getPropertyChanges(p).isEmpty())
                .collect(Collectors.toList());
        return differedParameter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, travellerType);
    }

    @Override
    public String toString() {
        return "\n" + "Title = " + title + " First-Name: " + firstName + " Last-Name: " + lastName + " Traveller-Type: " + getTravellerType() + " DOB: " + dob + "Gender: " + gender + "PassportDetails:" + passport + "\n";
    }
}
