package com.cleartrip.air.supplier.validatordata;

import com.cleartrip.air.supplier.validatordata.interfaces.DateConstants;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;

public class ArrivalDetails {

    private final Airport arrivalAirport;

    @JsonFormat(pattern = DateConstants.FORMAT)
    private final ZonedDateTime arrivalTime;

    public ArrivalDetails(@JsonProperty("arrivalAirport")Airport arrivalAirport, @JsonProperty("arrivalTime") ZonedDateTime arrivalTime) {
        this.arrivalAirport = arrivalAirport;
        this.arrivalTime = arrivalTime;
    }

    public Airport getArrivalAirport() {
        return arrivalAirport;
    }

    public ZonedDateTime getArrivalTime() {
        return arrivalTime;
    }

    @Override
    public String toString() {
        return "ArrivalDetails{" +
                "arrivalAirport=" + arrivalAirport +
                ", arrivalTime=" + arrivalTime +
                '}';
    }

    @JsonIgnore
    public boolean isScheduleChange(ArrivalDetails other) {
        if(null == other) {
            return true;
        }
        return !other.getArrivalTime().isEqual(arrivalTime);
    }

}
