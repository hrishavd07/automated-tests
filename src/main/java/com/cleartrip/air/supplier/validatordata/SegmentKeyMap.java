package com.cleartrip.air.supplier.validatordata;

import java.util.Objects;

public class SegmentKeyMap {

    String direction;
    String from ;
    String to;
    String supplier;
    int index;
    int directionWiseIndex;

    public SegmentKeyMap(String direction, String from, String to, String supplier, int index, int directionWiseIndex) {
        this.direction = direction;
        this.from = from;
        this.to = to;
        this.supplier = supplier;
        this.index = index;
        this.directionWiseIndex = directionWiseIndex;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getDirectionWiseIndex() {
        return directionWiseIndex;
    }

    public void setDirectionWiseIndex(int directionWiseIndex) {
        this.directionWiseIndex = directionWiseIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SegmentKeyMap that = (SegmentKeyMap) o;
        return index == that.index &&
                directionWiseIndex == that.directionWiseIndex &&
                direction.equals(that.direction) &&
                from.equals(that.from) &&
                to.equals(that.to) &&
                supplier.equals(that.supplier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(direction, from, to, supplier, index, directionWiseIndex);
    }

    public String getKey() {
        return direction + "_" + directionWiseIndex + "|" + supplier + "|" + from + "_" + to;

    }
}
