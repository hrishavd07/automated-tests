package com.cleartrip.air.supplier.validatordata;

import com.cleartrip.air.supplier.enums.FlightDetailsParams;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static org.javers.core.diff.ListCompareAlgorithm.LEVENSHTEIN_DISTANCE;

public class FlightSegment {

    private final long duration;
    private Flight flight;
    private DepartureDetails departureDetails;
    private ArrivalDetails arrivalDetails;
    private Airline operatingAirline;
    private Airline marketingAirline;
    private final Optional<List<Stop>> stops;
    private final Map<String,String> additionalProperties;
    private final List<String> parameters;


    public FlightSegment(@JsonProperty("flight") Flight flight, @JsonProperty("departureDetails") DepartureDetails departureDetails,
                         @JsonProperty("arrivalDetails") ArrivalDetails arrivalDetails, @JsonProperty("operatingAirline") Airline operatingAirline,
                         @JsonProperty("marketingAirline") Airline marketingAirline, @JsonProperty("stops") Optional<List<Stop>> stops,
                         @JsonProperty("additionalProperties") Map<String, String> additionalProperties) {
        this.stops = stops.map(s -> ImmutableList.copyOf(s));
        this.additionalProperties = additionalProperties != null ? ImmutableMap.copyOf(additionalProperties) : Collections.EMPTY_MAP;
        this.flight = flight;
        this.departureDetails = departureDetails;
        this.arrivalDetails = arrivalDetails;
        this.operatingAirline = operatingAirline;
        this.marketingAirline = marketingAirline;
        this.duration = ChronoUnit.MINUTES.between(departureDetails.getDepartTime(), arrivalDetails.getArrivalTime());
        parameters = getListOfParameters();
    }

    private List<String> getListOfParameters() {
        return Arrays.stream(FlightDetailsParams.values()).map(r -> r.getDisplayName()).collect(Collectors.toList());
    }

    public Flight getFlight() {
        return flight;
    }

    public DepartureDetails getDepartureDetails() {
        return departureDetails;
    }

    public ArrivalDetails getArrivalDetails() {
        return arrivalDetails;
    }

    public Airline getOperatingAirline() {
        return operatingAirline;
    }

    public Airline getMarketingAirline() {
        return marketingAirline;
    }

    public Optional<List<Stop>> getStops() {
        return stops;
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public long getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return "FlightSegment{" +
                "duration=" + duration +
                ", flight=" + flight +
                ", departureDetails=" + departureDetails +
                ", arrivalDetails=" + arrivalDetails +
                ", operatingAirline=" + operatingAirline +
                ", marketingAirline=" + marketingAirline +
                ", stops=" + stops +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    @JsonIgnore
    public ZonedDateTime getLastStopDepartDate() {
        List<Stop> stopList = this.getStops().orElse(Collections.emptyList());
        if(!stopList.isEmpty()) {
            return stopList.get(stopList.size()-1).getDepartDate();
        }
        return this.getDepartureDetails().getDepartTime();
    }

    @JsonIgnore
    public boolean isScheduleChange(FlightSegment other) {
        if (null == other) {
            return true;
        }
        return departureDetails.isScheduleChange(other.departureDetails) || arrivalDetails.isScheduleChange(other.arrivalDetails);
    }

    public List<String> compare(Object o) {
        List<String> differedParameter;
        Javers javers = JaversBuilder.javers().withListCompareAlgorithm(LEVENSHTEIN_DISTANCE)
                .build();
        Diff difference = javers.compare(this, o);
        differedParameter = parameters.stream()
                .filter(p -> !difference.getPropertyChanges(p).isEmpty())
                .collect(Collectors.toList());
        return differedParameter;
    }
}
