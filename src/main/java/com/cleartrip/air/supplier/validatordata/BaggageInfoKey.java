package com.cleartrip.air.supplier.validatordata;

import java.util.Objects;

public class BaggageInfoKey {
    String direction;
    String supplier;
    int paxIndex;

    public BaggageInfoKey(String direction, String supplier, int paxIndex) {
        this.direction = direction;
        this.supplier = supplier;
        this.paxIndex = paxIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaggageInfoKey that = (BaggageInfoKey) o;
        return paxIndex == that.paxIndex &&
                direction.equals(that.direction) &&
                supplier.equals(that.supplier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(direction, supplier, paxIndex);
    }

    public String getDirection() {
        return direction;
    }

    public String getSupplier() {
        return supplier;
    }

    public int getPaxIndex() {
        return paxIndex;
    }
}
