package com.cleartrip.air.supplier.validatordata;

import com.cleartrip.air.supplier.enums.PassengerType;

public class PaxDetails {
    private int count;
    private PassengerType passengerType;

    public PaxDetails(int count, PassengerType passengerType) {
        this.count = count;
        this.passengerType = passengerType;
    }

    public int getCount() {
        return count;
    }

    public PassengerType getPassengerType() {
        return passengerType;
    }
}
