package com.cleartrip.air.supplier.testcases;

import com.cleartrip.air.supplier.Ctxmlreader;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.testframework.TestResult;

public abstract class TestCase {

    /*enum Calls {
        PREPAYMENT, BOOK;
    };*/
    private final String name;
    public final Ctxmlreader ctXmlReader;

    public TestCase(String name, Ctxmlreader ctXmlReader) {
        this.name = name;
        this.ctXmlReader = ctXmlReader;
    }

    public abstract TestResult run(TestContext testContext);

    @Override
    public String toString() {
        return name;
    }

/*    private void getString(TestContext testContext) {

        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        Map<String, String> response;
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        for (Calls value : Calls.values()) {
            switch (value) {
                case PREPAYMENT:
                    response = statsResponse.getAirPrepaymentCallInAirCtCalls();
                    testContext.addAttribute(value.name(), httpReqResHandler.getResponseBody(response.get("itinerary"), "AIR_PREPAYMENT", "ct_req", response.get("tid")));
                    break;
                case BOOK:
                    response = statsResponse.getAirBookCallInAirCTCalls();
                    testContext.addAttribute(value.name(), httpReqResHandler.getResponseBody(response.get("itinerary"), "AIR_BOOK", "ct_req", response.get("tid")));
                    break;
            }
        }

    }*/

}
