package com.cleartrip.air.supplier.testcases;

import com.cleartrip.air.supplier.Ctxmlreader;
import com.cleartrip.air.supplier.Utils.TestUtils;
import com.cleartrip.air.supplier.testframework.DataExtractorsFactoryRegistry;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.testframework.TestResult;
import com.cleartrip.air.supplier.validatordata.BookingAmountData;
import com.cleartrip.air.supplier.validatordata.FlightSegment;
import com.cleartrip.air.supplier.validatordata.SegmentKeyMap;
import com.cleartrip.air.supplier.validatordata.ToBeCheckedValidatorData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import static com.cleartrip.air.supplier.testframework.TestResult.Status.FAILED;
import static com.cleartrip.air.supplier.testframework.TestResult.Status.SKIPPED;

public class BookingAmountValidationTestCase extends TestCase {

    private DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry;
    private final Logger LOGGER = LogManager.getLogger(BookingAmountValidationTestCase.class);

    public BookingAmountValidationTestCase(String name, DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry, Ctxmlreader ctxmlreader) {
        super(name, ctxmlreader);
        this.dataExtractorsFactoryRegistry = dataExtractorsFactoryRegistry;
    }

    //todo: keep methods according to purpose
    @Override
    public TestResult run(TestContext testContext) {
        LOGGER.error("entering booking amount validation testcase");
        // Journey direction wise , supplier wise list of Booking amounts from CTxml
        LinkedHashMap<String, LinkedHashMap<String, Double>> journeyAndSupplierwiseCtFare = getJourneyAndSupplierWiseCtFares(ctXmlReader.makeFlightSegments());
        Set<String> applicableSuppliersList =ctXmlReader.getApplicableSuppliers();
        //Supplier wise api responses according to API xmls
        Map<String, BookingAmountData> apiSuppliersBookingAmountData = getSuppliewiseApiBookingAmount(testContext, applicableSuppliersList);
        TestResult result = matchDetails(journeyAndSupplierwiseCtFare, apiSuppliersBookingAmountData, applicableSuppliersList);
        return result;
    }

    private LinkedHashMap<String, LinkedHashMap<String, Double>> getJourneyAndSupplierWiseCtFares(Map<SegmentKeyMap, FlightSegment> segmentedMap) {
        // segment wise passengers fares obtained
        LinkedHashMap<String, List<Double>> segmentwiseCtPricing = getSegmentBasedPricings(segmentedMap);
        //journey based supplier wise fare
        return getjourneyAndSupplierwiseCtFare(segmentwiseCtPricing);
    }

    private Map<String, BookingAmountData> getSuppliewiseApiBookingAmount(TestContext testContext, Set<String> applicableSuppliersList) {
        Map<String, BookingAmountData> apiSuppliersBookingAmountData = new HashMap<>();
        applicableSuppliersList.forEach(as ->
                {
                    if (apiSuppliersBookingAmountData.containsKey(as)) {
                        addInExistingAmount(testContext, apiSuppliersBookingAmountData, as);
                    } else {
                        addNewAmount(testContext, apiSuppliersBookingAmountData, as);
                    }
                }
        );
        return apiSuppliersBookingAmountData;
    }

    private LinkedHashMap<String, List<Double>> getSegmentBasedPricings(Map<SegmentKeyMap, FlightSegment> segmentedMap) {

        LinkedHashMap<String, List<Double>> pricingInfo = new LinkedHashMap<>();
        Map<Integer,Double> pricingIndexTotal= ctXmlReader.pricingInfoTotal();
        for (SegmentKeyMap segmentKeyMap : segmentedMap.keySet()) {

            pricingInfo.put(segmentKeyMap.getKey(),ctXmlReader.getPricingInfoIndexFromBookingInfo(segmentKeyMap.getIndex(),pricingIndexTotal));

        }
        return pricingInfo;
    }

    private void addNewAmount(TestContext testContext, Map<String, BookingAmountData> apiSuppliersBookingAmountData, String as) {
        apiSuppliersBookingAmountData.put(as, dataExtractorsFactoryRegistry.getFactory(as).getBookingAmountExtractor().getBookingAmount(testContext, as));
    }

    private void addInExistingAmount(TestContext testContext, Map<String, BookingAmountData> apiSuppliersBookingAmountData, String as) {
        double quotedAmount = apiSuppliersBookingAmountData.get(as).getQuotedAmount() + dataExtractorsFactoryRegistry.getFactory(as).getBookingAmountExtractor().getBookingAmount(testContext, as).getQuotedAmount();
        double threshold = apiSuppliersBookingAmountData.get(as).getThreshold() + dataExtractorsFactoryRegistry.getFactory(as).getBookingAmountExtractor().getBookingAmount(testContext, as).getThreshold();
        apiSuppliersBookingAmountData.put(as, new BookingAmountData(quotedAmount, threshold));
    }

    private LinkedHashMap<String, LinkedHashMap<String, Double>> getjourneyAndSupplierwiseCtFare(LinkedHashMap<String, List<Double>> pricingInfo) {

        LinkedHashMap<String, LinkedHashMap<String, Double>> journeyTypeFare = new LinkedHashMap<>();
        LinkedHashMap<String, Double> owSupplierWiseFare = new LinkedHashMap<>();
        LinkedHashMap<String, Double> rtSupplierWiseFare = new LinkedHashMap<>();
        for (Map.Entry<String, List<Double>> stringListEntry : pricingInfo.entrySet()) {


            String supp = TestUtils.getSupplierNameFromCode(stringListEntry.getKey().substring(stringListEntry.getKey().indexOf("|") + 1, stringListEntry.getKey().lastIndexOf("|")));
            if (stringListEntry.getKey().contains("OW")) {
                if (owSupplierWiseFare.containsKey(supp)) {
                    owSupplierWiseFare.put(supp, owSupplierWiseFare.get(supp) + stringListEntry.getValue().stream().mapToDouble(Double::doubleValue).sum());
                } else {
                    owSupplierWiseFare.put(supp, stringListEntry.getValue().stream().mapToDouble(Double::doubleValue).sum());
                }
                journeyTypeFare.put("OW", owSupplierWiseFare);

            } else if (stringListEntry.getKey().contains("RT")) {
                if (rtSupplierWiseFare.containsKey(supp)) {
                    rtSupplierWiseFare.put(supp, rtSupplierWiseFare.get(supp) + stringListEntry.getValue().stream().mapToDouble(Double::doubleValue).sum());
                } else {
                    rtSupplierWiseFare.put(supp, stringListEntry.getValue().stream().mapToDouble(Double::doubleValue).sum());
                }
                journeyTypeFare.put("RT", rtSupplierWiseFare);
            }
        }
        return journeyTypeFare;
    }

    public TestResult matchDetails(LinkedHashMap<String, LinkedHashMap<String, Double>> ctBookingAmount, Map<String, BookingAmountData> apiSuppliersBookingAmountData, Set<String> applicableSuppliersList) {
        Map<String, Double> ctSupplierBaseBookingAmount = getSupplierwiseBookingAmount(ctBookingAmount, applicableSuppliersList);
        //validation for empty data
        ToBeCheckedValidatorData validatorData = validateToBeMatchedDetails(ctSupplierBaseBookingAmount, apiSuppliersBookingAmountData);
        switch (validatorData.getValidStatus()){

            case FAILED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), FAILED,
                        validatorData.getMessage(),
                        Optional.empty());
            case SKIPPED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), SKIPPED,
                        validatorData.getMessage(),
                        Optional.empty());
        }
        LOGGER.error(validatorData.getMessage());

        for (Map.Entry<String, BookingAmountData> stringBookingAmountDataEntry : apiSuppliersBookingAmountData.entrySet()) {
            Double aDouble = ctSupplierBaseBookingAmount.get(stringBookingAmountDataEntry.getKey());
            double quotedAmount = stringBookingAmountDataEntry.getValue().getQuotedAmount();
            if (Math.abs(aDouble - quotedAmount) != Math.abs(stringBookingAmountDataEntry.getValue().getThreshold()) || (aDouble.equals(0.0) || quotedAmount == 0.0)) {
                return getFailureResult(ctSupplierBaseBookingAmount, apiSuppliersBookingAmountData);
            }
        }
        LOGGER.error(" Booking amount are validated ");
        return getSuccessResult(ctSupplierBaseBookingAmount,apiSuppliersBookingAmountData);
    }

    private ToBeCheckedValidatorData validateToBeMatchedDetails(Map<String, Double> supplierWiseBookingAmount, Map<String, BookingAmountData> apiSuppliersWiseBookingAmount) {

        if (!supplierWiseBookingAmount.isEmpty() && apiSuppliersWiseBookingAmount.isEmpty()) {
            return new ToBeCheckedValidatorData("Booking amount validation", FAILED, "CT and API Booking amount data not present hence skipping the testcase");
        }


        for (Map.Entry<String, Double> entry : supplierWiseBookingAmount.entrySet()) {
            if (!apiSuppliersWiseBookingAmount.containsKey(entry.getKey())) {
                return new ToBeCheckedValidatorData("booking amount validation", FAILED, "CT Booking amount data for supplier: " + entry.getKey() + " :is not present hence skipping the testcase");
            }
        }

        return new ToBeCheckedValidatorData("booking amount validation", TestResult.Status.SUCCESS, "Both Ct and api result are not empty hence proceeding the testcase");
    }

    private TestResult getSuccessResult(Map<String, Double> expectedValue, Map<String, BookingAmountData> oldValue) {
        return new TestResult("Booking Amount validation testcase",
                TestResult.Status.SUCCESS,
                "Booking amount validation success",
                expectedValue.toString(),
                oldValue.toString(),
                Optional.empty()
        );
    }

    private TestResult getFailureResult(Map<String, Double> expectedValue, Map<String, BookingAmountData> oldValue) {
        return new TestResult("Booking Amount validation validation testcase",
                TestResult.Status.FAILED,
                "Booking Amount validation failure validation failure",
                expectedValue.toString(),
                oldValue.toString(),
                Optional.empty()
        );
    }

    private Map<String, Double> getSupplierwiseBookingAmount(LinkedHashMap<String, LinkedHashMap<String, Double>> ctBookingAmount, Set<String> applicableSuppliersList) {
        Map<String, Double> ctSupplierBaseBookingAmount = new HashMap<>();
        for (String s : applicableSuppliersList) {
            ctSupplierBaseBookingAmount.put(s, 0.0d);
        }
        for (String s : applicableSuppliersList) {
            for (LinkedHashMap<String, Double> value : ctBookingAmount.values()) {
                if (value.containsKey(s)) {
                    Double aDouble = value.get(s);
                    ctSupplierBaseBookingAmount.put(s, aDouble + ctSupplierBaseBookingAmount.get(s));
                }
            }
        }
        return ctSupplierBaseBookingAmount;
    }
}
