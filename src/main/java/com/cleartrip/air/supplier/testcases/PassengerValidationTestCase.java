package com.cleartrip.air.supplier.testcases;

import com.cleartrip.air.supplier.Ctxmlreader;
import com.cleartrip.air.supplier.enums.Gender;
import com.cleartrip.air.supplier.enums.PassengerType;
import com.cleartrip.air.supplier.testframework.DataExtractorsFactoryRegistry;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.testframework.TestResult;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;
import com.cleartrip.air.supplier.validatordata.ToBeCheckedValidatorData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cleartrip.air.supplier.testframework.TestResult.Status;
import static com.cleartrip.air.supplier.testframework.TestResult.Status.FAILED;
import static com.cleartrip.air.supplier.testframework.TestResult.Status.SKIPPED;
import static com.cleartrip.air.supplier.testframework.TestResult.getTestResult;

public class PassengerValidationTestCase extends TestCase {

    private DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry;
    private final Map<String, Function<PassengerType, List<String>>> supplierWiseIgnorableParams;
    private static final Logger LOGGER = LogManager.getLogger(PassengerValidationTestCase.class);

    public PassengerValidationTestCase(String name, DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry, Ctxmlreader ctxmlreader, Map<String, Function<PassengerType, List<String>>> supplierWiseIgnorableParams) {
        super(name, ctxmlreader);
        this.dataExtractorsFactoryRegistry = dataExtractorsFactoryRegistry;
        this.supplierWiseIgnorableParams = supplierWiseIgnorableParams;
    }

    @Override
    public TestResult run(TestContext testContext) {
        LOGGER.error("entering passenger validation testcase");
        //fetching passenger details from cleartripXMLs.
        List<PassengersDetailsData> ctPassengerDetails = ctXmlReader.getPassengerDetails();
        Set<String> applicableSuppliersList = ctXmlReader.getApplicableSuppliers();
        Map<String, List<PassengersDetailsData>> apiSuppliersPassengersDetailsData = new HashMap<>();
        //fetching passenger details according to supplier from APIxmls.
        applicableSuppliersList.forEach(as -> apiSuppliersPassengersDetailsData.put(as, dataExtractorsFactoryRegistry.getFactory(as.toLowerCase()).getPassengerDataExtractor().getPassengerData(testContext,as)));
        TestResult result = matchDetails(ctPassengerDetails, apiSuppliersPassengersDetailsData);
        return result;
    }

    public TestResult matchDetails(List<PassengersDetailsData> ctPassengerDetails, Map<String, List<PassengersDetailsData>> apiDetails) {

        //validation for empty data
        ToBeCheckedValidatorData validatorData = validateToBeMatchedDetails(ctPassengerDetails, apiDetails);
        switch (validatorData.getValidStatus()){

            case FAILED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), FAILED,
                        validatorData.getMessage(),
                        Optional.empty());
            case SKIPPED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), SKIPPED,
                        validatorData.getMessage(),
                        Optional.empty());
        }
        LOGGER.error(validatorData.getMessage());

        ctPassengerDetails = sortPassengerDetails(ctPassengerDetails);

        for (Map.Entry<String, List<PassengersDetailsData>> entry : apiDetails.entrySet()) {
            entry.setValue(sortPassengerDetails(entry.getValue()));
            List<PassengersDetailsData> ctPassengerDetailsRefined = PassengerFactory.buildPax(ctPassengerDetails,entry.getKey());
            for (int index = 0; index < entry.getValue().size(); index++) {
                List<String> ignorableList = getIgnorableList(entry, index);
                // Due to mismatching of MS and MRS title for female adult modifing api passenger details to avoid testcase failure
                PassengersDetailsData apiPassengerDetails = tweakMismatchedTitleForApiResponse(ctPassengerDetailsRefined.get(index), entry.getValue().get(index));

                List<String> differParams =  ctPassengerDetailsRefined.get(index)
                       .compare(apiPassengerDetails, ignorableList);
               if(!differParams.isEmpty()){
                   return getFailureResult(ctPassengerDetailsRefined, entry.getValue(), entry.getKey(), differParams);
               }
            }
        }
        LOGGER.error(" Passenger details are validated ");
        return getSuccessResult(ctPassengerDetails, apiDetails);
    }

    private PassengersDetailsData tweakMismatchedTitleForApiResponse(PassengersDetailsData ctPassengersDetails, PassengersDetailsData apiPassengersDetails) {
        if((apiPassengersDetails.getTravellerType() == PassengerType.ADT
                && apiPassengersDetails.getGender() == Gender.FEMALE)
                && (apiPassengersDetails.getTitle().equalsIgnoreCase("MS") || (apiPassengersDetails.getTitle().equalsIgnoreCase("MRS")))
        && !ctPassengersDetails.getTitle().equalsIgnoreCase(apiPassengersDetails.getTitle())){
            LOGGER.error("Due to mismatching of MS and MRS title for female adult modifing api passenger details to avoid testcase failure");
            return new PassengersDetailsData(apiPassengersDetails.getFirstName(),
                    apiPassengersDetails.getLastName(),
                    apiPassengersDetails.getTravellerType(),
                    apiPassengersDetails.getDob(),
                    apiPassengersDetails.getPassport().isPresent() ? apiPassengersDetails.getPassport().get(): null,
                    ctPassengersDetails.getTitle(),
                    apiPassengersDetails.getGender()
                    );
        }
        return apiPassengersDetails;
    }

    private List<String> getIgnorableList(Map.Entry<String, List<PassengersDetailsData>> entry, int index) {
        return supplierWiseIgnorableParams.get(entry.getKey().toLowerCase()) == null ?
                new ArrayList<>() :
                supplierWiseIgnorableParams.get(entry.getKey().toLowerCase()).apply(entry.getValue().get(index).getTravellerType()) == null ?
                        new ArrayList<>() :
                        supplierWiseIgnorableParams.get(entry.getKey().toLowerCase()).apply(entry.getValue().get(index).getTravellerType());
    }

    private ToBeCheckedValidatorData validateToBeMatchedDetails(List<PassengersDetailsData> passengersDetailsData, Map<String, List<PassengersDetailsData>> apipassengerDetailsData) {

        if(!passengersDetailsData.isEmpty() && apipassengerDetailsData.isEmpty()){
            return new ToBeCheckedValidatorData("Passenger details validation", FAILED, "CT and API passengers details validation data not present hence failing the testcase");
        }

        for (Map.Entry<String, List<PassengersDetailsData>> entry : apipassengerDetailsData.entrySet()) {
            if(entry.getValue().isEmpty()){
                return new ToBeCheckedValidatorData("Passenger details validation", FAILED, "API passenger details for supplier:" + entry.getKey() + " :is not present hence failing the testcase");
            }
        }

        return new ToBeCheckedValidatorData("passenger details validation", Status.SUCCESS, "Both Ct and api result are not empty hence proceeding the testcase");
    }

    private List<PassengersDetailsData> sortPassengerDetails(List<PassengersDetailsData> value) {
        return value.stream().sorted(getSortCriteria()).collect(Collectors.toList());
    }

    private Comparator<? super PassengersDetailsData> getSortCriteria() {
        return Comparator.comparing(PassengersDetailsData::getTravellerType)
                        .thenComparing(PassengersDetailsData::getFirstName)
                        .thenComparing(PassengersDetailsData::getLastName);
    }

    private TestResult getSuccessResult(List<PassengersDetailsData> ctPassengerDetails, Map<String, List<PassengersDetailsData>> apiDetails) {
        return getTestResult("Passenger Details Verification",
                Status.SUCCESS,
                "Passenger details verification passed",
                ctPassengerDetails,
                apiDetails,
                Optional.empty());
    }

    private TestResult getFailureResult(List<PassengersDetailsData> ctPassengerDetails, List<PassengersDetailsData> value, String supplier, List<String> differedParams) {
        return getTestResult("Passenger Details Verification",
                FAILED,
                "Passenger details verification failed for supplier "+ supplier + " for parameters " + differedParams + " ",
                ctPassengerDetails,
                value,
                Optional.empty());
    }
}
