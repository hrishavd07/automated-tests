package com.cleartrip.air.supplier.testcases;

import com.cleartrip.air.supplier.Ctxmlreader;
import com.cleartrip.air.supplier.testframework.DataExtractorsFactoryRegistry;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.testframework.TestResult;
import com.cleartrip.air.supplier.validatordata.CustomerData;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;
import com.cleartrip.air.supplier.validatordata.ToBeCheckedValidatorData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.cleartrip.air.supplier.testframework.TestResult.Status.FAILED;
import static com.cleartrip.air.supplier.testframework.TestResult.Status.SKIPPED;
import static com.cleartrip.air.supplier.testframework.TestResult.getTestResult;

public class CustomerDataValidationTestCase extends TestCase {
    private DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry;
    private final Map<String, Supplier <List<String>>> supplierWiseIgnorableParams;
    private static final Logger LOGGER = LogManager.getLogger(CustomerDataValidationTestCase.class);

    public CustomerDataValidationTestCase(String name, DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry, Ctxmlreader ctxmlreader, Map<String, Supplier <List<String>>> supplierWiseIgnorableParams) {
        super(name, ctxmlreader);
        this.dataExtractorsFactoryRegistry = dataExtractorsFactoryRegistry;
        this.supplierWiseIgnorableParams = supplierWiseIgnorableParams;
    }

    @Override
    public TestResult run(TestContext testContext) {
        LOGGER.error("Entering customer details validation testcase");
        CustomerData ctCustomerData = ctXmlReader.getCustomerData();
        Set<String> applicableSuppliersList = ctXmlReader.getApplicableSuppliers();
        Map<String, List<CustomerData>> apiCustomerDetails = new HashMap<>();
        applicableSuppliersList.forEach(as -> apiCustomerDetails.put(as, dataExtractorsFactoryRegistry.getFactory(as.toLowerCase()).getCustomerDetailsExtractor().getCustomerData(testContext,as)));
        return matchDetails(ctCustomerData, apiCustomerDetails);
    }

    private TestResult matchDetails(CustomerData ctCustomerData, Map<String, List<CustomerData>> apiCustomerDetails) {

        ToBeCheckedValidatorData validatorData = validateToBeMatchedDetails(ctCustomerData, apiCustomerDetails);
        switch (validatorData.getValidStatus()){

            case FAILED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), FAILED,
                        validatorData.getMessage(),
                        Optional.empty());
            case SKIPPED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), SKIPPED,
                        validatorData.getMessage(),
                        Optional.empty());
        }


        for (Map.Entry<String, List<CustomerData>> entry : apiCustomerDetails.entrySet()) {
            for (int index = 0; index < entry.getValue().size(); index++) {
                List<String> ignorableList = supplierWiseIgnorableParams.get(entry.getKey().toLowerCase()).get() == null ? new ArrayList<>() : supplierWiseIgnorableParams.get(entry.getKey().toLowerCase()).get();
                List<String> differParams =  ctCustomerData.compare(entry.getValue().get(index), ignorableList);
                if(!differParams.isEmpty()){
                    return getFailureResult(ctCustomerData, entry.getValue(), entry.getKey(), differParams);
                }
            }
        }
        LOGGER.error(" Customer details are validated ");
        return getSuccessResult(ctCustomerData, apiCustomerDetails);
    }

    private ToBeCheckedValidatorData validateToBeMatchedDetails(CustomerData ctCustomerData, Map<String, List<CustomerData>> apiCustomerDetails) {

        if(null!=ctCustomerData && apiCustomerDetails.isEmpty()){
            return new ToBeCheckedValidatorData("customer details validation", FAILED, "CT and API customer details validation data not present hence skipping the testcase");
        }

        for (Map.Entry<String, List<CustomerData>> entry : apiCustomerDetails.entrySet()) {
            if(entry.getValue().isEmpty()){
                return new ToBeCheckedValidatorData("customer details validation", FAILED, "API customer details for supplier:" + entry.getKey() + " :is not present hence failing the testcase");
            }
        }

        return new ToBeCheckedValidatorData("customer details validation", TestResult.Status.SUCCESS, "Both Ct and api result are not empty hence proceeding the testcase");
    }
    private TestResult getSuccessResult(CustomerData ctCustomerData, Map<String, List<CustomerData>> apiCustomerDetails) {
   return new TestResult("Customer Details Verification ", TestResult.Status.SUCCESS, "Customer Details verified sucessfully ",ctCustomerData,apiCustomerDetails,Optional.empty());


    }

    private TestResult getFailureResult(CustomerData ctPassecustomerDataerDetails, List<CustomerData> value, String supplier, List<String> differedParams) {

        return new TestResult("Customer Details Verification",FAILED , "Customer details verification failed for supplier "+ supplier + " for parameters " + differedParams + " ", ctPassecustomerDataerDetails, value,Optional.empty());
    }
}
