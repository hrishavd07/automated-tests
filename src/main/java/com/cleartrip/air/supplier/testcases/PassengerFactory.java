package com.cleartrip.air.supplier.testcases;

import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;

import java.util.List;

public class PassengerFactory {
    public static List<PassengersDetailsData> buildPax(List<PassengersDetailsData> passengersDetailsData, String supplier) {
        switch (supplier) {
            case "AMADEUS!":
                return new AmadeusPaxValidationTestCase().preparePassengerName(passengersDetailsData);
            default:
                return passengersDetailsData;
        }
    }
}
