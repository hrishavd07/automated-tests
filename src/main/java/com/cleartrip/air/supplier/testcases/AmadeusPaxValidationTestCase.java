package com.cleartrip.air.supplier.testcases;

import com.cleartrip.air.supplier.enums.PassengerType;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class AmadeusPaxValidationTestCase {

    static class AmadeusTraveller {
        public final PassengersDetailsData passenger;
        public final Optional<PassengersDetailsData> infPassenger;
        public final boolean isInfantHasSameLastName;

        public AmadeusTraveller(PassengersDetailsData passenger, Optional<PassengersDetailsData> infPassenger, boolean isInfantHasSameLastName) {
            this.passenger = passenger;
            this.infPassenger = infPassenger;
            this.isInfantHasSameLastName = isInfantHasSameLastName;
        }
    }

    public List<PassengersDetailsData> preparePassengerName(List<PassengersDetailsData> passengersDetailsData) {
        List<AmadeusTraveller> amadeusTravellerList = getAmadeusTravellerList(passengersDetailsData);
        //make our paxdetails from amadeustraveller list
        return makingPaxDetailWithAmdFormat(amadeusTravellerList);
    }

    private List<PassengersDetailsData> makingPaxDetailWithAmdFormat(List<AmadeusTraveller> amadeusTravellerList) {
        List<PassengersDetailsData> passengersDetailsData = new ArrayList<>();
        for (AmadeusTraveller amadeusTraveller : amadeusTravellerList) {
            String firstName = getFirstName(amadeusTraveller.passenger);
            amadeusTraveller.passenger.setFirstName(firstName);
            passengersDetailsData.add(amadeusTraveller.passenger);
            if (amadeusTraveller.isInfantHasSameLastName) {
                firstName = getFirstName(amadeusTraveller.passenger);
                amadeusTraveller.infPassenger.get().setFirstName(firstName);
                passengersDetailsData.add(amadeusTraveller.infPassenger.get());
            }
        }
        return passengersDetailsData;
    }

    private String getFirstName(PassengersDetailsData passenger) {
        String firstName = passenger.getFirstName();
        firstName = firstName.trim();
        firstName = (firstName + " " + passenger.getTitle()).length() < 57 ? firstName + " " + passenger.getTitle() : firstName;
        return firstName.toUpperCase();
    }

    private List<AmadeusTraveller> getAmadeusTravellerList(List<PassengersDetailsData> passengerList) {
        List<PassengersDetailsData> adultPassengerList = passengerList.stream().filter(passenger -> PassengerType.ADT.equals(passenger.getTravellerType())).collect(Collectors.toList());
        List<PassengersDetailsData> childPassengerList = passengerList.stream().filter(passenger -> PassengerType.CHD.equals(passenger.getTravellerType())).collect(Collectors.toList());
        List<PassengersDetailsData> infantPassengerList = passengerList.stream().filter(passenger -> PassengerType.INF.equals(passenger.getTravellerType())).collect(Collectors.toList());
        Map<Integer, Integer> infantVsAdultIndexMap = new HashMap<>();
        List<Integer> nonSameLastNameInfantPaxIndexes = new ArrayList<>();

        // map adult index vs infant index if common last name
        for (int infantIndex = 0; infantIndex < infantPassengerList.size(); infantIndex++) {
            PassengersDetailsData infantPax = infantPassengerList.get(infantIndex);
            boolean isMapped = false;
            for (int adultIndex = 0; adultIndex < adultPassengerList.size(); adultIndex++) {
                if (!infantVsAdultIndexMap.containsKey(adultIndex)) {
                    PassengersDetailsData adultPax = adultPassengerList.get(adultIndex);
                    String infantLastName = infantPax.getLastName();
                    String adultLastName = adultPax.getLastName();
                    if (infantLastName.equalsIgnoreCase(adultLastName)) {
                        infantVsAdultIndexMap.put(adultIndex, infantIndex);
                        isMapped = true;
                    }
                }
                if (!isMapped) {
                    nonSameLastNameInfantPaxIndexes.add(infantIndex);
                }
                isMapped = false;
            }
        }

        List<AmadeusTraveller> amadeusTravellerList = infantVsAdultIndexMap.entrySet().stream().map(e -> {
            int adultIndex = e.getKey();
            int infantIndex = e.getValue();
            return new AmadeusTraveller(adultPassengerList.get(adultIndex), Optional.of(infantPassengerList.get(infantIndex)), true);
        }).collect(Collectors.toList());


        for (int adultIndex = 0; adultIndex < adultPassengerList.size(); adultIndex++) {
            if (!infantVsAdultIndexMap.containsKey(adultIndex)) {
                PassengersDetailsData adultPassenger = adultPassengerList.get(adultIndex);
                amadeusTravellerList.add(new AmadeusTraveller(adultPassenger, Optional.empty(), false));
            }
        }

        for (PassengersDetailsData childPassenger : childPassengerList) {
            amadeusTravellerList.add(new AmadeusTraveller(childPassenger, Optional.empty(), false));
        }

        for (int infantIndex = 0; infantIndex < nonSameLastNameInfantPaxIndexes.size(); infantIndex++) {
            PassengersDetailsData infantPassenger = infantPassengerList.get(infantIndex);
            amadeusTravellerList.add(new AmadeusTraveller(infantPassenger, Optional.empty(), false));
        }
        return amadeusTravellerList;
    }
}
