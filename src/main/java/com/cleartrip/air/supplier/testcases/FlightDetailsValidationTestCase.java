package com.cleartrip.air.supplier.testcases;

import com.cleartrip.air.supplier.Ctxmlreader;
import com.cleartrip.air.supplier.testframework.DataExtractorsFactoryRegistry;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.testframework.TestResult;
import com.cleartrip.air.supplier.validatordata.FlightSegment;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;
import com.cleartrip.air.supplier.validatordata.SegmentKeyMap;
import com.cleartrip.air.supplier.validatordata.ToBeCheckedValidatorData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

import static com.cleartrip.air.supplier.testframework.TestResult.Status.FAILED;
import static com.cleartrip.air.supplier.testframework.TestResult.Status.SKIPPED;
import static com.cleartrip.air.supplier.testframework.TestResult.getTestResult;

public class FlightDetailsValidationTestCase extends TestCase {
    private DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry;
    private static final Logger LOGGER = LogManager.getLogger(FlightDetailsValidationTestCase.class);

    public FlightDetailsValidationTestCase(String name, DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry, Ctxmlreader ctXmlReader) {
        super(name, ctXmlReader);
        this.dataExtractorsFactoryRegistry = dataExtractorsFactoryRegistry;
    }

    @Override
    public TestResult run(TestContext testContext) {
        LOGGER.error("entering flight details validation testcase");
        LinkedHashMap<SegmentKeyMap, FlightSegment> ctSegmentDetail = ctXmlReader.makeFlightSegments();
        Set<String> applicableSuppliersList = ctXmlReader.getApplicableSuppliers();
        Map<String, FlightSegment> apiSegmentDetail = new HashMap<>();
        applicableSuppliersList.forEach(as -> apiSegmentDetail.putAll(dataExtractorsFactoryRegistry.getFactory(as.toLowerCase()).getFlightDetailsExtractor().getFlightDetails(testContext, as)));
        TestResult result = matchDetails(ctSegmentDetail, apiSegmentDetail);
        return result;
    }

    private TestResult matchDetails(LinkedHashMap<SegmentKeyMap, FlightSegment> ctSegmentDetails, Map<String, FlightSegment> apiSegmentDetails) {

        ToBeCheckedValidatorData validatorData = validateToBeMatchedDetails(ctSegmentDetails, apiSegmentDetails);
        switch (validatorData.getValidStatus()) {

            case FAILED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), FAILED,
                        validatorData.getMessage(),
                        Optional.empty());
            case SKIPPED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), SKIPPED,
                        validatorData.getMessage(),
                        Optional.empty());
        }
        LOGGER.error(validatorData.getMessage());

        for (Map.Entry<SegmentKeyMap, FlightSegment> segmentKeyMapFlightSegmentEntry : ctSegmentDetails.entrySet()) {

            SegmentKeyMap key = segmentKeyMapFlightSegmentEntry.getKey();
            FlightSegment flightSegment = apiSegmentDetails.get(key.getFrom() +"_"+ key.getTo());

            List<String> differParams = segmentKeyMapFlightSegmentEntry.getValue()
                    .compare(flightSegment);
            if (!differParams.isEmpty()) {
                return getFailureResult(ctSegmentDetails, segmentKeyMapFlightSegmentEntry.getValue(), segmentKeyMapFlightSegmentEntry.getKey(), differParams);
            }
        }
        LOGGER.error(" Flight details are validated ");
        return getSuccessResult(ctSegmentDetails, apiSegmentDetails);
    }

    private ToBeCheckedValidatorData validateToBeMatchedDetails(LinkedHashMap<SegmentKeyMap, FlightSegment> ctSegmentDetails, Map<String, FlightSegment> apiSegmentDetails) {

        if (!ctSegmentDetails.isEmpty() && apiSegmentDetails.isEmpty()) {
            return new ToBeCheckedValidatorData("Flight details validation", FAILED, "CT and API Flight details validation data not present hence failing the testcase");
        }

        for (Map.Entry<String, FlightSegment> entry : apiSegmentDetails.entrySet()) {
            if (entry == null) {
                return new ToBeCheckedValidatorData("Flight details validation", FAILED, "API Flight details for supplier:" + entry.getKey() + " :is not present hence failing the testcase");
            }
        }

        return new ToBeCheckedValidatorData("Flight details validation", TestResult.Status.SUCCESS, "Both Ct and api result are not empty hence proceeding the testcase");
    }


    private TestResult getSuccessResult(LinkedHashMap<SegmentKeyMap, FlightSegment> ctPassengerDetails, Map<String, FlightSegment> apiDetails) {
        return new TestResult("Flight details Verification ", TestResult.Status.SUCCESS, "Flight details verified sucessfully ", ctPassengerDetails, apiDetails, Optional.empty());
    }

    private TestResult getFailureResult(LinkedHashMap<SegmentKeyMap, FlightSegment> ctPassengerDetails, FlightSegment value, SegmentKeyMap supplier, List<String> differedParams) {
        return new TestResult("FLight Details Verification", FAILED, "Flight details verification failed for supplier " + supplier + " for parameters " + differedParams + " ", ctPassengerDetails, value, Optional.empty());
    }

}
