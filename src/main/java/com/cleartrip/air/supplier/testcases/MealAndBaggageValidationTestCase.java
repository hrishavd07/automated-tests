package com.cleartrip.air.supplier.testcases;

import com.cleartrip.air.supplier.Ctxmlreader;
import com.cleartrip.air.supplier.testframework.DataExtractorsFactoryRegistry;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.testframework.TestResult;
import com.cleartrip.air.supplier.validatordata.FlightSegment;
import com.cleartrip.air.supplier.validatordata.SegmentKeyMap;
import com.cleartrip.air.supplier.validatordata.ToBeCheckedValidatorData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import static com.cleartrip.air.supplier.testframework.TestResult.Status.FAILED;
import static com.cleartrip.air.supplier.testframework.TestResult.Status.SKIPPED;

public class MealAndBaggageValidationTestCase extends TestCase {

    private static final Logger LOGGER = LogManager.getLogger(MealAndBaggageValidationTestCase.class);
    private DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry;
    private List<String> journeyWiseBaggageEnabledSuppliers;

    public MealAndBaggageValidationTestCase(String name, DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry, Ctxmlreader ctxmlreader, List<String> journeyWiseBaggageEnabledSuppliers) {
        super(name, ctxmlreader);
        this.dataExtractorsFactoryRegistry = dataExtractorsFactoryRegistry;
        this.journeyWiseBaggageEnabledSuppliers = journeyWiseBaggageEnabledSuppliers;
    }

    @Override
    public TestResult run(TestContext testContext) {
        LOGGER.error("entering meal and baggage validation testcase");
        LinkedHashMap<SegmentKeyMap, FlightSegment> segmentedMap = ctXmlReader.makeFlightSegments();
        // supplier wise list of meal and baggage codes according to cleartrip XML
        LinkedHashMap<String, List<String>> ctMealAndBaggageDetails = ctXmlReader.getMealAndBaggageDetails(segmentedMap, journeyWiseBaggageEnabledSuppliers);
        Set<String> applicableSuppliersList = ctXmlReader.getApplicableSuppliers();
        Map<String, List<String>> apiSuppliersMealsAndBaggageDetailsData = new HashMap<>();
        // supplier wise list of meal and baggage codes according to api XML
        applicableSuppliersList.forEach(as -> apiSuppliersMealsAndBaggageDetailsData.put(as, dataExtractorsFactoryRegistry.getFactory(as.toLowerCase()).getMealDetailsExtractor().getMealAndBaggageData(testContext,as)));
        TestResult result = matchDetails(ctMealAndBaggageDetails, apiSuppliersMealsAndBaggageDetailsData);
        return result;
    }

    private TestResult matchDetails(LinkedHashMap<String, List<String>> supplierWiseMealDetails, Map<String, List<String>> apiSuppliersMealsAndBaggageDetailsData) {

        //validation for empty data
        ToBeCheckedValidatorData validatorData = validateToBeMatchedDetails(supplierWiseMealDetails, apiSuppliersMealsAndBaggageDetailsData);
        switch (validatorData.getValidStatus()){

            case FAILED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), FAILED,
                        validatorData.getMessage(),
                        Optional.empty());
            case SKIPPED:
                LOGGER.error(validatorData.getMessage());
                return new TestResult(validatorData.getName(), SKIPPED,
                        validatorData.getMessage(),
                        Optional.empty());
        }
        LOGGER.error(validatorData.getMessage());

        for (Map.Entry<String, List<String>> entry : apiSuppliersMealsAndBaggageDetailsData.entrySet()) {
            if (findDifference(entry.getValue(), supplierWiseMealDetails.get(entry.getKey()))) {
                return getFailureResult(supplierWiseMealDetails.get(entry.getKey()), entry.getValue());
            }
        }
        LOGGER.error(" Meal and Baggage details are validated ");
        return getSuccessResult();
    }

    private ToBeCheckedValidatorData validateToBeMatchedDetails(LinkedHashMap<String, List<String>> supplierWiseCTMealDetails, Map<String, List<String>> apiSuppliersMealsAndBaggageDetailsData) {

        if(!supplierWiseCTMealDetails.isEmpty() && apiSuppliersMealsAndBaggageDetailsData.isEmpty()){
            return new ToBeCheckedValidatorData("meal and baggage validation", TestResult.Status.FAILED, "CT meal and baggage data present and  api meal and baggage data is not present hence failing the testcase");
        }

        for (Map.Entry<String, List<String>> entry : supplierWiseCTMealDetails.entrySet()) {
            if(!apiSuppliersMealsAndBaggageDetailsData.containsKey(entry.getKey())){
                return new ToBeCheckedValidatorData("meal and baggage validation", FAILED, "CT meal and baggage data for supplier: " + entry.getKey() + " :is not present in api call hence failing the testcase");
            }
        }

        for (Map.Entry<String, List<String>> entry : supplierWiseCTMealDetails.entrySet()) {
            if(entry.getValue().isEmpty()){
                return new ToBeCheckedValidatorData("meal and baggage validation", SKIPPED, "CT meal and baggage data for supplier: " + entry.getKey() + " :is not present hence skipping the testcase");
            }
        }
        for (Map.Entry<String, List<String>> entry : apiSuppliersMealsAndBaggageDetailsData.entrySet()) {
            if(entry.getValue().isEmpty()){
                return new ToBeCheckedValidatorData("meal and baggage validation", SKIPPED, "API meal and baggage data for supplier:" + entry.getKey() + " :is not present hence skipping the testcase");
            }
        }


        return new ToBeCheckedValidatorData("meal and baggage validation", TestResult.Status.SUCCESS, "Both Ct and api result are not empty hence proceeding the testcase");
    }

    private boolean findDifference(List<String> ctValues, List<String> apiValues) {

        Collections.sort(ctValues);
        Collections.sort(apiValues);
        return !ctValues.equals(apiValues);
    }
    private TestResult getSuccessResult() {
        return new TestResult("Meal and baggage validation testcase",
                TestResult.Status.SUCCESS,
                "Meal validation sucess",
                Optional.empty()
        );
    }

    private TestResult getFailureResult(List<String> expectedValue, List<String> oldValue) {
        return new TestResult("Meal and baggage validation testcase",
                TestResult.Status.FAILED,
                "Meal validation failure",
                expectedValue.toString(),
                oldValue.toString(),
                Optional.empty()
        );
    }

}
