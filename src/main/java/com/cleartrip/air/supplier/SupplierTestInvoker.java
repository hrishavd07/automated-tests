package com.cleartrip.air.supplier;

import com.cleartrip.air.supplier.testframework.SupplierTestConfigurationFactory;
import com.cleartrip.air.supplier.testframework.TestResult;
import com.cleartrip.air.supplier.testframework.TestRunner;
import statsreader.StatsApiReader;

import java.util.List;

public class SupplierTestInvoker {
    public static void main(String[] args) {


        String tripId = "Q190918500506";

      //  String tripId = "Q190913496320";
        //amadeus
        /*String tripId = "Q190913496182";*/
        // amadeus multi-segmented
      //  String tripId = "Q190913496330";
        //airasia
      //  String tripId = "Q190911488134";
        //gal single pax
     //   String tripId = "Q190904480656";
        //indigo single pax
        /*String tripId = "Q190906482348";*/
        //gal multiple pax
        /*String tripId = "Q190909484008";*/
        //gal roundtrip
        /*String tripId = "Q190904480670";*/
        //gal oneway 2+2+2
        /*String tripId = "Q190909483968";*/

        /*String tripId = "Q190912495498";*/

                TestRunner runner = new TestRunner(new SupplierTestConfigurationFactory(new StatsApiReader()), tripId);
        runner.run();
    }
    public List<TestResult> testTripId(String tripID){
        TestRunner runner = new TestRunner(new SupplierTestConfigurationFactory(new StatsApiReader()), tripID);
        return runner.run();
    }
}
