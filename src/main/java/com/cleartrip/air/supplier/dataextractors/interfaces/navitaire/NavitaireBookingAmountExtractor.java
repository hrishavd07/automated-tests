package com.cleartrip.air.supplier.dataextractors.interfaces.navitaire;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.BookingAmountExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.BookingAmountData;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import httpreqreshandler.HttpReqResHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class NavitaireBookingAmountExtractor implements BookingAmountExtractor {

    Logger LOGGER = LogManager.getLogger(NavitaireBookingAmountExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public BookingAmountData getBookingAmount(TestContext testContext, String as) {
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return new BookingAmountData(getQuotedAmount(apiResponseBody), getThreshold((String) testContext.getValue(ITINERARY_ID), statsResponse, as, httpReqResHandler));
    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        // This call return all api calls with name updatePassenger for all suppliers
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "addPaymentToBooking", itineraryId);
        if (!callDetails.isEmpty()) {

            HashSet<String> tid = callDetails.get("tid" + "_" + as);
            HashSet<String> apiResponseBodySet = new HashSet<>();
            //fetching response body for each tIds
            tid.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", id)));
            return apiResponseBodySet;
        } else {
            LOGGER.error("NO API CALL FOUND FOR ADD PAYMENT");
            return new HashSet<>();
        }
    }

    private Double getQuotedAmount(HashSet<String> apiResponseBodySet) {
        List<Double> amount = new ArrayList<>();
        apiResponseBodySet.forEach(apiResponseBody -> {
            Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
            Node documentElement = document.getDocumentElement();
            Node airUpdatePassenegerNode = DOMHelper.selectNode("soapenv:Body/AddpaymentToBookingRequest", documentElement);
            for (Node n : DOMHelper.selectNodes("ns5:addPaymentToBookingReqData", airUpdatePassenegerNode)) {
                amount.add(DOMHelper.selectNodeDoubleValue("ns5:QuotedAmount", n));
            }
        });
        return getTotal(amount);
    }

    private Double getTotal(List<Double> amount) {
        Double total = 0.0;
        for (Double amt : amount) {
            total = total + amt;
        }
        return total;
    }

    private Double getThreshold(String itineraryId, StatsResponse statsResponse, String as, HttpReqResHandler httpReqResHandler) {
        double sellAmount = 0.0;
        double updateContactAmount = getUpdateContactAmount(as, httpReqResHandler, itineraryId, statsResponse);
        if (updateContactAmount == 0.0) {
            return 0.0;
        }
        double ssrInftAmount = getSsrInftAmount(as, httpReqResHandler, itineraryId, statsResponse);
        if (ssrInftAmount == 0.0) {
            sellAmount = getSellAmount(as, httpReqResHandler, itineraryId, statsResponse);
        }
        return updateContactAmount - ssrInftAmount - sellAmount;
    }

    private double getSellAmount(String as, HttpReqResHandler httpReqResHandler, String itineraryId, StatsResponse statsResponse) {
        HashSet<String> tidSet;
        List<String> apiResponseBodyList = new ArrayList<>();
        double sellAmount;
        Map<String, HashSet<String>> sellDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "SELL", itineraryId);
        tidSet = sellDetails.get("tid" + "_" + as);
        tidSet.forEach(tid -> {
            apiResponseBodyList.add
                    (httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_res", tid));
        });
        sellAmount = getSsrAmount(apiResponseBodyList, "SellResponse");
        return sellAmount;
    }

    private double getSsrInftAmount(String as, HttpReqResHandler httpReqResHandler, String itineraryId, StatsResponse statsResponse) {
        HashSet<String> tidSet;
        List<String> apiResponseBodyList = new ArrayList<>();
        double ssrInftAmount = 0.0;
        Map<String, HashSet<String>> infantSsrDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "SellSsrInft", itineraryId);
        if (!infantSsrDetails.isEmpty()) {
            tidSet = infantSsrDetails.get("tid" + "_" + as);
            tidSet.forEach(tid -> {
                apiResponseBodyList.add
                        (httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_res", tid));
            });
            ssrInftAmount = getSsrAmount(apiResponseBodyList, "SellResponse");
        }
        return ssrInftAmount;
    }

    private double getUpdateContactAmount(String as, HttpReqResHandler httpReqResHandler, String itineraryId, StatsResponse statsResponse) {
        double updateContactAmount;
        List<String> apiResponseBodyList = new ArrayList<>();
        Map<String, HashSet<String>> updateContactDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "UpdateContacts", itineraryId);
        if (updateContactDetails.isEmpty()) {
            LOGGER.error("No update contact call has been made");
            return 0.0;
        }
        HashSet<String> tidSet = updateContactDetails.get("tid" + "_" + as);
        tidSet.forEach(tid -> {
            apiResponseBodyList.add
                    (httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_res", tid));
        });
        updateContactAmount = getSsrAmount(apiResponseBodyList, "UpdateContactsResponse");
        return updateContactAmount;
    }

    private double getSsrAmount(List<String> apiResponseBodyList, String type) {

        double amount = 0.0;
        Document document;
        for (String apiResponseBody : apiResponseBodyList) {
            document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
            Node documentElement = document.getDocumentElement();
            String path = "s:Body/" + type + "/BookingUpdateResponseData";
            Node bookingUpdateResponseData = DOMHelper.selectNode(path, documentElement);

            for (Node n : DOMHelper.selectNodes("Success", bookingUpdateResponseData)) {
                for (Node totalCost : DOMHelper.selectNodes("PNRAmount", n)) {
                    amount = amount + DOMHelper.selectNodeDoubleValue("TotalCost", totalCost);
                }
            }
        }

        return amount;
    }
}
