package com.cleartrip.air.supplier.dataextractors.interfaces.amadeus;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.CustomerDataExtractor;
import com.cleartrip.air.supplier.dataextractors.interfaces.airAsia.AirAsiaCustomerDetailsExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.CustomerData;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class AmadeusCustomerDetailsExctractor implements CustomerDataExtractor {

    private final Logger LOGGER = LogManager.getLogger(AmadeusCustomerDetailsExctractor.class);
    private final String ITINERARY_ID = "itineraryId";
    @Override
    public List<CustomerData> getCustomerData(TestContext testContext, String as) {
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return getApiCustomerData(apiResponseBody);
    }

    private List<CustomerData> getApiCustomerData(HashSet<String> apiResponseBodySet) {
        List<CustomerData> customerData = new ArrayList<>();
        apiResponseBodySet.forEach(apiResponseBody -> {
            Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
            Node documentElement = document.getDocumentElement();
            Node dataElementsMaster = DOMHelper.selectNode("S:Body/PNR_Reply/dataElementsMaster", documentElement);
            for (Node dataElementsInDiv : DOMHelper.selectNodes("dataElementsInDiv", dataElementsMaster)) {
                String longFreeText = DOMHelper.selectNodeValue("otherDataFreeText/longFreeText", dataElementsInDiv);
                if(longFreeText.contains("//")){
                    customerData.add(new CustomerData(longFreeText.toLowerCase().replace("//","@"), StringUtils.EMPTY,StringUtils.EMPTY,StringUtils.EMPTY,StringUtils.EMPTY));
                }
            }
            });
        return customerData;
        }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "AMADEUS-ADD_TRAVELLER", itineraryId);
        HashSet<String> tidSet = callDetails.get("tid" + "_" + as);
        if (tidSet.isEmpty()) {
            LOGGER.error("No Tid found for add traveller api");
            return new HashSet<>();
        }
        HashSet<String> apiResponseBodySet = new HashSet<>();
        //fetching response body for each tIds
        tidSet.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_res", id)));
        return apiResponseBodySet;
    }
}
