package com.cleartrip.air.supplier.dataextractors.factory.galileo;

import com.cleartrip.air.supplier.dataextractors.factory.SupplierDataExtractorsFactory;
import com.cleartrip.air.supplier.dataextractors.interfaces.*;
import com.cleartrip.air.supplier.dataextractors.interfaces.galileo.*;

public class GalileoDataExtractorFactory implements SupplierDataExtractorsFactory {
    @Override
    public PassengerDataExtractor getPassengerDataExtractor() {
        return new GalileoPassengerDataExtractor();
    }

    @Override
    public BookingAmountExtractor getBookingAmountExtractor() {

        return new GalileoBookingAmountExtractor();
    }

    @Override
    public MealAndBaggageDetailsExtractor getMealDetailsExtractor() {
        return new GalileoMealAndBaggageExtractor();
    }

    @Override
    public CustomerDataExtractor getCustomerDetailsExtractor() {
        return new GalileoCustomerDataExtractor();
    }

    @Override
    public FlightDetailDataExtractor getFlightDetailsExtractor() {
        return new GalileoFlightDetailsExtractor();
    }
}
