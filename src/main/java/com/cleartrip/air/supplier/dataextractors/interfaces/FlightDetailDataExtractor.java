package com.cleartrip.air.supplier.dataextractors.interfaces;

import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.FlightSegment;

import java.util.Map;

public interface FlightDetailDataExtractor {
    Map<String, FlightSegment> getFlightDetails(TestContext testContext, String as);
}
