package com.cleartrip.air.supplier.dataextractors.interfaces.airAsia;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.Utils.NavitaireTitleUtil;
import com.cleartrip.air.supplier.dataextractors.interfaces.PassengerDataExtractor;
import com.cleartrip.air.supplier.enums.Gender;
import com.cleartrip.air.supplier.enums.PassengerType;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;
import com.google.common.collect.ImmutableMap;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;

public class AirAsiaPassengerDataExtractor implements PassengerDataExtractor {

    private final Logger LOGGER = LogManager.getLogger(AirAsiaPassengerDataExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public List<PassengersDetailsData> getPassengerData(TestContext testContext, String as) {

        Map<String, HashSet<String>> callDetails;
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        String itineraryId = (String) testContext.getValue(ITINERARY_ID);
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "doCommitBookingCall", itineraryId);
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        HashSet<String> tidSet = callDetails.get("tid" + "_" + as);
        if (CollectionUtils.isEmpty(tidSet)) {
            LOGGER.error("No Tid found for commitBooking api hence returning empty Passenger's list");
            return new ArrayList<>();
        }
        //fetching only first element to set of tID as the passenger name would be same for all calls.
        String apiResponseBody = httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", tidSet.iterator().next());
        List<PassengersDetailsData> passengersDetailsData = makePassengers(apiResponseBody);
        return passengersDetailsData;
    }

    private List<PassengersDetailsData> makePassengers(String apiResponseBody) {
        Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
        Node documentElement = document.getDocumentElement();
        Node passengers = DOMHelper.selectNode("soapenv:Body/Commit/objCommitRequestData/Booking", documentElement);
        List<PassengersDetailsData> passengerDetailsData = new ArrayList<>();
        for (Node n : DOMHelper.selectNodes("Passengers/Passenger", passengers)) {
            String firstName = DOMHelper.selectNodeValue("Names/BookingName/FirstName", n);
            String lastName = DOMHelper.selectNodeValue("Names/BookingName/LastName", n);
            String type = DOMHelper.selectNodeValue("PassengerTypeInfos/PassengerTypeInfo/PaxType", n);
            String dob = DOMHelper.selectNodeValue("PassengerTypeInfos/PassengerTypeInfo/DOB", n);
            String gender = DOMHelper.selectNodeValue("PassengerInfo/Gender", n);
            String apiTitle = DOMHelper.selectNodeValue("Names/BookingName/Title", n);
            String title = getTitle(type, apiTitle);
            passengerDetailsData.add(new PassengersDetailsData(firstName.toLowerCase(), lastName.toLowerCase(), PassengerType.valueOf(type), dob, null, title.toUpperCase(), Gender.getGender(gender)));
            if (type.equalsIgnoreCase("ADT")) {
                NodeList childNodes = n.getChildNodes();
                for (int i = 0; i < childNodes.getLength() - 1; i++) {
                    if (childNodes.item(i).getNodeName().equalsIgnoreCase("infant")) {
                        String infantDOB = DOMHelper.selectNodeValue("DOB", childNodes.item(i));
                        String infantFirstName = DOMHelper.selectNodeValue("Names/BookingName/FirstName", childNodes.item(i));
                        String infantLastName = DOMHelper.selectNodeValue("Names/BookingName/LastName", childNodes.item(i));
                        String infantGender = DOMHelper.selectNodeValue("Gender", childNodes.item(i));
                        String infantTitle = NavitaireTitleUtil.getTitle(Gender.getGender(infantGender), PassengerType.valueOf("INF"));
                        passengerDetailsData.add(new PassengersDetailsData(infantFirstName.toLowerCase(), infantLastName.toLowerCase(), PassengerType.INF, infantDOB, null, infantTitle.toUpperCase(), Gender.getGender(gender)));
                    }
                }
            }
        }
        return passengerDetailsData;
    }

    private String getTitle(String type, String title) {
        String key = type + "_" + title;
        switch (key.toLowerCase()) {
            case "adt_mr":
                return "Mr";
            case "chd_mr":
                return "Mstr";
            case "inf_mr":
                return "Mstr";
            case "adt_ms":
                return "Mrs";
            case "chd_ms":
                return "miss";
            case "inf_ms":
                return "miss";
        }
        return StringUtils.EMPTY;
    }
}

