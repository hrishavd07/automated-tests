package com.cleartrip.air.supplier.dataextractors.interfaces.galileo;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.CustomerDataExtractor;
import com.cleartrip.air.supplier.dataextractors.interfaces.navitaire.NavitaireCustomerDetailsDataExtractor;
import com.cleartrip.air.supplier.enums.PassengerType;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.CustomerData;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class GalileoCustomerDataExtractor implements CustomerDataExtractor {

    private final Logger LOGGER = LogManager.getLogger(GalileoCustomerDataExtractor.class);
    private final String ITINERARY_ID = "itineraryId";
    @Override
    public List<CustomerData> getCustomerData(TestContext testContext, String as) {
        Map<String, HashSet<String>> callDetails;
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        String itineraryId = (String) testContext.getValue(ITINERARY_ID);
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"),"AIR_AMEND","GALILEO-AddTraveller", itineraryId);
        HttpReqResHandler httpReqResHandler= new HttpReqResHandler();
        HashSet<String> tidSet = callDetails.get("tid"+"_"+as);
        if(CollectionUtils.isEmpty(tidSet)){
            LOGGER.error("No Tid found for GALILEO-AddTraveller api, hence returning empty array list");
            return new ArrayList<>();
        }
        //fetching only first element to set of tID as the passenger name would be same for all calls.
        String apiResponseBody = httpReqResHandler.getResponseBody(itineraryId,"AIR_AMEND","api_req", tidSet.iterator().next());
        return getApiCustomerData(apiResponseBody);
    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "GALILEO-AddTraveller", itineraryId);
        HashSet<String> tidSet = callDetails.get("tid" + "_" + as);
        if (CollectionUtils.isEmpty(tidSet)) {
            LOGGER.error("No Tid found for GALILEO-AddTraveller api");
            return new HashSet<>();
        }
        HashSet<String> apiResponseBodySet = new HashSet<>();
        //fetching response body for each tIds
        tidSet.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", id)));
        return apiResponseBodySet;
    }

    private List<CustomerData> getApiCustomerData(String apiResponseBody) {
        String phoneNumber = StringUtils.EMPTY;
        String emailId = StringUtils.EMPTY;
        Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
        Node documentElement = document.getDocumentElement();
        Node airAddTravellerNode = DOMHelper.selectNode("S:Body/ns9:BookingTravelerReq/ns9:AddTraveler", documentElement);
        String bookingTravellerTagName = getTagName(airAddTravellerNode, "BookingTraveler");
        if(StringUtils.isEmpty(bookingTravellerTagName)){
            LOGGER.error("booking Traveller tag name is empty hence returning empty passenger list");
            return new ArrayList<>();
        }
        List<CustomerData> customerData = new ArrayList<>();
        for (Node n : DOMHelper.selectNodes(bookingTravellerTagName, airAddTravellerNode)) {
            if (StringUtils.isEmpty(phoneNumber)){
                phoneNumber = getTagName(n, "PhoneNumber");
            };
            if(StringUtils.isEmpty(phoneNumber)){
                LOGGER.error("phone number tag name is empty hence returning empty passenger list");
                return new ArrayList<>();
            }
            Node phoneNumberDetails =  DOMHelper.selectNode(phoneNumber, n);
            NamedNodeMap namedNodeMapForPhoneNumber = phoneNumberDetails.getAttributes();

            String phoneNumberValue = namedNodeMapForPhoneNumber.getNamedItem("Number")!= null ? namedNodeMapForPhoneNumber.getNamedItem("Number").getNodeValue() : "";

            if (StringUtils.isEmpty(emailId)){
                emailId = getTagName(n, "Email");
            };
            if(StringUtils.isEmpty(emailId)){
                LOGGER.error("email Id tag name is empty hence returning empty passenger list");
                return new ArrayList<>();
            }

            Node emailDetails =  DOMHelper.selectNode(emailId, n);
            NamedNodeMap namedNodeMapForEmail = emailDetails.getAttributes();

            String emailIdValue = namedNodeMapForEmail.getNamedItem("EmailID")!= null ? namedNodeMapForEmail.getNamedItem("EmailID").getNodeValue() : "";

            customerData.add(new CustomerData(emailIdValue, "", "", phoneNumberValue, ""));
            break;
        }
        return customerData;
    }

    private String getTagName(Node airAddTravellerNode, String tag) {
        List<String> nodeNameList = new ArrayList<>();
        NodeList nodeList = airAddTravellerNode.getChildNodes();
        for(int i=0; i<nodeList.getLength();i++){
            Node n = nodeList.item(i);
            nodeNameList.add(n.getNodeName());
        }
        return nodeNameList.stream().filter(name -> name.contains(tag)).findFirst().orElse("");
    }
}
