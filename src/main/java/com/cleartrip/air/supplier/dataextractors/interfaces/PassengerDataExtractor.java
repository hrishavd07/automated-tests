package com.cleartrip.air.supplier.dataextractors.interfaces;

import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;

import java.util.List;

public interface PassengerDataExtractor {

    public List<PassengersDetailsData> getPassengerData(TestContext testContext, String as);

}
