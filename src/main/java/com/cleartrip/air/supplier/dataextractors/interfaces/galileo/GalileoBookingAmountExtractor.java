package com.cleartrip.air.supplier.dataextractors.interfaces.galileo;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.BookingAmountExtractor;
import com.cleartrip.air.supplier.enums.PassengerType;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.BookingAmountData;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import com.cleartrip.air.supplier.validatordata.PaxDetails;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.*;

public class GalileoBookingAmountExtractor implements BookingAmountExtractor {

    Logger LOGGER = LogManager.getLogger(GalileoBookingAmountExtractor.class);
    private final String ITINERARY_ID = "itineraryId";
    @Override
    public BookingAmountData getBookingAmount(TestContext testContext, String as) {
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return new BookingAmountData(getQuotedAmount(apiResponseBody), 0.0);
    }

    private Double getQuotedAmount(HashSet<String> apiResponseBodySet) {
        Map<PaxDetails, List<String>> studentDetailsAmountMap = new HashMap<>();
        apiResponseBodySet.stream()
                .map(DocumentReaderUtils::readDocumentForApiResponseBody)
                .map(Document::getDocumentElement)
                .map(doc -> DOMHelper.selectNode("S:Body/ns9:BookingPricingReq/ns9:AddPricing", doc))
                .forEach(node -> {
                    for (Node n : DOMHelper.selectNodes("ns5:AirPricingInfo", node)) {
                        List<String> baseAmount = getBaseAmount(n);
                        List<String> taxAmount = getTaxAmount(n);
                        List<String> totalAmount = new ArrayList<>();
                        totalAmount.addAll(baseAmount);
                        totalAmount.addAll(taxAmount);
                        PaxDetails paxDetails = getPaxDetails(n);
                        studentDetailsAmountMap.put(paxDetails, totalAmount);
                    }

                });

        return getTotal(studentDetailsAmountMap);
    }

    private PaxDetails getPaxDetails(Node n) {
        int count = 0;
        String paxType = StringUtils.EMPTY;
        for (Node node : DOMHelper.selectNodes("ns5:PassengerType", n)) {
            count ++;
            NamedNodeMap namedNodeMap = node.getAttributes();
            paxType = namedNodeMap.getNamedItem("Code") != null ? namedNodeMap.getNamedItem("Code").getNodeValue() : "";
        }
        PassengerType passengerType = paxType.equalsIgnoreCase("CNN") ? PassengerType.valueOf("CHD") : PassengerType.valueOf(paxType);
        return new PaxDetails(count, passengerType);
    }

    private List<String> getTaxAmount(Node n) {
        List<String> taxAmountList = new ArrayList<>();
        for (Node node : DOMHelper.selectNodes("ns5:TaxInfo", n)) {
            NamedNodeMap namedNodeMap = node.getAttributes();
            String value = namedNodeMap.getNamedItem("Amount") != null ? namedNodeMap.getNamedItem("Amount").getNodeValue() : "";
            taxAmountList.add(value);
        }
        return taxAmountList;
    }

    private List<String> getBaseAmount(Node n) {
        List<String> baseAmountList = new ArrayList<>();
        for (Node node : DOMHelper.selectNodes("ns5:FareInfo", n)) {
            NamedNodeMap namedNodeMap = node.getAttributes();
            String value = namedNodeMap.getNamedItem("Amount") != null ? namedNodeMap.getNamedItem("Amount").getNodeValue() : "";
            baseAmountList.add(value);
        }
        return baseAmountList;
    }

    private Double getTotal(Map<PaxDetails, List<String>> amount) {
        Double total = 0.0;
        for (Map.Entry<PaxDetails, List<String>> paxDetailsListEntry : amount.entrySet()) {
            Double totalValue = getTotalValue(paxDetailsListEntry.getValue());
            total = total + totalValue * paxDetailsListEntry.getKey().getCount();
        }
        return total;
    }

    private Double getTotalValue(List<String> value) {
        return value.stream()
                .map(val -> Double.parseDouble(val.replaceAll("\\D+","")))
                .mapToDouble(val -> val).sum();
    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        // This call return all api calls with name updatePassenger for all suppliers
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "GALILEO-AddPricing", itineraryId);
        if (!callDetails.isEmpty()) {

            HashSet<String> tid = callDetails.get("tid" + "_" + as);
            HashSet<String> apiResponseBodySet = new HashSet<>();
            //fetching response body for each tIds
            tid.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", id)));
            return apiResponseBodySet;
        } else {
            LOGGER.error("NO API CALL FOUND FOR ADD PAYMENT");
            return new HashSet<>();
        }
    }
}
