package com.cleartrip.air.supplier.dataextractors.interfaces.navitaire;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.CustomerDataExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.CustomerData;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class NavitaireCustomerDetailsDataExtractor implements CustomerDataExtractor {

    private final Logger LOGGER = LogManager.getLogger(NavitaireCustomerDetailsDataExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public List<CustomerData> getCustomerData(TestContext testContext, String as) {
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return getApiCustomerData(apiResponseBody);
    }

    private List<CustomerData> getApiCustomerData(HashSet<String> apiResponseBodySet) {
        List<CustomerData> customerData = new ArrayList<>();
        apiResponseBodySet.forEach(apiResponseBody -> {
            Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
            Node documentElement = document.getDocumentElement();
            Node airUpdatePassenegerNode = DOMHelper.selectNode("soapenv:Body/UpdateContactsRequest/updateContactsRequestData/ns5:BookingContactList", documentElement);
            Node firstBookingContact = airUpdatePassenegerNode.getFirstChild();
            String firstName = DOMHelper.selectNodeValue("ns5:Names/ns5:BookingName/ns5:FirstName", firstBookingContact);
            String lastName = DOMHelper.selectNodeValue("ns5:Names/ns5:BookingName/ns5:LastName", firstBookingContact);
            String title = DOMHelper.selectNodeValue("ns5:Names/ns5:BookingName/ns5:Title", firstBookingContact);
            String email = DOMHelper.selectNodeValue("ns5:EmailAddress",firstBookingContact);
            String mobile = DOMHelper.selectNodeValue("ns5:HomePhone",firstBookingContact);
            customerData.add(new CustomerData(email,firstName,lastName,mobile,title));
        });
        return customerData;
    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "UpdateContacts", itineraryId);
        HashSet<String> tidSet = callDetails.get("tid" + "_" + as);
        if (CollectionUtils.isEmpty(tidSet)) {
            LOGGER.error("No Tid found for updatePassenger api");
            return new HashSet<>();
        }
        HashSet<String> apiResponseBodySet = new HashSet<>();
        //fetching response body for each tIds
        tidSet.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", id)));
        return apiResponseBodySet;
    }
}
