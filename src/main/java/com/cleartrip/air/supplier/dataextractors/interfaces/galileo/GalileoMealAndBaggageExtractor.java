package com.cleartrip.air.supplier.dataextractors.interfaces.galileo;

import com.cleartrip.air.supplier.dataextractors.interfaces.MealAndBaggageDetailsExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class GalileoMealAndBaggageExtractor implements MealAndBaggageDetailsExtractor {

    public static final Logger LOGGER = LogManager.getLogger(GalileoMealAndBaggageExtractor.class);
    @Override
    public List<String> getMealAndBaggageData(TestContext testContext, String as) {
        LOGGER.error("As meal and baggage is not supported , hence returning empty meal and baggage data");
        return new ArrayList<>();
    }
}
