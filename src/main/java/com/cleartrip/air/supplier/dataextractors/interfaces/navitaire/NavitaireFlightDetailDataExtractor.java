package com.cleartrip.air.supplier.dataextractors.interfaces.navitaire;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DateUtil;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.FlightDetailDataExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.*;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

public class NavitaireFlightDetailDataExtractor implements FlightDetailDataExtractor {

    private final Logger LOGGER = LogManager.getLogger(NavitaireCustomerDetailsDataExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public Map<String, FlightSegment> getFlightDetails(TestContext testContext, String as) {

        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return getApiFlightDetails(apiResponseBody);
    }

    private Map<String, FlightSegment> getApiFlightDetails(HashSet<String> apiResponseBodySet) {
        Map<String, FlightSegment> flightSegmentMap = new HashMap<>();
        apiResponseBodySet.stream()
                .map(DocumentReaderUtils::readDocumentForApiResponseBody)
                .map(Document::getDocumentElement)
                .map(documentElement -> DOMHelper.selectNode("soapenv:Body/SellRequest/SellRequestData/ns6:SellJourneyByKeyRequest/ns6:SellJourneyByKeyRequestData/ns6:JourneySellKeys", documentElement))
                .map(Node::getChildNodes)
                .forEach(childNodes -> {
            for (int i = 0; i < childNodes.getLength(); i++) {
                String journeySellKey = DOMHelper.selectNodeValue("ns6:JourneySellKey", childNodes.item(i));
                addSegmentToMap(journeySellKey, flightSegmentMap);
            }
        });
        return flightSegmentMap;
    }

    private void addSegmentToMap(String journeySellKey, Map<String, FlightSegment> flightSegmentMap) {

        String[] split = journeySellKey.split("~");
        String supplierCode = split[0].trim();
        String flightNumber = split[1].trim();

        String departureAirport = split[4].trim();
        String departureTime = split[5].trim().replaceAll("\\s","-");

        String arrivalAirport = split[6].trim();
        String arrivalTime = split[7].trim().replaceAll("\\s","-");

        Flight flight = new Flight(flightNumber, supplierCode, null, null);
        ZonedDateTime departTime = DateUtil.getZonedDateTime(DateUtil.convertStringToDateNavitaireFormat(departureTime));
        DepartureDetails departureDetails = new DepartureDetails(new Airport(departureAirport), departTime);

        ZonedDateTime arrivalZonedTime = DateUtil.getZonedDateTime(DateUtil.convertStringToDateNavitaireFormat(arrivalTime));
        ArrivalDetails arrivalDetails = new ArrivalDetails(new Airport(arrivalAirport), arrivalZonedTime);

        Airline operatingAirline = new Airline(supplierCode, flightNumber);
        FlightSegment flightSegment = new FlightSegment(flight, departureDetails, arrivalDetails, operatingAirline, operatingAirline, Optional.empty(), null);

        flightSegmentMap.put(departureAirport + "_" + arrivalAirport, flightSegment);
    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "SELL", itineraryId);
        HashSet<String> tidSet = callDetails.get("tid" + "_" + as);
        if (CollectionUtils.isEmpty(tidSet)) {
            LOGGER.error("No Tid found for updatePassenger api");
            return new HashSet<>();
        }
        HashSet<String> apiResponseBodySet = new HashSet<>();
        //fetching response body for each tIds
        tidSet.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", id)));
        return apiResponseBodySet;
    }
}
