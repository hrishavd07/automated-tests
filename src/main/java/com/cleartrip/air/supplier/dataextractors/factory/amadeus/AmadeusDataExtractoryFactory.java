package com.cleartrip.air.supplier.dataextractors.factory.amadeus;

import com.cleartrip.air.supplier.dataextractors.factory.SupplierDataExtractorsFactory;
import com.cleartrip.air.supplier.dataextractors.interfaces.*;
import com.cleartrip.air.supplier.dataextractors.interfaces.amadeus.*;

public class AmadeusDataExtractoryFactory implements SupplierDataExtractorsFactory {
    @Override
    public PassengerDataExtractor getPassengerDataExtractor() {
        return new AmadeusPassengerDataExtractor();
    }

    @Override
    public BookingAmountExtractor getBookingAmountExtractor() {
        return new AmadeusBookingAmountExtractor();
    }

    @Override
    public MealAndBaggageDetailsExtractor getMealDetailsExtractor() {
        return new AmadeusMealAndBaggageDetailsExtractor();
    }

    @Override
    public CustomerDataExtractor getCustomerDetailsExtractor() {
        return new AmadeusCustomerDetailsExctractor();
    }

    @Override
    public FlightDetailDataExtractor getFlightDetailsExtractor() {
        return new AmadeusFlightDetailsExtractor();
    }
}
