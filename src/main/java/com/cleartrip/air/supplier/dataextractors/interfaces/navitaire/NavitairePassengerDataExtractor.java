package com.cleartrip.air.supplier.dataextractors.interfaces.navitaire;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.Utils.NavitaireTitleUtil;
import com.cleartrip.air.supplier.Utils.TestUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.PassengerDataExtractor;
import com.cleartrip.air.supplier.enums.Gender;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.enums.PassengerType;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class NavitairePassengerDataExtractor implements PassengerDataExtractor {


    private final Logger LOGGER = LogManager.getLogger(NavitairePassengerDataExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public List<PassengersDetailsData> getPassengerData(TestContext testContext, String as) {
        Map<String, HashSet<String>> callDetails;
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        String itineraryId = (String) testContext.getValue(ITINERARY_ID);
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"),"AIR_AMEND","updatePassenger", itineraryId);
        HttpReqResHandler httpReqResHandler= new HttpReqResHandler();
        HashSet<String> tidSet = callDetails.get("tid"+"_"+as);
        if(CollectionUtils.isEmpty(tidSet)){
            LOGGER.error("No Tid found for updatePassenger api");
            return new ArrayList<>();
        }
        //fetching only first element to set of tID as the passenger name would be same for all calls.
        String apiResponseBody = httpReqResHandler.getResponseBody(itineraryId,"AIR_AMEND","api_req", tidSet.iterator().next());
        List<PassengersDetailsData> passengersDetailsData = makePassengers(apiResponseBody,as);
        return passengersDetailsData;

    }

    private List<PassengersDetailsData> makePassengers(String apiResponseBody, String as){
        Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
        Node documentElement = document.getDocumentElement();
        Node airUpdatePassenegerNode = DOMHelper.selectNode("soapenv:Body/UpdatePassengersRequest/updatePassengersRequestData", documentElement);
        List<PassengersDetailsData> passengerDetailsData = new ArrayList<>();
        for (Node n : DOMHelper.selectNodes("ns5:Passengers/ns5:Passenger", airUpdatePassenegerNode)) {
            String firstName = DOMHelper.selectNodeValue("ns5:Names/ns5:BookingName/ns5:FirstName", n);
            String lastName = DOMHelper.selectNodeValue("ns5:Names/ns5:BookingName/ns5:LastName", n);
            String type = DOMHelper.selectNodeValue("ns5:PassengerTypeInfos/ns5:PassengerTypeInfo/ns5:PaxType", n);
            String dob = DOMHelper.selectNodeValue("ns5:PassengerTypeInfos/ns5:PassengerTypeInfo/ns5:DOB", n);
            String gender= DOMHelper.selectNodeValue("ns5:PassengerInfo/ns5:Gender",n);
            String title = NavitaireTitleUtil.getTitle(Gender.getGender(gender), PassengerType.valueOf(type));

            passengerDetailsData.add(new PassengersDetailsData(firstName.toLowerCase(), lastName.toLowerCase(), PassengerType.valueOf(type), dob, null,title.toUpperCase(),Gender.getGender(gender)));
            if (type.equalsIgnoreCase("ADT")) {
                NodeList childNodes = n.getChildNodes();
                for (int i = 0; i < childNodes.getLength() - 1; i++) {
                    if (childNodes.item(i).getNodeName().equalsIgnoreCase("ns5:infant")) {
                        String infantDOB = DOMHelper.selectNodeValue("ns5:DOB", childNodes.item(i));
                        String infantFirstName = DOMHelper.selectNodeValue("ns5:Names/ns5:BookingName/ns5:FirstName", childNodes.item(i));
                        String infantLastName = DOMHelper.selectNodeValue("ns5:Names/ns5:BookingName/ns5:LastName", childNodes.item(i));
                        String infantGender = DOMHelper.selectNodeValue("ns5:Gender", childNodes.item(i));
                        String infantTitle=  NavitaireTitleUtil.getTitle(TestUtils.getAirlineCodeFromSupplier(as), Gender.getGender(infantGender), PassengerType.valueOf(PassengerType.INF.toString()));
                        passengerDetailsData.add(new PassengersDetailsData(infantFirstName.toLowerCase(), infantLastName.toLowerCase(), PassengerType.INF, infantDOB, null,infantTitle.toUpperCase(),Gender.getGender(infantGender)));
                    }
                }
            }
        }
        return passengerDetailsData;
    }
}
