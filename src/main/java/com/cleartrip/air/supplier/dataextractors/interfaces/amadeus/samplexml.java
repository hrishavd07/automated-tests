package com.cleartrip.air.supplier.dataextractors.interfaces.amadeus;

public class samplexml {

    public static String strVal(){

        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:awsse=\"http://xml.amadeus.com/2010/06/Session_v3\" xmlns:awsl=\"http://wsdl.amadeus.com/2010/06/ws/Link_v1\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">\n" +
                "  <soapenv:Header>\n" +
                "    <wsa:To>http://www.w3.org/2005/08/addressing/anonymous</wsa:To>\n" +
                "    <wsa:From>\n" +
                "      <wsa:Address>https://nodeD1.test.webservices.amadeus.com/1ASIWATOAKB</wsa:Address>\n" +
                "    </wsa:From>\n" +
                "    <wsa:Action>http://webservices.amadeus.com/PNRADD_11_3_1A</wsa:Action>\n" +
                "    <wsa:MessageID>urn:uuid:4d81a1b6-6552-cfa4-dd4e-0f840b42818e</wsa:MessageID>\n" +
                "    <wsa:RelatesTo RelationshipType=\"http://www.w3.org/2005/08/addressing/reply\">b106fe1d-975d-dfbf-8165-62bb42d2d1ec</wsa:RelatesTo>\n" +
                "    <awsl:TransactionFlowLink>\n" +
                "      <awsl:Consumer>\n" +
                "        <awsl:UniqueID>779ecadc-1857-2b24-abc5-d91128ec3224</awsl:UniqueID>\n" +
                "      </awsl:Consumer>\n" +
                "      <awsl:Receiver>\n" +
                "        <awsl:ServerID>urn:uuid:b6fa266c-864e-56a0-ab9c-3c26732109aa</awsl:ServerID>\n" +
                "      </awsl:Receiver>\n" +
                "    </awsl:TransactionFlowLink>\n" +
                "    <awsse:Session TransactionStatusCode=\"InSeries\">\n" +
                "      <awsse:SessionId>00BWJG51KP</awsse:SessionId>\n" +
                "      <awsse:SequenceNumber>2</awsse:SequenceNumber>\n" +
                "      <awsse:SecurityToken>3TVI58II35H121MDJ1JVXVU5K8</awsse:SecurityToken>\n" +
                "    </awsse:Session>\n" +
                "  </soapenv:Header>\n" +
                "  <soapenv:Body>\n" +
                "    <PNR_Reply xmlns=\"http://xml.amadeus.com/PNRACC_11_3_1A\">\n" +
                "      <pnrHeader>\n" +
                "        <reservationInfo>\n" +
                "          <reservation>\n" +
                "            <companyId>1A</companyId>\n" +
                "          </reservation>\n" +
                "        </reservationInfo>\n" +
                "      </pnrHeader>\n" +
                "      <securityInformation>\n" +
                "        <responsibilityInformation>\n" +
                "          <typeOfPnrElement>RP</typeOfPnrElement>\n" +
                "          <officeId>JEDIK34L9</officeId>\n" +
                "          <iataCode>14365352</iataCode>\n" +
                "        </responsibilityInformation>\n" +
                "        <queueingInformation>\n" +
                "          <queueingOfficeId>JEDIK34L9</queueingOfficeId>\n" +
                "        </queueingInformation>\n" +
                "        <cityCode>BOM</cityCode>\n" +
                "      </securityInformation>\n" +
                "      <sbrPOSDetails>\n" +
                "        <sbrUserIdentificationOwn>\n" +
                "          <originIdentification>\n" +
                "            <inHouseIdentification1></inHouseIdentification1>\n" +
                "          </originIdentification>\n" +
                "        </sbrUserIdentificationOwn>\n" +
                "        <sbrSystemDetails>\n" +
                "          <deliveringSystem>\n" +
                "            <companyId></companyId>\n" +
                "          </deliveringSystem>\n" +
                "        </sbrSystemDetails>\n" +
                "        <sbrPreferences>\n" +
                "          <userPreferences>\n" +
                "            <codedCountry></codedCountry>\n" +
                "          </userPreferences>\n" +
                "        </sbrPreferences>\n" +
                "      </sbrPOSDetails>\n" +
                "      <sbrCreationPosDetails>\n" +
                "        <sbrUserIdentificationOwn>\n" +
                "          <originIdentification>\n" +
                "            <inHouseIdentification1></inHouseIdentification1>\n" +
                "          </originIdentification>\n" +
                "        </sbrUserIdentificationOwn>\n" +
                "        <sbrSystemDetails>\n" +
                "          <deliveringSystem>\n" +
                "            <companyId></companyId>\n" +
                "          </deliveringSystem>\n" +
                "        </sbrSystemDetails>\n" +
                "        <sbrPreferences>\n" +
                "          <userPreferences>\n" +
                "            <codedCountry></codedCountry>\n" +
                "          </userPreferences>\n" +
                "        </sbrPreferences>\n" +
                "      </sbrCreationPosDetails>\n" +
                "      <sbrUpdatorPosDetails>\n" +
                "        <sbrUserIdentificationOwn>\n" +
                "          <originIdentification>\n" +
                "            <originatorId>14365352</originatorId>\n" +
                "            <inHouseIdentification1>JEDIK34L9</inHouseIdentification1>\n" +
                "          </originIdentification>\n" +
                "          <originatorTypeCode>E</originatorTypeCode>\n" +
                "        </sbrUserIdentificationOwn>\n" +
                "        <sbrSystemDetails>\n" +
                "          <deliveringSystem>\n" +
                "            <companyId>1A</companyId>\n" +
                "            <locationId>BOM</locationId>\n" +
                "          </deliveringSystem>\n" +
                "        </sbrSystemDetails>\n" +
                "        <sbrPreferences>\n" +
                "          <userPreferences>\n" +
                "            <codedCountry>IN</codedCountry>\n" +
                "          </userPreferences>\n" +
                "        </sbrPreferences>\n" +
                "      </sbrUpdatorPosDetails>\n" +
                "\n" +
                "\n" +
                "<travellerInfo>\n" +
                "<elementManagementPassenger>\n" +
                "<reference>\n" +
                "<qualifier>PR</qualifier>\n" +
                "<flightNumber>1</flightNumber>\n" +
                "</reference>\n" +
                "<segmentName>NM</segmentName>\n" +
                "</elementManagementPassenger>\n" +
                "<passengerData>\n" +
                "<travellerInformation>\n" +
                "<traveller>\n" +
                "<surname>TESTCHILD</surname>\n" +
                "<quantity>1</quantity>\n" +
                "</traveller>\n" +
                "<passenger>\n" +
                "<firstName>TESTCHILD MSTR</firstName>\n" +
                "<type>CHD</type>\n" +
                "</passenger>\n" +
                "</travellerInformation>\n" +
                "<dateOfBirth>\n" +
                "<dateAndTimeDetails>\n" +
                "<date>16NOV14</date>\n" +
                "</dateAndTimeDetails>\n" +
                "</dateOfBirth>\n" +
                "</passengerData>\n" +
                "</travellerInfo>\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "<travellerInfo>\n" +
                "<elementManagementPassenger>\n" +
                "<reference>\n" +
                "<qualifier>PR</qualifier>\n" +
                "<flightNumber>2</flightNumber>\n" +
                "</reference>\n" +
                "<segmentName>NM</segmentName>\n" +
                "</elementManagementPassenger>\n" +
                "<passengerData>\n" +
                "<travellerInformation>\n" +
                "<traveller>\n" +
                "<surname>TEST</surname>\n" +
                "<quantity>1</quantity>\n" +
                "</traveller>\n" +
                "<passenger>\n" +
                "<firstName>TESTCHLD MISS</firstName>\n" +
                "<type>CHD</type>\n" +
                "</passenger>\n" +
                "</travellerInformation>\n" +
                "<dateOfBirth>\n" +
                "<dateAndTimeDetails>\n" +
                "<date>02JAN15</date>\n" +
                "</dateAndTimeDetails>\n" +
                "</dateOfBirth>\n" +
                "</passengerData>\n" +
                "</travellerInfo>\n" +
                "\n" +
                "\n" +
                "\n" +
                "<travellerInfo>\n" +
                "<elementManagementPassenger>\n" +
                "<reference>\n" +
                "<qualifier>PR</qualifier>\n" +
                "<flightNumber>3</flightNumber>\n" +
                "</reference>\n" +
                "<segmentName>NM</segmentName>\n" +
                "</elementManagementPassenger>\n" +
                "<passengerData>\n" +
                "<travellerInformation>\n" +
                "<traveller>\n" +
                "<surname>KUMAR AHUJA</surname>\n" +
                "<quantity>2</quantity>\n" +
                "</traveller>\n" +
                "<passenger>\n" +
                "<firstName>VINOD MR</firstName>\n" +
                "<infantIndicator>3</infantIndicator>\n" +
                "</passenger>\n" +
                "</travellerInformation>\n" +
                "</passengerData>\n" +
                "<passengerData>\n" +
                "<travellerInformation>\n" +
                "<traveller>\n" +
                "<surname>TEST</surname>\n" +
                "</traveller>\n" +
                "<passenger>\n" +
                "<firstName>TESTINFTMSTR</firstName>\n" +
                "<type>INF</type>\n" +
                "</passenger>\n" +
                "</travellerInformation>\n" +
                "<dateOfBirth>\n" +
                "<dateAndTimeDetails>\n" +
                "<date>18JAN19</date>\n" +
                "</dateAndTimeDetails>\n" +
                "</dateOfBirth>\n" +
                "</passengerData>\n" +
                "</travellerInfo>\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "<travellerInfo>\n" +
                "<elementManagementPassenger>\n" +
                "<reference>\n" +
                "<qualifier>PR</qualifier>\n" +
                "<flightNumber>4</flightNumber>\n" +
                "</reference>\n" +
                "<segmentName>NM</segmentName>\n" +
                "</elementManagementPassenger>\n" +
                "<passengerData>\n" +
                "<travellerInformation>\n" +
                "<traveller>\n" +
                "<surname>AHUJA</surname>\n" +
                "<quantity>2</quantity>\n" +
                "</traveller>\n" +
                "<passenger>\n" +
                "<firstName>MADHU MRS</firstName>\n" +
                "<infantIndicator>3</infantIndicator>\n" +
                "</passenger>\n" +
                "</travellerInformation>\n" +
                "</passengerData>\n" +
                "<passengerData>\n" +
                "<travellerInformation>\n" +
                "<traveller>\n" +
                "<surname>TESTINFANT</surname>\n" +
                "</traveller>\n" +
                "<passenger>\n" +
                "<firstName>T</firstName>\n" +
                "<type>INF</type>\n" +
                "</passenger>\n" +
                "</travellerInformation>\n" +
                "<dateOfBirth>\n" +
                "<dateAndTimeDetails>\n" +
                "<date>17MAR18</date>\n" +
                "</dateAndTimeDetails>\n" +
                "</dateOfBirth>\n" +
                "</passengerData>\n" +
                "</travellerInfo>\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "      <originDestinationDetails>\n" +
                "        <originDestination/>\n" +
                "        <itineraryInfo>\n" +
                "          <elementManagementItinerary>\n" +
                "            <reference>\n" +
                "              <qualifier>ST</qualifier>\n" +
                "              <flightNumber>1</flightNumber>\n" +
                "            </reference>\n" +
                "            <segmentName>AIR</segmentName>\n" +
                "            <lineNumber>2</lineNumber>\n" +
                "          </elementManagementItinerary>\n" +
                "          <travelProduct>\n" +
                "            <product>\n" +
                "              <depDate>071215</depDate>\n" +
                "              <depTime>1610</depTime>\n" +
                "              <arrDate>071215</arrDate>\n" +
                "              <arrTime>1835</arrTime>\n" +
                "            </product>\n" +
                "            <boardpointDetail>\n" +
                "              <cityCode>DEL</cityCode>\n" +
                "            </boardpointDetail>\n" +
                "            <offpointDetail>\n" +
                "              <cityCode>DXB</cityCode>\n" +
                "            </offpointDetail>\n" +
                "            <companyDetail>\n" +
                "              <identification>EK</identification>\n" +
                "            </companyDetail>\n" +
                "            <productDetails>\n" +
                "              <identification>517</identification>\n" +
                "              <classOfService>V</classOfService>\n" +
                "            </productDetails>\n" +
                "            <typeDetail>\n" +
                "              <detail>ET</detail>\n" +
                "            </typeDetail>\n" +
                "          </travelProduct>\n" +
                "          <itineraryMessageAction>\n" +
                "            <business>\n" +
                "              <function>1</function>\n" +
                "            </business>\n" +
                "          </itineraryMessageAction>\n" +
                "          <relatedProduct>\n" +
                "            <quantity>1</quantity>\n" +
                "            <status>HK</status>\n" +
                "          </relatedProduct>\n" +
                "          <flightDetail>\n" +
                "            <productDetails>\n" +
                "              <equipment>772</equipment>\n" +
                "              <numOfStops>0</numOfStops>\n" +
                "              <duration>0355</duration>\n" +
                "              <weekDay>1</weekDay>\n" +
                "            </productDetails>\n" +
                "            <departureInformation>\n" +
                "              <departTerminal>3</departTerminal>\n" +
                "            </departureInformation>\n" +
                "            <arrivalStationInfo>\n" +
                "              <terminal>3</terminal>\n" +
                "            </arrivalStationInfo>\n" +
                "            <mileageTimeDetails>\n" +
                "              <flightLegMileage>1362</flightLegMileage>\n" +
                "              <unitQualifier>M</unitQualifier>\n" +
                "            </mileageTimeDetails>\n" +
                "            <facilities>\n" +
                "              <entertainement>M</entertainement>\n" +
                "              <entertainementDescription>M</entertainementDescription>\n" +
                "            </facilities>\n" +
                "          </flightDetail>\n" +
                "          <cabinDetails>\n" +
                "            <cabinDetails>\n" +
                "              <classDesignator>M</classDesignator>\n" +
                "            </cabinDetails>\n" +
                "          </cabinDetails>\n" +
                "          <selectionDetails>\n" +
                "            <selection>\n" +
                "              <option>P2</option>\n" +
                "            </selection>\n" +
                "          </selectionDetails>\n" +
                "          <itineraryfreeFormText>\n" +
                "            <freetextDetail>\n" +
                "              <subjectQualifier>3</subjectQualifier>\n" +
                "            </freetextDetail>\n" +
                "            <text>ADD ADVANCE PASSENGER INFORMATION IN SSR DOCS</text>\n" +
                "            <text>SEE RTSVC</text>\n" +
                "          </itineraryfreeFormText>\n" +
                "          <legInfo>\n" +
                "            <markerLegInfo/>\n" +
                "            <legTravelProduct>\n" +
                "              <flightDate>\n" +
                "                <departureDate>071215</departureDate>\n" +
                "                <departureTime>1610</departureTime>\n" +
                "                <arrivalDate>071215</arrivalDate>\n" +
                "                <arrivalTime>1835</arrivalTime>\n" +
                "              </flightDate>\n" +
                "              <boardPointDetails>\n" +
                "                <trueLocationId>DEL</trueLocationId>\n" +
                "              </boardPointDetails>\n" +
                "              <offpointDetails>\n" +
                "                <trueLocationId>DXB</trueLocationId>\n" +
                "              </offpointDetails>\n" +
                "            </legTravelProduct>\n" +
                "            <interactiveFreeText>\n" +
                "              <freeTextQualification>\n" +
                "                <textSubjectQualifier>ACO</textSubjectQualifier>\n" +
                "              </freeTextQualification>\n" +
                "              <freeText>AIRCRAFT OWNER EMIRATES</freeText>\n" +
                "            </interactiveFreeText>\n" +
                "          </legInfo>\n" +
                "          <markerRailTour/>\n" +
                "        </itineraryInfo>\n" +
                "      </originDestinationDetails>\n" +
                "      <dataElementsMaster>\n" +
                "        <marker2/>\n" +
                "        <dataElementsIndiv>\n" +
                "          <elementManagementData>\n" +
                "            <segmentName>RF</segmentName>\n" +
                "          </elementManagementData>\n" +
                "          <otherDataFreetext>\n" +
                "            <freetextDetail>\n" +
                "              <subjectQualifier>3</subjectQualifier>\n" +
                "              <type>P22</type>\n" +
                "            </freetextDetail>\n" +
                "            <longFreetext>NA</longFreetext>\n" +
                "          </otherDataFreetext>\n" +
                "        </dataElementsIndiv>\n" +
                "        <dataElementsIndiv>\n" +
                "          <elementManagementData>\n" +
                "            <reference>\n" +
                "              <qualifier>OT</qualifier>\n" +
                "              <flightNumber>3</flightNumber>\n" +
                "            </reference>\n" +
                "            <segmentName>AP</segmentName>\n" +
                "            <lineNumber>3</lineNumber>\n" +
                "          </elementManagementData>\n" +
                "          <otherDataFreetext>\n" +
                "            <freetextDetail>\n" +
                "              <subjectQualifier>3</subjectQualifier>\n" +
                "              <type>5</type>\n" +
                "            </freetextDetail>\n" +
                "            <longFreetext>900000000000000</longFreetext>\n" +
                "          </otherDataFreetext>\n" +
                "        </dataElementsIndiv>\n" +
                "        <dataElementsIndiv>\n" +
                "          <elementManagementData>\n" +
                "            <reference>\n" +
                "              <qualifier>OT</qualifier>\n" +
                "              <flightNumber>4</flightNumber>\n" +
                "            </reference>\n" +
                "            <segmentName>AP</segmentName>\n" +
                "            <lineNumber>4</lineNumber>\n" +
                "          </elementManagementData>\n" +
                "          <otherDataFreetext>\n" +
                "            <freetextDetail>\n" +
                "              <subjectQualifier>3</subjectQualifier>\n" +
                "              <type>5</type>\n" +
                "            </freetextDetail>\n" +
                "            <longFreetext>SUJEET.KUMAR@RESBIRD.COM</longFreetext>\n" +
                "          </otherDataFreetext>\n" +
                "        </dataElementsIndiv>\n" +
                "        <dataElementsIndiv>\n" +
                "          <elementManagementData>\n" +
                "            <reference>\n" +
                "              <qualifier>OT</qualifier>\n" +
                "              <flightNumber>6</flightNumber>\n" +
                "            </reference>\n" +
                "            <segmentName>FM</segmentName>\n" +
                "            <lineNumber>5</lineNumber>\n" +
                "          </elementManagementData>\n" +
                "          <otherDataFreetext>\n" +
                "            <freetextDetail>\n" +
                "              <subjectQualifier>3</subjectQualifier>\n" +
                "              <type>11</type>\n" +
                "            </freetextDetail>\n" +
                "            <longFreetext>PAX *M*0</longFreetext>\n" +
                "          </otherDataFreetext>\n" +
                "        </dataElementsIndiv>\n" +
                "        <dataElementsIndiv>\n" +
                "          <elementManagementData>\n" +
                "            <reference>\n" +
                "              <qualifier>OT</qualifier>\n" +
                "              <flightNumber>7</flightNumber>\n" +
                "            </reference>\n" +
                "            <segmentName>FP</segmentName>\n" +
                "            <lineNumber>6</lineNumber>\n" +
                "          </elementManagementData>\n" +
                "          <otherDataFreetext>\n" +
                "            <freetextDetail>\n" +
                "              <subjectQualifier>3</subjectQualifier>\n" +
                "              <type>16</type>\n" +
                "            </freetextDetail>\n" +
                "            <longFreetext>CASH</longFreetext>\n" +
                "          </otherDataFreetext>\n" +
                "        </dataElementsIndiv>\n" +
                "        <dataElementsIndiv>\n" +
                "          <elementManagementData>\n" +
                "            <status>ERR</status>\n" +
                "            <reference>\n" +
                "              <qualifier>OT</qualifier>\n" +
                "              <flightNumber>10</flightNumber>\n" +
                "            </reference>\n" +
                "            <segmentName>TK</segmentName>\n" +
                "            <lineNumber>3</lineNumber>\n" +
                "          </elementManagementData>\n" +
                "          <ticketElement>\n" +
                "            <ticket>\n" +
                "              <indicator>TL</indicator>\n" +
                "              <date>122017</date>\n" +
                "            </ticket>\n" +
                "          </ticketElement>\n" +
                "          <elementErrorInformation>\n" +
                "            <errorInformation>\n" +
                "              <errorDetail>\n" +
                "                <errorCode>2298</errorCode>\n" +
                "                <qualifier>EC</qualifier>\n" +
                "                <responsibleAgency>1A</responsibleAgency>\n" +
                "              </errorDetail>\n" +
                "            </errorInformation>\n" +
                "            <elementErrorText>\n" +
                "              <freetextDetail>\n" +
                "                <subjectQualifier>3</subjectQualifier>\n" +
                "              </freetextDetail>\n" +
                "              <text>INVALID TIME</text>\n" +
                "            </elementErrorText>\n" +
                "          </elementErrorInformation>\n" +
                "        </dataElementsIndiv>\n" +
                "      </dataElementsMaster>\n" +
                "    </PNR_Reply>\n" +
                "  </soapenv:Body>\n" +
                "</soapenv:Envelope>";
    }
}
