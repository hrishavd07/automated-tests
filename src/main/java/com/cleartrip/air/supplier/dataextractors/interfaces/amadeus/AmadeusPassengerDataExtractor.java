package com.cleartrip.air.supplier.dataextractors.interfaces.amadeus;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.PassengerDataExtractor;
import com.cleartrip.air.supplier.enums.Gender;
import com.cleartrip.air.supplier.enums.PassengerType;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class AmadeusPassengerDataExtractor implements PassengerDataExtractor {

    private final Logger LOGGER = LogManager.getLogger(AmadeusPassengerDataExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public List<PassengersDetailsData> getPassengerData(TestContext testContext, String as) {

        Map<String, HashSet<String>> callDetails;
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        String itineraryId = (String) testContext.getValue(ITINERARY_ID);
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "AMADEUS-ADD_TRAVELLER", itineraryId);
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        HashSet<String> tidSet = null;
        for (Map.Entry<String, HashSet<String>> stringHashSetEntry : callDetails.entrySet()) {
            if (stringHashSetEntry.getKey().contains(as)) {
                tidSet = stringHashSetEntry.getValue();
            }
        }

        if (CollectionUtils.isEmpty(tidSet)) {
            LOGGER.error("No Tid found for commitBooking api");
            return new ArrayList<>();
        }
        //fetching only first element to set of tID as the passenger name would be same for all calls.
        String apiResponseBody = httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_res", tidSet.iterator().next());
        //  apiResponseBody = samplexml.strVal();
        List<PassengersDetailsData> passengersDetailsData = makePassengers(apiResponseBody);
        return passengersDetailsData;
    }

    private List<PassengersDetailsData> makePassengers(String apiResponseBody) {

        Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
        Node documentElement = document.getDocumentElement();
        Node pnrReply = DOMHelper.selectNode("S:Body/PNR_Reply", documentElement);
        List<PassengersDetailsData> passengerDetailsData = new ArrayList<>();
        for (Node travellerInfo : DOMHelper.selectNodes("travellerInfo", pnrReply)) {
            for (Node paxInfo : DOMHelper.selectNodes("passengerData", travellerInfo)) {
                String lastName = DOMHelper.selectNodeValue("travellerInformation/traveller/surname", paxInfo).trim();
                String type = DOMHelper.selectNodeValue("travellerInformation/passenger/type", paxInfo);
                if (StringUtils.isEmpty(type)) {
                    type = "ADT";
                }
                String firstName = DOMHelper.selectNodeValue("travellerInformation/passenger/firstName", paxInfo).trim();
                String title = firstName.substring(firstName.lastIndexOf(" ")).trim();
                String gender = getGender(title);
                String DOB = DOMHelper.selectNodeValue("dateOfBirth/dateAndTimeDetails/date", paxInfo);
                DateFormat dobInDate = new SimpleDateFormat("ddMMyyyy");
                if (StringUtils.isNotEmpty(DOB)) {
                    try {
                        Date parse = dobInDate.parse(DOB);
                        DOB = (parse.getYear() + 1900 + "-" + (parse.getMonth() + 1) + "-" + parse.getDate());
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    if (type.equalsIgnoreCase("ADT")) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeZone(TimeZone.getDefault());
                        calendar.add(Calendar.YEAR, -25);
                        Date time = calendar.getTime();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                        DOB = dateFormat.format(time);
                    }
                }
                if ((type.equalsIgnoreCase("inf") || type.equalsIgnoreCase("chd")) && firstName.length() > 4) {
                    firstName = firstName.substring(0, firstName.length() - 4).trim();
                } else if (firstName.contains(" ")) {
                    firstName = firstName.substring(0, firstName.lastIndexOf(" "));
                }
                passengerDetailsData.add(new PassengersDetailsData(firstName.toLowerCase(), lastName.toLowerCase(), PassengerType.valueOf(type), DOB, null, title, Gender.getGender(gender)));
            }
        }
        return passengerDetailsData;
    }

    private String getGender(String title) {

        switch (title.toLowerCase()) {
            case "mr":
            case "mstr":
                return "male";
            case "ms":
            case "mrs":
            case "miss":
                return "female";

        }
        return StringUtils.EMPTY;
    }
}
