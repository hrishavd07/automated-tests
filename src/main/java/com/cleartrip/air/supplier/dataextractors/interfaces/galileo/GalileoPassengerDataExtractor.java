package com.cleartrip.air.supplier.dataextractors.interfaces.galileo;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.PassengerDataExtractor;
import com.cleartrip.air.supplier.enums.Gender;
import com.cleartrip.air.supplier.enums.PassengerType;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;
import com.cleartrip.air.supplier.validatordata.PassportDetails;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class GalileoPassengerDataExtractor implements PassengerDataExtractor {
    private static final String INTL = "intl";
    private final Logger LOGGER = LogManager.getLogger(GalileoPassengerDataExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public List<PassengersDetailsData> getPassengerData(TestContext testContext, String as) {
        Map<String, HashSet<String>> callDetails;
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        String itineraryId = (String) testContext.getValue(ITINERARY_ID);
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "GALILEO-AddTraveller", itineraryId);
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        HashSet<String> tidSet = callDetails.get("tid" + "_" + as);
        if (CollectionUtils.isEmpty(tidSet)) {
            LOGGER.error("No Tid found for GALILEO-AddTraveller api, hence returning empty array list");
            return new ArrayList<>();
        }
        //fetching only first element to set of tID as the passenger name would be same for all calls.
        String apiResponseBody = httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", tidSet.iterator().next());
        List<PassengersDetailsData> passengersDetailsData = makePassengers(apiResponseBody);
        return passengersDetailsData;
    }

    private List<PassengersDetailsData> makePassengers(String apiResponseBody) {
        String travelerName = StringUtils.EMPTY;
        Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
        Node documentElement = document.getDocumentElement();
        Node airAddTravellerNode = DOMHelper.selectNode("S:Body/ns9:BookingTravelerReq/ns9:AddTraveler", documentElement);
        String bookingTravellerTagName = getTagName(airAddTravellerNode, "BookingTraveler");
        if (StringUtils.isEmpty(bookingTravellerTagName)) {
            LOGGER.error("booking Traveller tag name is empty hence returning empty passenger list");
            return new ArrayList<>();
        }
        List<PassengersDetailsData> passengerDetailsData = new ArrayList<>();
        for (Node n : DOMHelper.selectNodes(bookingTravellerTagName, airAddTravellerNode)) {
            if (StringUtils.isEmpty(travelerName)) {
                travelerName = getTagName(n, "BookingTravelerName");
            }
            ;
            if (StringUtils.isEmpty(bookingTravellerTagName)) {
                LOGGER.error("booking Traveller tag name is empty hence returning empty passenger list");
                return new ArrayList<>();
            }
            Node nameDetails = DOMHelper.selectNode(travelerName, n);
            NamedNodeMap namedNodeMapForName = nameDetails.getAttributes();
            String firstName = namedNodeMapForName.getNamedItem("First") != null ? namedNodeMapForName.getNamedItem("First").getNodeValue() : "";
            String lastName = namedNodeMapForName.getNamedItem("Last") != null ? namedNodeMapForName.getNamedItem("Last").getNodeValue() : "";
            String title = namedNodeMapForName.getNamedItem("Prefix") != null ? namedNodeMapForName.getNamedItem("Prefix").getNodeValue() : "";

            PassportDetails passportDetails = null;
            String expiryDate;
            String[] ssr = DOMHelper.selectNode("ns2:SSR", n).getAttributes().getNamedItem("FreeText").getNodeValue().split("/");
            if (ssr[0].equalsIgnoreCase("p")) {
                String DOB = ssr[6];
                DateFormat dobInDate = new SimpleDateFormat("ddMMMyy");
                try {
                    Date parse = dobInDate.parse(DOB);
                   expiryDate = parse.getYear() + 1900 + "-" + (parse.getMonth() + 1) + "-" + parse.getDate();
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
                passportDetails = new PassportDetails(ssr[2], expiryDate, ssr[1]);
            }
            NamedNodeMap namedNodeMapForDetails = n.getAttributes();
            String type = namedNodeMapForDetails.getNamedItem("TravelerType").getNodeValue().equalsIgnoreCase("CNN") ? "CHD" : namedNodeMapForDetails.getNamedItem("TravelerType").getNodeValue();
            String dob = namedNodeMapForDetails.getNamedItem("DOB") != null ? namedNodeMapForDetails.getNamedItem("DOB").getNodeValue() : "";
            String gender = namedNodeMapForDetails.getNamedItem("Gender") != null ? namedNodeMapForDetails.getNamedItem("Gender").getNodeValue() : "";
            Gender genderValue = StringUtils.isNotEmpty(gender) ?
                    (gender.equalsIgnoreCase("M") ?
                            Gender.MALE : gender.equalsIgnoreCase("F") ?
                            Gender.FEMALE : Gender.UNKNOWN) : Gender.UNKNOWN;
            passengerDetailsData.add(new PassengersDetailsData(firstName.toLowerCase(), lastName.toLowerCase(), PassengerType.valueOf(type), dob, passportDetails, title.toUpperCase(), genderValue));
        }
        return passengerDetailsData;
    }

    private String getTagName(Node airAddTravellerNode, String tag) {
        List<String> nodeNameList = new ArrayList<>();
        NodeList nodeList = airAddTravellerNode.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node n = nodeList.item(i);
            nodeNameList.add(n.getNodeName());
        }
        return nodeNameList.stream().filter(name -> name.contains(tag)).findFirst().orElse("");
    }
}
