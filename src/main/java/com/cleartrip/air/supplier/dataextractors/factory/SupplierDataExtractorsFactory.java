package com.cleartrip.air.supplier.dataextractors.factory;

import com.cleartrip.air.supplier.dataextractors.interfaces.*;
import com.cleartrip.air.supplier.validatordata.CustomerData;

public interface SupplierDataExtractorsFactory {

    public PassengerDataExtractor getPassengerDataExtractor();

    public BookingAmountExtractor getBookingAmountExtractor();

    public MealAndBaggageDetailsExtractor getMealDetailsExtractor();

    public CustomerDataExtractor getCustomerDetailsExtractor();

    public FlightDetailDataExtractor getFlightDetailsExtractor();

}
