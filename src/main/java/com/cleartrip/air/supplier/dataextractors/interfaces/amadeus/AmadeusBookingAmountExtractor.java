package com.cleartrip.air.supplier.dataextractors.interfaces.amadeus;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.BookingAmountExtractor;
import com.cleartrip.air.supplier.dataextractors.interfaces.airAsia.AirAsiaBookingAmountExtractor;
import com.cleartrip.air.supplier.testframework.SearchCriteria;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.BookingAmountData;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import com.sun.org.apache.xalan.internal.xsltc.DOM;
import httpreqreshandler.HttpReqResHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.annotation.XmlType;
import java.security.DomainCombiner;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class AmadeusBookingAmountExtractor implements BookingAmountExtractor {

    private final Logger LOGGER = LogManager.getLogger(AmadeusBookingAmountExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public BookingAmountData getBookingAmount(TestContext testContext, String as) {
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return new BookingAmountData(getQuotedAmount(apiResponseBody, testContext), 0.0);
    }

    private double getQuotedAmount(HashSet<String> apiResponseBodySet, TestContext testContext) {

        SearchCriteria searchCriteria = (SearchCriteria) testContext.getValue("searchCriteria");

        List<Double> amount = new ArrayList<>();
        apiResponseBodySet.forEach(apiResponseBody -> {
            Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
            Node documentElement = document.getDocumentElement();
            Node fareDataInformation = DOMHelper.selectNode("S:Body/Fare_PricePNRWithBookingClassReply", documentElement);

            for (Node fare : DOMHelper.selectNodes("fareList", fareDataInformation)) {
                String type = DOMHelper.selectNodeValue("segmentInformation/fareQualifier/fareBasisDetails/discTktDesignator", fare);
                int count = getPaxCountFromType(type, searchCriteria);
                for (Node dataInformation : DOMHelper.selectNodes("fareDataInformation/fareDataSupInformation", fare)) {
                    if (DOMHelper.selectNodeValue("fareDataQualifier", dataInformation).equalsIgnoreCase("712")) {
                        amount.add(DOMHelper.selectNodeDoubleValue("fareAmount", dataInformation) * count);
                    }
                }
            }

        });
        return getTotal(amount);

    }

    private int getPaxCountFromType(String type, SearchCriteria searchCriteria) {
        switch (type) {
            case "ADT":
                return searchCriteria.getAdultCount();
            case "CH":
                return searchCriteria.getChdCount();
            case "IN":
                return searchCriteria.getInfCount();
            default:
                return 0;
        }
    }

    private Double getTotal(List<Double> amount) {
        Double total = 0.0;
        for (Double amt : amount) {
            total = total + amt;
        }
        return total;
    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler
            httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        // This call return all api calls with name updatePassenger for all suppliers
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "AMADEUS-PNRWBC", itineraryId);
        if (!callDetails.isEmpty()) {

            HashSet<String> tid = callDetails.get("tid" + "_" + as);
            HashSet<String> apiResponseBodySet = new HashSet<>();
            //fetching response body for each tIds
            tid.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_res", id)));
            return apiResponseBodySet;
        } else {
            LOGGER.error("NO API CALL FOUND FOR AMADEUS-PNRWBC");
            return new HashSet<>();
        }
    }
}
