package com.cleartrip.air.supplier.dataextractors.interfaces.amadeus;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DateUtil;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.FlightDetailDataExtractor;
import com.cleartrip.air.supplier.dataextractors.interfaces.galileo.GalileoFlightDetailsExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.*;
import com.sun.org.apache.xalan.internal.xsltc.DOM;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.time.ZonedDateTime;
import java.util.*;

public class AmadeusFlightDetailsExtractor implements FlightDetailDataExtractor {

    private final String ITINERARY_ID = "itineraryId";
    public static final Logger LOGGER = LogManager.getLogger(AmadeusFlightDetailsExtractor.class);


    @Override
    public Map<String, FlightSegment> getFlightDetails(TestContext testContext, String as) {
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return getApiFlightDetails(apiResponseBody);
    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "AMADEUS-ASFR", itineraryId);
        HashSet<String> tidSet = callDetails.get("tid" + "_" + as);
        if (CollectionUtils.isEmpty(tidSet)) {
            LOGGER.error("No Tid found for updatePassenger api");
            return new HashSet<>();
        }
        HashSet<String> apiResponseBodySet = new HashSet<>();
        //fetching response body for each tIds
        tidSet.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_res", id)));
        return apiResponseBodySet;
    }

    private Map<String, FlightSegment> getApiFlightDetails(HashSet<String> apiResponseBodySet) {
        Map<String, FlightSegment> flightSegmentMap = new HashMap<>();
        apiResponseBodySet.stream()
                .map(DocumentReaderUtils::readDocumentForApiResponseBody)
                .map(Document::getDocumentElement)
                .map(documentElement -> DOMHelper.selectNode("S:Body/Air_SellFromRecommendationReply/itineraryDetails", documentElement))
                .forEach(addAirSegment -> {
                    for (Node airSegment : DOMHelper.selectNodes("segmentInformation", addAirSegment)) {
                        /*Node travelInformationNode = DOMHelper.selectNode("ns9:travelProductInformation", airSegment);*/

                        String departureTime = getDepartureTime(DOMHelper.selectNode("flightDetails/flightDate", airSegment));
                        String arrivalTime = getArrivalTime(DOMHelper.selectNode("flightDetails/flightDate", airSegment));

                        String origin = DOMHelper.selectNodeValue("flightDetails/boardPointDetails/trueLocationId", airSegment);
                        String destination = DOMHelper.selectNodeValue("flightDetails/offpointDetails/trueLocationId", airSegment);
                        String flightNumber = DOMHelper.selectNodeValue("flightDetails/flightIdentification/flightNumber",airSegment);
                        String carrierCode = DOMHelper.selectNodeValue("flightDetails/companyDetails/marketingCompany",airSegment);


                        addSegmentToMap(flightSegmentMap, origin, destination, departureTime, arrivalTime, flightNumber, carrierCode);
                    }
                });
        return flightSegmentMap;
    }

    private String getArrivalTime(Node selectNode) {
        List<String> nodeNamesList = new ArrayList<>();
        for(int i=0 ; i< selectNode.getChildNodes().getLength(); i++){
            nodeNamesList.add(selectNode.getChildNodes().item(i).getNodeName());
        }
        String arrivalDate = nodeNamesList.contains("arrivalDate") ?
                DOMHelper.selectNodeValue("arrivalDate", selectNode) : DOMHelper.selectNodeValue("departureDate", selectNode);
        String arrivalTime = DOMHelper.selectNodeValue("arrivalTime", selectNode);
        return arrivalDate + 'T' + arrivalTime + "00";
    }

    private String getDepartureTime(Node selectNode) {
        String departDate = DOMHelper.selectNodeValue("departureDate", selectNode);
        String departTime = DOMHelper.selectNodeValue("departureTime", selectNode);
        return departDate + 'T' + departTime + "00";

    }

    private void addSegmentToMap(Map<String, FlightSegment> flightSegmentMap, String origin, String destination, String departureTime, String arrivalTime, String flightNumber, String carrierCode) {

        Flight flight = new Flight(flightNumber, carrierCode, null, null);
        ZonedDateTime departTime = DateUtil.getZonedDateTime(DateUtil.convertToDate(departureTime));
        DepartureDetails departureDetails = new DepartureDetails(new Airport(origin), departTime);

        ZonedDateTime arrivalZonedTime = DateUtil.getZonedDateTime(DateUtil.convertToDate(arrivalTime));
        ArrivalDetails arrivalDetails = new ArrivalDetails(new Airport(destination), arrivalZonedTime);

        Airline operatingAirline = new Airline(carrierCode, flightNumber);
        FlightSegment flightSegment = new FlightSegment(flight, departureDetails, arrivalDetails, operatingAirline, operatingAirline, Optional.empty(), null);

        flightSegmentMap.put(origin + "_" + destination, flightSegment);
    }
}
