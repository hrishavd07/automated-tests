package com.cleartrip.air.supplier.dataextractors.interfaces.airAsia;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DateUtil;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.FlightDetailDataExtractor;
import com.cleartrip.air.supplier.dataextractors.interfaces.navitaire.NavitaireCustomerDetailsDataExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.*;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.lang.reflect.Array;
import java.util.*;

public class AirAsiaFlightDetailsExtractor implements FlightDetailDataExtractor {
    private final Logger LOGGER = LogManager.getLogger(NavitaireCustomerDetailsDataExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public Map<String, FlightSegment> getFlightDetails(TestContext testContext, String as) {
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return getApiFlightDetails(apiResponseBody);
    }

    private Map<String, FlightSegment> getApiFlightDetails(HashSet<String> apiResponseBodySet) {
        Map<String, FlightSegment> flightSegmentMap = new HashMap<>();

        apiResponseBodySet.forEach(apiResponseBody -> {
            Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
            Node documentElement = document.getDocumentElement();
            Node journeySellKeys = DOMHelper.selectNode("soapenv:Body/Sell/objSellRequestData/SellJourneyRequest/SellJourneyRequestData/Journeys/SellJourney/Segments", documentElement);
            NodeList childNodes = journeySellKeys.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                String arrivalStation = DOMHelper.selectNodeValue("ArrivalStation", childNodes.item(i));
                String departureStation = DOMHelper.selectNodeValue("DepartureStation", childNodes.item(i));
                String arrivalTime = DOMHelper.selectNodeValue("STA", childNodes.item(i));
                String departureTime = DOMHelper.selectNodeValue("STD", childNodes.item(i));
                String flightNumber = DOMHelper.selectNodeValue("FlightDesignator/FlightNumber", childNodes.item(i));
                String CarrierCode = DOMHelper.selectNodeValue("FlightDesignator/CarrierCode", childNodes.item(i));
                Flight flight = new Flight(flightNumber,CarrierCode,null,null);
                DepartureDetails departureDetails = new DepartureDetails(new Airport(departureStation),DateUtil.getZonedDateTime(DateUtil.convertStringToDate(departureTime)));
                ArrivalDetails arrivalDetails = new ArrivalDetails(new Airport(arrivalStation),DateUtil.getZonedDateTime(DateUtil.convertStringToDate(arrivalTime)));
                Airline airline = new Airline(CarrierCode, flightNumber);
                FlightSegment flightSegment = new FlightSegment(flight, departureDetails, arrivalDetails,airline,airline,Optional.empty(), null);
                flightSegmentMap.put(departureStation+"_"+arrivalStation,flightSegment);
            }
        });
        return flightSegmentMap;
    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "sell", itineraryId);
        HashSet<String> tidSet = callDetails.get("tid" + "_" + as);
        if (CollectionUtils.isEmpty(tidSet)) {
            LOGGER.error("No Tid found for updatePassenger api");
            return new HashSet<>();
        }
        HashSet<String> apiResponseBodySet = new HashSet<>();
        //fetching response body for each tIds
        tidSet.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", id)));
        return apiResponseBodySet;
    }
}
