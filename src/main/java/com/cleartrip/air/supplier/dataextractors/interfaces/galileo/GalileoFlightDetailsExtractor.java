package com.cleartrip.air.supplier.dataextractors.interfaces.galileo;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DateUtil;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.FlightDetailDataExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.*;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

public class GalileoFlightDetailsExtractor implements FlightDetailDataExtractor {

    private final String ITINERARY_ID = "itineraryId";
    public static final Logger LOGGER = LogManager.getLogger(GalileoFlightDetailsExtractor.class);
    @Override
    public Map<String, FlightSegment> getFlightDetails(TestContext testContext, String as) {
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return getApiFlightDetails(apiResponseBody);
    }

    private Map<String, FlightSegment> getApiFlightDetails(HashSet<String> apiResponseBodySet) {
        Map<String, FlightSegment> flightSegmentMap = new HashMap<>();
        apiResponseBodySet.stream()
                .map(DocumentReaderUtils::readDocumentForApiResponseBody)
                .map(Document::getDocumentElement)
                .map(documentElement -> DOMHelper.selectNode("S:Body/ns9:BookingAirSegmentReq/ns9:AddAirSegment", documentElement))
                .forEach(addAirSegment -> {
            for (Node airSegment : DOMHelper.selectNodes("ns5:AirSegment", addAirSegment)) {
                NamedNodeMap segmentDetails = airSegment.getAttributes();
                String origin = segmentDetails.getNamedItem("Origin") != null ? segmentDetails.getNamedItem("Origin").getNodeValue() : "";
                String destination = segmentDetails.getNamedItem("Destination") != null ? segmentDetails.getNamedItem("Destination").getNodeValue() : "";
                String departureTime = segmentDetails.getNamedItem("DepartureTime") != null ? segmentDetails.getNamedItem("DepartureTime").getNodeValue() : "";
                String arrivalTime = segmentDetails.getNamedItem("ArrivalTime") != null ? segmentDetails.getNamedItem("ArrivalTime").getNodeValue() : "";
                String flightNumber = segmentDetails.getNamedItem("FlightNumber") != null ? segmentDetails.getNamedItem("FlightNumber").getNodeValue() : "";
                String carrierCode = segmentDetails.getNamedItem("Carrier") != null ? segmentDetails.getNamedItem("Carrier").getNodeValue() : "";
                addSegmentToMap(flightSegmentMap, origin, destination, departureTime, arrivalTime, flightNumber, carrierCode);
            }
        });
        return flightSegmentMap;
    }

    private void addSegmentToMap(Map<String, FlightSegment> flightSegmentMap, String origin, String destination, String departureTime, String arrivalTime, String flightNumber, String carrierCode) {

        Flight flight = new Flight(flightNumber, carrierCode, null, null);
        ZonedDateTime departTime = DateUtil.getZonedDateTime(DateUtil.convertStringToDate(departureTime));
        DepartureDetails departureDetails = new DepartureDetails(new Airport(origin), departTime);

        ZonedDateTime arrivalZonedTime = DateUtil.getZonedDateTime(DateUtil.convertStringToDate(arrivalTime));
        ArrivalDetails arrivalDetails = new ArrivalDetails(new Airport(destination), arrivalZonedTime);

        Airline operatingAirline = new Airline(carrierCode, flightNumber);
        FlightSegment flightSegment = new FlightSegment(flight, departureDetails, arrivalDetails, operatingAirline, operatingAirline, Optional.empty(), null);

        flightSegmentMap.put(origin + "_" + destination, flightSegment);
    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "GALILEO-AddAirSegment", itineraryId);
        HashSet<String> tidSet = callDetails.get("tid" + "_" + as);
        if (CollectionUtils.isEmpty(tidSet)) {
            LOGGER.error("No Tid found for updatePassenger api");
            return new HashSet<>();
        }
        HashSet<String> apiResponseBodySet = new HashSet<>();
        //fetching response body for each tIds
        tidSet.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", id)));
        return apiResponseBodySet;
    }
}
