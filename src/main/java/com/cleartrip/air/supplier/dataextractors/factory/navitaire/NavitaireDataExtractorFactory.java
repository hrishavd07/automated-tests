package com.cleartrip.air.supplier.dataextractors.factory.navitaire;

import com.cleartrip.air.supplier.dataextractors.interfaces.*;
import com.cleartrip.air.supplier.dataextractors.interfaces.navitaire.*;
import com.cleartrip.air.supplier.dataextractors.factory.SupplierDataExtractorsFactory;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.FlightSegment;

import java.util.Map;

public class NavitaireDataExtractorFactory implements SupplierDataExtractorsFactory {

    @Override
    public PassengerDataExtractor getPassengerDataExtractor() {
        return new NavitairePassengerDataExtractor();
    }

    @Override
    public BookingAmountExtractor getBookingAmountExtractor() {
        return new NavitaireBookingAmountExtractor();
    }

    @Override
    public MealAndBaggageDetailsExtractor getMealDetailsExtractor() {
        return new NavitaireMealAndBaggageDetailsExtractor();
    }

    @Override
    public CustomerDataExtractor getCustomerDetailsExtractor(){
     return  new NavitaireCustomerDetailsDataExtractor();
    }

    @Override
    public FlightDetailDataExtractor getFlightDetailsExtractor() {
        return new NavitaireFlightDetailDataExtractor();
    }

}
