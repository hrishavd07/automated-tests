package com.cleartrip.air.supplier.dataextractors.factory.airAsia;

import com.cleartrip.air.supplier.dataextractors.factory.SupplierDataExtractorsFactory;
import com.cleartrip.air.supplier.dataextractors.interfaces.*;
import com.cleartrip.air.supplier.dataextractors.interfaces.airAsia.*;

public class AirAsiaDataExtractorFactory implements SupplierDataExtractorsFactory {
    @Override
    public PassengerDataExtractor getPassengerDataExtractor() {
        return new AirAsiaPassengerDataExtractor();
    }

    @Override
    public BookingAmountExtractor getBookingAmountExtractor() {
        return new AirAsiaBookingAmountExtractor();
    }

    @Override
    public MealAndBaggageDetailsExtractor getMealDetailsExtractor() {
        return new AirAsiaMealAndBaggageDetailsExtractor();
    }

    @Override
    public CustomerDataExtractor getCustomerDetailsExtractor() {
        return new AirAsiaCustomerDetailsExtractor();
    }

    @Override
    public FlightDetailDataExtractor getFlightDetailsExtractor() {
        return new AirAsiaFlightDetailsExtractor();
    }
}
