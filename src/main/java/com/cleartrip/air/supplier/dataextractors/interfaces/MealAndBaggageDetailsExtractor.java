package com.cleartrip.air.supplier.dataextractors.interfaces;

import com.cleartrip.air.supplier.testframework.TestContext;

import java.util.List;

public interface MealAndBaggageDetailsExtractor {

    public List<String> getMealAndBaggageData(TestContext testContext, String as);

}
