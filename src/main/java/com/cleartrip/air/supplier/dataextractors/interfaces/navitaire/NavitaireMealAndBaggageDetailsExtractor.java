package com.cleartrip.air.supplier.dataextractors.interfaces.navitaire;

import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.dataextractors.interfaces.MealAndBaggageDetailsExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.DOMHelper;
import httpreqreshandler.HttpReqResHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class NavitaireMealAndBaggageDetailsExtractor implements MealAndBaggageDetailsExtractor {

    Logger LOGGER = LogManager.getLogger(NavitaireMealAndBaggageDetailsExtractor.class);
    private final String ITINERARY_ID = "itineraryId";

    @Override
    public List<String> getMealAndBaggageData(TestContext testContext, String as) {
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        HashSet<String> apiResponseBody = fetchApiResponseBody((String) testContext.getValue(ITINERARY_ID), as, httpReqResHandler, statsResponse);
        return getMealCodeList(apiResponseBody);
    }

    private List<String> getMealCodeList(HashSet<String> apiResponseBodySet) {
        List<String> mealBaggageCodeList = new ArrayList<>();
        apiResponseBodySet.forEach(apiResponseBody -> {
            mealBaggageCodeList.addAll(fetchMealCode(apiResponseBody));
        });
        return mealBaggageCodeList;
    }

    private List<String> fetchMealCode(String apiResponseBody) {
        List<String> codes = new ArrayList<>();
        Document document = DocumentReaderUtils.readDocumentForApiResponseBody(apiResponseBody);
        Node documentElement = document.getDocumentElement();
        Node sellRequest = DOMHelper.selectNode("soapenv:Body/SellRequest/SellRequestData/ns6:SellSSR/ns6:SSRRequest/ns6:SegmentSSRRequests", documentElement);
        for (Node selectNode : DOMHelper.selectNodes("ns6:SegmentSSRRequest", sellRequest)) {
            Node paxSSRs = DOMHelper.selectNode("ns6:PaxSSRs", selectNode);
            for (Node paxSSR : DOMHelper.selectNodes("ns6:PaxSSR", paxSSRs)) {
                codes.add(DOMHelper.selectNodeValue("ns6:SSRCode", paxSSR));
            }
        }
        return codes;

    }

    private HashSet<String> fetchApiResponseBody(String itineraryId, String as, HttpReqResHandler httpReqResHandler, StatsResponse statsResponse) {
        Map<String, HashSet<String>> callDetails;
        // This call return all api calls with name updatePassenger for all suppliers
        callDetails = statsResponse.getApiCallDetails(statsResponse.getJsonNode("air_api_calls"), "AIR_AMEND", "SellSsrMealBag", itineraryId);
       if(!callDetails.isEmpty()) {
           HashSet<String> tid = callDetails.get("tid" + "_" + as);
           HashSet<String> apiResponseBodySet = new HashSet<>();
           //fetching response body for each tIds
           tid.forEach(id -> apiResponseBodySet.add(httpReqResHandler.getResponseBody(itineraryId, "AIR_AMEND", "api_req", id)));
           return apiResponseBodySet;
       }
       else{
           LOGGER.error("MEAL OR BAGGAGE WASN'T SELECTED");
           return new HashSet<>();
       }
       }
}
