package com.cleartrip.air.supplier.dataextractors.interfaces.amadeus;

import com.cleartrip.air.supplier.dataextractors.interfaces.MealAndBaggageDetailsExtractor;
import com.cleartrip.air.supplier.testframework.TestContext;

import java.util.ArrayList;
import java.util.List;

public class AmadeusMealAndBaggageDetailsExtractor implements MealAndBaggageDetailsExtractor {
    @Override
    public List<String> getMealAndBaggageData(TestContext testContext, String as) {
        return new ArrayList<>();
    }
}
