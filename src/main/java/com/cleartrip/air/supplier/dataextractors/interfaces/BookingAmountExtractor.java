package com.cleartrip.air.supplier.dataextractors.interfaces;

import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.BookingAmountData;

public interface BookingAmountExtractor {

    public BookingAmountData getBookingAmount(TestContext testContext, String as);
}
