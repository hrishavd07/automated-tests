package com.cleartrip.air.supplier.dataextractors.interfaces;

import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.CustomerData;

import java.util.List;

public interface CustomerDataExtractor {
    List<CustomerData> getCustomerData(TestContext testContext, String as);
}
