package com.cleartrip.air.supplier.customexceptions;

import com.cleartrip.air.supplier.enums.ErrorCode;

public class XMLParsingException extends RuntimeException {
    private final String message;
    private final ErrorCode errorCode;

    public XMLParsingException(ErrorCode errorCode, String errorMessage){
        super(errorMessage);
        this.errorCode = errorCode;
        this.message = errorMessage;
    }

    public XMLParsingException(Throwable cause, ErrorCode errorCode, String errorMessage){
        super(errorMessage, cause);
        this.errorCode = errorCode;
        this.message = errorMessage;
    }

}
