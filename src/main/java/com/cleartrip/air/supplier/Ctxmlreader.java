package com.cleartrip.air.supplier;

import com.cleartrip.air.supplier.Utils.DateUtil;
import com.cleartrip.air.supplier.Utils.DocumentReaderUtils;
import com.cleartrip.air.supplier.Utils.TestUtils;
import com.cleartrip.air.supplier.enums.AirSupplier;
import com.cleartrip.air.supplier.enums.AllowedTax;
import com.cleartrip.air.supplier.enums.Gender;
import com.cleartrip.air.supplier.enums.PassengerType;
import com.cleartrip.air.supplier.testframework.TestContext;
import com.cleartrip.air.supplier.validatordata.*;
import httpreqreshandler.HttpReqResHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.*;

import static java.util.Arrays.asList;

public class Ctxmlreader {
    public final Map<String, String> stepResponseBodyMap;

    Logger LOGGER = LogManager.getLogger(Ctxmlreader.class);
    private final String PREPAYMENT = "PREPAYMENT";
    private final String BOOK = "BOOK";
    private final String SEATSELL1 = "SEATSELL1";

    public Ctxmlreader(TestContext testContext) {
        this.stepResponseBodyMap = prepareStepResponseBodyMap(testContext);
    }

    private Map<String, String> prepareStepResponseBodyMap(TestContext testContext) {
        Map<String, String> stepResponseBodyMap = new HashMap<>();
        LOGGER.info("Reading prepayment and book body and storing it in text context");
        stepResponseBodyMap.put(PREPAYMENT, preparePrepaymentBody(testContext));
        stepResponseBodyMap.put(BOOK, prepareBookBody(testContext));
        stepResponseBodyMap.put(SEATSELL1, prepareSeatSellOneBody(testContext));
        return stepResponseBodyMap;
    }

    private String prepareBookBody(TestContext testContext) {
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        Map<String, String> response = statsResponse.getAirBookCallInAirCTCalls();
        String tid = response.get("tid");
        String itineraryId = response.get("itinerary");
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        return httpReqResHandler.getResponseBody(itineraryId, "AIR_BOOK", "ct_res", tid);
    }

    private String preparePrepaymentBody(TestContext testContext) {
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        Map<String, String> response = statsResponse.getAirPrepaymentCallInAirCtCalls();
        String tid = response.get("tid");
        String itineraryId = response.get("itinerary");
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        return httpReqResHandler.getResponseBody(itineraryId, "AIR_PREPAYMENT", "ct_req", tid);
    }

    private String prepareSeatSellOneBody(TestContext testContext) {
        StatsResponse statsResponse = (StatsResponse) testContext.getValue("statsObject");
        Map<String, String> response = statsResponse.getAirSeatSellOneCallInAirCtCalls();
        String tid = response.get("tid");
        String itineraryId = response.get("itinerary");
        HttpReqResHandler httpReqResHandler = new HttpReqResHandler();
        return httpReqResHandler.getResponseBody(itineraryId, "AIR_SEATSELL1", "ct_req", tid);
    }


    public List<PassengersDetailsData> getPassengerDetails() {
        Document document = DocumentReaderUtils.readDocumentForStep(stepResponseBodyMap, PREPAYMENT);
        Node documentElement = document.getDocumentElement();
        Node airBookingNode = DOMHelper.selectNode("air-bookings/air-booking", documentElement);
        List<PassengersDetailsData> passengerDetailsData = new ArrayList<>();
        for (Node paxInfoNode : DOMHelper.selectNodes("pax-list/pax-info", airBookingNode)) {

            String firstName = DOMHelper.selectNodeValue("first-name", paxInfoNode);
            String lastName = DOMHelper.selectNodeValue("last-name", paxInfoNode);
            String travellerType = DOMHelper.selectNodeValue("pax-type-code", paxInfoNode);
            String dobStr = DOMHelper.selectNodeValue("date-of-birth", paxInfoNode);
            String title = DOMHelper.selectNodeValue("title", paxInfoNode);
            Gender gender = Gender.getGender(getgenderFromTitle(title));

            PassportDetails passport = null;
            for (Node passportDetails : DOMHelper.selectNodes("passport-detail", paxInfoNode)) {

                String passportNumber = DOMHelper.selectNodeValue("passport-number", passportDetails);
                String expiryDate = DOMHelper.selectNodeValue("date-of-expiry", passportDetails);
                String issusingCountry = DOMHelper.selectNodeValue("issuing-country", passportDetails);
                passport = new PassportDetails(passportNumber, expiryDate, issusingCountry);
            }
            //TODO: make date in date format, keeping it now as string.
            if (travellerType.equalsIgnoreCase("ADT") && StringUtils.isEmpty(dobStr)) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeZone(TimeZone.getDefault());
                calendar.add(Calendar.YEAR, -25);
                Date time = calendar.getTime();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                dobStr = dateFormat.format(time);
            }
            LOGGER.info("Creating passengers details object with fetched passengers data from trip XML "
                    + firstName + " "
                    + lastName + " "
                    + travellerType + " "
                    + dobStr + " ");
                   // + "Passport Details: "
                 //   + passport==null?"Passport info not available":passport.toString());

            passengerDetailsData.add(new PassengersDetailsData(firstName.toLowerCase(),
                    lastName.toLowerCase(),
                    PassengerType.getPseudoValueOf(travellerType),
                    dobStr,
                    passport,
                    title.toUpperCase(), gender));
        }
        return passengerDetailsData;
    }

    private String getgenderFromTitle(String title) {

        switch (title.toLowerCase()) {
            case "mr":
            case "mstr":
                return "Male";
            case "ms":
            case "mrs":
            case "miss":
                return "Female";
            default:
                return "unknown";
        }
    }

    public Map<Integer, Double> pricingInfoTotal() {
        Map<Integer, Double> pricingIndexTotal = new HashMap<>();
        Document document = DocumentReaderUtils.readDocumentForStep(stepResponseBodyMap, PREPAYMENT);
        Node documentElement = document.getDocumentElement();
        Node airBookingNode = DOMHelper.selectNode("air-bookings/air-booking", documentElement);
        for (Node bookingInfoList : DOMHelper.selectNodes("booking-info-list", airBookingNode)) {
            for (Node bookingInfo : DOMHelper.selectNodes("booking-info", bookingInfoList)) {
                pricingIndexTotal.put(DOMHelper.selectNodeIntValue("pricing-info-seq-no", bookingInfo), getTotal(DOMHelper.selectNodeIntValue("pricing-info-seq-no", bookingInfo)));
            }
        }
        return pricingIndexTotal;
    }

    public List<Double> getPricingInfoIndexFromBookingInfo(int segmentNumber, Map<Integer, Double> pricingIndexTotal) {
        List<Double> totalFarePerSegment = new ArrayList<>();
        Document document = DocumentReaderUtils.readDocumentForStep(stepResponseBodyMap, PREPAYMENT);
        Node documentElement = document.getDocumentElement();
        Node airBookingNode = DOMHelper.selectNode("air-bookings/air-booking", documentElement);
        for (Node bookingInfoList : DOMHelper.selectNodes("booking-info-list", airBookingNode)) {
            for (Node bookingInfo : DOMHelper.selectNodes("booking-info", bookingInfoList)) {
                if (DOMHelper.selectNodeIntValue("segment-seq-no", bookingInfo) == segmentNumber) {
                    totalFarePerSegment.add(pricingIndexTotal.get(DOMHelper.selectNodeIntValue("pricing-info-seq-no", bookingInfo)));
                    pricingIndexTotal.put(DOMHelper.selectNodeIntValue("pricing-info-seq-no", bookingInfo), 0.0);
                }
            }
        }
        return totalFarePerSegment;
    }

    private double getTotal(int selectNodeIntValue) {
        return getBookingAmount(selectNodeIntValue);
    }

    //Todo: to implement fetching of booking amount
    public double getBookingAmount(int selectNodeIntValue) {
        Double totalFare = 0.0D;
        Document document = DocumentReaderUtils.readDocumentForStep(stepResponseBodyMap, PREPAYMENT);
        Node documentElement = document.getDocumentElement();
        Node airBookingNode = DOMHelper.selectNode("air-bookings/air-booking", documentElement);
        for (Node bookingInfoList : DOMHelper.selectNodes("pricing-info-list", airBookingNode)) {
            for (Node bookingInfo : DOMHelper.selectNodes("pricing-info", bookingInfoList)) {
                if (DOMHelper.selectNodeIntValue("seq-no", bookingInfo) == selectNodeIntValue) {
                    Node pp = DOMHelper.selectNode("cost-pricing-object/pricing-elements", bookingInfo);
                    for (Node extRef : DOMHelper.selectNodes("pricing-element", pp)) {
                        String category = DOMHelper.selectNodeValue("category", extRef);
                        for (AllowedTax value : AllowedTax.values()) {
                            if (category.equalsIgnoreCase(value.getDisplayName())) {
                                totalFare += DOMHelper.selectNodeDoubleValue("amount", extRef);
                            }
                        }

                    }
                }
            }
        }

        return totalFare;
    }

    public LinkedHashMap<SegmentKeyMap, FlightSegment> makeFlightSegments() {
        LinkedHashMap<SegmentKeyMap, FlightSegment> map = new LinkedHashMap<>();
        int flightCount = 0;
        int segmentCount = 0;
        int index = 1;
        String tripType = "OW";

        Document document = DocumentReaderUtils.readDocumentForStep(stepResponseBodyMap, PREPAYMENT);
        Node documentElement = document.getDocumentElement();
        Node airBookingNode = DOMHelper.selectNode("air-bookings/air-booking", documentElement);
        for (Node flights : DOMHelper.selectNodes("flights/flight", airBookingNode)) {
            flightCount += 1;
            if (flightCount == 2) {
                segmentCount = 0;
                tripType = "RT";
            }
            Node ac = DOMHelper.selectNode("segments", flights);
            for (Node pp : DOMHelper.selectNodes("segment", ac)) {
                segmentCount += 1;
                FlightSegment flightSegment = getFlightSegmentObject(pp);
                map.put(new SegmentKeyMap(tripType, DOMHelper.selectNodeValue("departure-airport", pp), DOMHelper.selectNodeValue("arrival-airport", pp), DOMHelper.selectNodeValue("supplier", pp), index, segmentCount), flightSegment);
                index += 1;
            }
        }
        return map;
    }

    private FlightSegment getFlightSegmentObject(Node pp) {
        Flight flight = new Flight(DOMHelper.selectNodeValue("flight-number", pp), DOMHelper.selectNodeValue("operating-airline", pp), null, null);
        ZonedDateTime departTime = DateUtil.getZonedDateTime(DateUtil.convertStringToDate(DOMHelper.selectNodeValue("departure-date-time", pp)));
        ZonedDateTime arrivalTime = DateUtil.getZonedDateTime(DateUtil.convertStringToDate(DOMHelper.selectNodeValue("arrival-date-time", pp)));
        DepartureDetails departureDetails = new DepartureDetails(new Airport(DOMHelper.selectNodeValue("departure-airport", pp)), departTime);
        ArrivalDetails arrivalDetails = new ArrivalDetails(new Airport(DOMHelper.selectNodeValue("arrival-airport", pp)), arrivalTime);
        Airline operatingAirline = new Airline(DOMHelper.selectNodeValue("operating-airline", pp), flight.getFlightNumber());
        Airline marketingAirline = new Airline(DOMHelper.selectNodeValue("marketing-airline", pp), flight.getFlightNumber());
        return new FlightSegment(flight, departureDetails, arrivalDetails, operatingAirline, marketingAirline, Optional.empty(), null);
    }

    public Set<String> getApplicableSuppliers() {

        Map<SegmentKeyMap, FlightSegment> segmentedMap = makeFlightSegments();
        Set<String> supplierLists = new HashSet<>();
        for (SegmentKeyMap s : segmentedMap.keySet()) {
            String airline = s.getSupplier();
            for (AirSupplier value : AirSupplier.values()) {
                if (value.getCarrierName().equalsIgnoreCase(airline)) {
                    supplierLists.add(value.getDisplayName());
                }
            }
        }
        return supplierLists;
    }

    public LinkedHashMap<String, List<String>> getMealAndBaggageDetails(LinkedHashMap<SegmentKeyMap, FlightSegment> segmentedMap, List<String> journeywiseBaggageEnabledSuppliers) {
        // supplier wise meal and baggage code
        LinkedHashMap<String, List<String>> mealAndBaggageInfo = new LinkedHashMap<>();
        updateMealDetails(segmentedMap, mealAndBaggageInfo);
        updateBaggageDetails(segmentedMap, journeywiseBaggageEnabledSuppliers, mealAndBaggageInfo);
        return mealAndBaggageInfo;
    }

    //TODO: fill baggage details fetching method
    private void updateBaggageDetails(LinkedHashMap<SegmentKeyMap, FlightSegment> segmentedMap, List<String> journeywiseBaggageEnabledSuppliers, LinkedHashMap<String, List<String>> mealAndBaggageInfo) {
        Map<BaggageInfoKey, List<String>> baggageInfoKeMap = new HashMap<>();
        Document document = DocumentReaderUtils.readDocumentForStep(stepResponseBodyMap, PREPAYMENT);
        for (SegmentKeyMap segmentKeyMap : segmentedMap.keySet()) {
            if (journeywiseBaggageEnabledSuppliers.contains(TestUtils.getSupplierNameFromCode(segmentKeyMap.getSupplier()))) {
                journeyWiseBaggage(baggageInfoKeMap, document, segmentKeyMap);
            } else {
                segmentWiseBaggage(baggageInfoKeMap, document, segmentKeyMap);
            }
        }
        for (Map.Entry<BaggageInfoKey, List<String>> baggageInfoKeyListEntry : baggageInfoKeMap.entrySet()) {
            if (mealAndBaggageInfo.containsKey(baggageInfoKeyListEntry.getKey().getSupplier())) {
                mealAndBaggageInfo.get(baggageInfoKeyListEntry.getKey().getSupplier()).addAll(baggageInfoKeyListEntry.getValue());
            }
        }
    }

    private void segmentWiseBaggage(Map<BaggageInfoKey, List<String>> baggageInfoKeMap, Document document, SegmentKeyMap segmentKeyMap) {
        Node documentElement = document.getDocumentElement();
        Node airBookingNode = DOMHelper.selectNode("air-bookings/air-booking", documentElement);
        for (Node bookingInfoList : DOMHelper.selectNodes("booking-info-list", airBookingNode)) {
            for (Node bookingInfo : DOMHelper.selectNodes("booking-info", bookingInfoList)) {
                if (StringUtils.isNotEmpty(DOMHelper.selectNodeValue("baggage-code", bookingInfo))) {
                    baggageInfoKeMap.put(new BaggageInfoKey(segmentKeyMap.getDirection(), TestUtils.getSupplierNameFromCode(segmentKeyMap.getSupplier()), DOMHelper.selectNodeIntValue("pax-info-seq-no", bookingInfo)), asList(DOMHelper.selectNodeValue("baggage-code", bookingInfo)));
                }
            }
        }
    }

    private void journeyWiseBaggage(Map<BaggageInfoKey, List<String>> baggageInfoKeMap, Document document, SegmentKeyMap segmentKeyMap) {
        int directionWiseIndex = segmentKeyMap.getIndex();
        Node documentElement = document.getDocumentElement();
        Node airBookingNode = DOMHelper.selectNode("air-bookings/air-booking", documentElement);
        for (Node bookingInfoList : DOMHelper.selectNodes("booking-info-list", airBookingNode)) {
            for (Node bookingInfo : DOMHelper.selectNodes("booking-info", bookingInfoList)) {
                if (DOMHelper.selectNodeIntValue("segment-seq-no", bookingInfo) == directionWiseIndex && StringUtils.isNotEmpty(DOMHelper.selectNodeValue("baggage-code", bookingInfo))) {
                    baggageInfoKeMap.put(new BaggageInfoKey(segmentKeyMap.getDirection(), TestUtils.getSupplierNameFromCode(segmentKeyMap.getSupplier()), DOMHelper.selectNodeIntValue("pax-info-seq-no", bookingInfo)), asList(DOMHelper.selectNodeValue("baggage-code", bookingInfo)));
                }
            }
        }
    }

    private void updateMealDetails(LinkedHashMap<SegmentKeyMap, FlightSegment> segmentedMap, LinkedHashMap<String, List<String>> mealAndBaggageInfo) {
        for (SegmentKeyMap segmentKeyMap : segmentedMap.keySet()) {
            if (mealAndBaggageInfo.containsKey(TestUtils.getSupplierNameFromCode(segmentKeyMap.getSupplier()))) {
                mealAndBaggageInfo.get(TestUtils.getSupplierNameFromCode(segmentKeyMap.getSupplier())).addAll(getMealInfo(segmentKeyMap.getIndex()));
            } else {
                List<String> mealAndBaggageCode = new ArrayList<>();
                mealAndBaggageCode.addAll(getMealInfo(segmentKeyMap.getIndex()));
                mealAndBaggageInfo.put(TestUtils.getSupplierNameFromCode(segmentKeyMap.getSupplier()), mealAndBaggageCode);
            }
        }
    }

    private List<String> getMealInfo(int index) {
        List<String> mealAndBaggageSegment = new ArrayList<>();
        Document document = DocumentReaderUtils.readDocumentForStep(stepResponseBodyMap, PREPAYMENT);
        Node documentElement = document.getDocumentElement();
        Node airBookingNode = DOMHelper.selectNode("air-bookings/air-booking", documentElement);
        for (Node bookingInfoList : DOMHelper.selectNodes("booking-info-list", airBookingNode)) {
            for (Node bookingInfo : DOMHelper.selectNodes("booking-info", bookingInfoList)) {
                if (DOMHelper.selectNodeIntValue("segment-seq-no", bookingInfo) == index) {
                    if (StringUtils.isNotEmpty(DOMHelper.selectNodeValue("meal-code", bookingInfo))) {
                        mealAndBaggageSegment.add(DOMHelper.selectNodeValue("meal-code", bookingInfo));
                    }
                }
            }
        }
        return mealAndBaggageSegment;
    }

    public CustomerData getCustomerData() {
        Document document = DocumentReaderUtils.readDocumentForStep(stepResponseBodyMap, PREPAYMENT);
        Node documentElement = document.getDocumentElement();
        Node contactDetail = DOMHelper.selectNode("contact-detail", documentElement);
        String firstName = DOMHelper.selectNodeValue("first-name", contactDetail);
        String lastName = DOMHelper.selectNodeValue("last-name", contactDetail);
        String email = DOMHelper.selectNodeValue("email", contactDetail);
        String mobile = DOMHelper.selectNodeValue("mobile", contactDetail);
        String title = DOMHelper.selectNodeValue("title", contactDetail).toUpperCase();
        return new CustomerData(email, firstName, lastName, mobile, title);
    }


    /*private static List<String> getFareKeyList() throws ParserConfigurationException, SAXException, IOException {
        Document document = DOMHelper.createDocument(xmlInput);
        Node documentElement = document.getDocumentElement();
        Node airBookingNode = DOMHelper.selectNode("air-bookings/air-booking", documentElement);
        List<String> fareKeys = new ArrayList<>();
        for (Node pricingInfoList : DOMHelper.selectNodes("pricing-info-list", airBookingNode)) {
            for (Node pricingInfo : DOMHelper.selectNodes("pricing-info", pricingInfoList)) {
                fareKeys.add(DOMHelper.selectNodeValue("fare-key", pricingInfo));
            }
        }
        return fareKeys;
    }*/

    public boolean isInternational() {
        stepResponseBodyMap.get(PREPAYMENT);
        return false;
    }

}
