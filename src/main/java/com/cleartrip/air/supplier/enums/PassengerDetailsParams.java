package com.cleartrip.air.supplier.enums;

public enum PassengerDetailsParams {
    FIRST_NAME("firstName"), LAST_NAME("lastName"), DOB("dob"), TRAVELLER_TYPE("travellerType"), PASSPORT("passport"), TITLE("title"), GENDER("gender"), PASSPORTNUMBER("passportNumber"), PASSPORTEXPIRYDATE("passportExpiryDate"), PASSPORTISSUECOUNTRY("passportIssuingCountry");

    String dispayName;

    PassengerDetailsParams(String name) {
        this.dispayName = name;
    }

    public String getDisplayName() {
        return this.dispayName;
    }
}
