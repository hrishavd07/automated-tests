package com.cleartrip.air.supplier.enums;

public enum ErrorCode {
    PARSING_ERROR(101), CONNECTION_ERROR(100);

    public int code;

    ErrorCode(int errorCode){
        this.code = errorCode;
    }

    public int getErrorValue(){
        return this.code;
    }
}
