package com.cleartrip.air.supplier.enums;

public enum Testcases {
    PASSENGER_DETAILS_VALIDATION("passenger validation"),
    BOOKING_AMOUNT_VALIDATION("Booking amount validation"),
    MEAL_AND_BAGGAGE_VALIDATION("meal and baggage validation"),
    CUSTOMER_DETAILS_VALIDATION("customer details validation"),
    FLIGHT_DETAILS_VALIDATION("Flight details validation");

    String message;

    Testcases(String message){
        this.message = message;
    }

    public String getDispalyName(){
        return this.message;
    }
}
