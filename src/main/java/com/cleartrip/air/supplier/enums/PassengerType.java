package com.cleartrip.air.supplier.enums;

import java.util.Arrays;

public enum PassengerType {
    INF("infant", "INF"), ADT("adult","ADT"), CHD("child", "CHD");

    String name;
    String pseudoName;

    PassengerType(String name, String pseudoName) {
        this.name = name;
        this.pseudoName = pseudoName;
    }

    public String getDisplayName() {
        return this.name;
    }

    public static PassengerType getPseudoValueOf(String name){
        return Arrays.stream(PassengerType.values())
                .filter(r -> r.pseudoName.equalsIgnoreCase(name))
                .findFirst().get();

    }

}
