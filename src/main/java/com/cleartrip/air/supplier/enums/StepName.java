package com.cleartrip.air.supplier.enums;

public enum StepName {
    PREPAYMENT("prepayment"), BOOK("book");

    String name;

    StepName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return this.name;
    }
}
