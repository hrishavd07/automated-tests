package com.cleartrip.air.supplier.enums;

public enum Gender {

    MALE("Male"), FEMALE("Female"), UNKNOWN("Unknown");

    private final String genderStr;

    Gender(String genderStr) {
        this.genderStr = genderStr;
    }

    public String getGenderStr() {
        return this.genderStr;
    }

    public static Gender getGender(String genderStr) {
        Gender gender = Gender.UNKNOWN;
        for(Gender gen : Gender.values()) {
            if(gen.genderStr.equalsIgnoreCase(genderStr)) {
                gender = gen;
                break;
            }
        }
        return gender;
    }
}