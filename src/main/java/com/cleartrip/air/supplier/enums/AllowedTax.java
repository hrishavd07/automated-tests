package com.cleartrip.air.supplier.enums;

public enum AllowedTax {

    TAX("TAX"), FEE("FEE"),BF("BF");

    private String displayName;

    AllowedTax(String supplier) {
        displayName = supplier;
    }

    public String getDisplayName() {
        return displayName;
    }
}