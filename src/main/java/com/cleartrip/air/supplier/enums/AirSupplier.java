package com.cleartrip.air.supplier.enums;

public enum AirSupplier {
    SPICEJET("SPICEJET", "SG"), INDIGO("INDIGO", "6E"),AIRASIA("AIR_ASIA","AIR_ASIA"), AMD("AMADEUS","AMD")
    ,RADIXX("RADIXX","G8"),GAL("GALILEO", "GAL");

    private String carrierName;
    private String displayName;

    AirSupplier(String displayName, String carrierName) {
        this.displayName =displayName;
        this.carrierName = carrierName;
    }
    public String getDisplayName(){
        return displayName;
    }

    public String getCarrierName() {
        return this.carrierName;
    }

}
