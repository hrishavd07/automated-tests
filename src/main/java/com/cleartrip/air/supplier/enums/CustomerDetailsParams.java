package com.cleartrip.air.supplier.enums;

public enum CustomerDetailsParams {
    FIRST_NAME("firstName"), LAST_NAME("lastName"), EMAIL_ID("emailId"),MOBILE_NO("mobileNumber"),TITLE("title");

    String dispayName;

    CustomerDetailsParams(String name){
        this.dispayName = name;
    }

    public String getDisplayName(){
        return this.dispayName;
    }

}
