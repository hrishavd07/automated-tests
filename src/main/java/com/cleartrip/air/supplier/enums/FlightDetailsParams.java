package com.cleartrip.air.supplier.enums;

public enum FlightDetailsParams {
    FLIGHT("Flight"), DEPARTURE_DETAILS("DepartureDetails"), ARRIVAL_DETAILS("ArrivalDetails"), AIRLINE("Airline"),
    FLIGHTNUMBER("flightNumber"), AIRLINECODE("airlineCode"), AIRPORTCODE("airportCode"), TERMINAL("terminal"),
    DEPART_TIME("departTime"), ARRIVAL_TIME("arrivalTime"), DURATION("duration");

    String dispayName;

    FlightDetailsParams(String name) {
        this.dispayName = name;
    }

    public String getDisplayName() {
        return this.dispayName;
    }


}
