package com.cleartrip.air.supplier.testframework;

import com.cleartrip.air.supplier.validatordata.PassengersDetailsData;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class TestResult {

    private final String name;

    private Status status;

    private String message;

    private Object expectedValue;

    private Object actualValue;

    private Optional<List<TestResult>> testResultList;


    public TestResult(String name, Status status, String message, Object expectedValue, Object actualValue, Optional<List<TestResult>> testResultList) {
        this.name = name;
        this.status = status;
        this.message = message;
        this.expectedValue = expectedValue;
        this.actualValue = actualValue;
        this.testResultList = testResultList;
    }

    public TestResult(String name, Status status, String message, Optional<List<TestResult>> testResultList){
        this.name = name;
        this.status = status;
        this.message = message;
        this.testResultList = testResultList;
    }

    public String getName() {
        return name;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Optional<List<TestResult>> getTestResultList() {
        return testResultList;
    }


    public static TestResult getTestResult(String name, Status failed, String message, List<PassengersDetailsData> ctPassengerDetails, Object value, Optional<List<TestResult>> empty) {
        return new TestResult(name, failed, message, ctPassengerDetails, value, empty);
    }

    public enum Status {
        SUCCESS, FAILED, SKIPPED, NOT_APPLICABLE
    }


    @Override
    public String toString() {
        return "TestResult{" +
                "name='" + name + '\'' +
                ", status=" + status +
                ", message='" + message + '\'' +
                ", expectedValue=" + expectedValue + '\'' +
                ", actualValue=" + actualValue + '\'' +
                ", testResultList=" + testResultList +
                '}';
    }
}
