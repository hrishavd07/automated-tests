package com.cleartrip.air.supplier.testframework.ignorableparamfactory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class RadixxIgnorableParameterFactory extends NavitaireIgnorableParameterFactory {

    public static Supplier <List<String>> getRadixxCustomerDetailsIgnorableParams() {
        return () -> new ArrayList<>();
    }
}
