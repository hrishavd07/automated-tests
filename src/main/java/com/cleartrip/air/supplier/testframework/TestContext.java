package com.cleartrip.air.supplier.testframework;

import java.util.HashMap;
import java.util.Map;

public class TestContext {

    private Map<String,Object> contextMap;

    public TestContext() {
        this.contextMap = new HashMap<>();
    }

    public void addAttribute(String key, Object value) {
        this.contextMap.put(key,value);
    }

    public Object getValue(String key) {
        return contextMap.get(key);
    }
}
