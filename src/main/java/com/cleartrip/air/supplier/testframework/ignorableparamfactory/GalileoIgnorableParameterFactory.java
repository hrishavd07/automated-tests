package com.cleartrip.air.supplier.testframework.ignorableparamfactory;

import com.cleartrip.air.supplier.enums.CustomerDetailsParams;
import com.cleartrip.air.supplier.enums.PassengerDetailsParams;
import com.cleartrip.air.supplier.enums.PassengerType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class GalileoIgnorableParameterFactory {


    public static Supplier<List<String>> getGalileoCustomerDetailsIgnorableParams() {
        return () -> getGalileoCustomerIgnorableParamsList();
    }

    private static List<String> getGalileoCustomerIgnorableParamsList() {
        List<String> ignorableParams = new ArrayList<>();
        ignorableParams.add(CustomerDetailsParams.FIRST_NAME.getDisplayName());
        ignorableParams.add(CustomerDetailsParams.LAST_NAME.getDisplayName());
        ignorableParams.add(CustomerDetailsParams.TITLE.getDisplayName());
        return ignorableParams;
    }

    public static Function<PassengerType, List<String>> getGalileoPassengerDetailsIgnorableParams() {
        Map<PassengerType, List<String>> passengerWiseIgnorableParams = getGalileoPassengerWiseIgnorableParams();
        return passengerType -> passengerWiseIgnorableParams.get(passengerType);
    }

    private static Map<PassengerType, List<String>> getGalileoPassengerWiseIgnorableParams() {
        Map<PassengerType, List<String>> passengerWiseIgnorableParams = new HashMap<>();
        passengerWiseIgnorableParams.put(PassengerType.ADT, getAdultIgnorableParams());
        return passengerWiseIgnorableParams;
    }

    private static List<String> getAdultIgnorableParams() {
        List<String> ignorableParams = new ArrayList<>();
        ignorableParams.add(PassengerDetailsParams.DOB.getDisplayName());
        ignorableParams.add(PassengerDetailsParams.PASSPORT.getDisplayName());
        return ignorableParams;
    }

}
