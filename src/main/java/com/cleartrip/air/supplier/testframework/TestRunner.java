package com.cleartrip.air.supplier.testframework;

import com.cleartrip.air.supplier.testcases.TestCase;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.cleartrip.air.supplier.testframework.TestResult.Status.SKIPPED;

public class TestRunner {

    private final TestConfigurationFactory testConfigurationFactory;
    private final String tripID ;
    Logger LOGGER = LogManager.getLogger(TestRunner.class);

    public TestRunner(TestConfigurationFactory testConfigurationFactory, String tripID) {
        this.testConfigurationFactory = testConfigurationFactory;
        this.tripID = tripID;
    }

    public List<TestResult> run() {
        List<TestCase> testCases = testConfigurationFactory.prepareTestCase(tripID);
        List<TestResult> testResults = new ArrayList<>();

        if(testCases.isEmpty()){
            testResults.add(new TestResult(StringUtils.EMPTY, SKIPPED,
                    "No testcases available for the given set of suppliers",
                    Optional.empty()));
            return  testResults;
        }
        TestContext testContext = testConfigurationFactory.prepareTextContext(tripID);
        for (TestCase testCase: testCases) {
            testResults.add(testCase.run(testContext));
        }
        return testResults;
    }
}
