package com.cleartrip.air.supplier.testframework;

import com.cleartrip.air.supplier.Ctxmlreader;
import com.cleartrip.air.supplier.StatsResponse;
import com.cleartrip.air.supplier.dataextractors.factory.SupplierDataExtractorsFactory;
import com.cleartrip.air.supplier.dataextractors.factory.airAsia.AirAsiaDataExtractorFactory;
import com.cleartrip.air.supplier.dataextractors.factory.amadeus.AmadeusDataExtractoryFactory;
import com.cleartrip.air.supplier.dataextractors.factory.galileo.GalileoDataExtractorFactory;
import com.cleartrip.air.supplier.dataextractors.factory.navitaire.NavitaireDataExtractorFactory;
import com.cleartrip.air.supplier.enums.AirSupplier;
import com.cleartrip.air.supplier.enums.PassengerDetailsParams;
import com.cleartrip.air.supplier.enums.Testcases;
import com.cleartrip.air.supplier.testcases.*;
import com.cleartrip.air.supplier.testframework.ignorableparamfactory.IgnorableParameterFactory;
import statsreader.StatsApiReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SupplierTestConfigurationFactory implements TestConfigurationFactory {

    private StatsApiReader statsApiReader;
    private DataExtractorsFactoryRegistry dataExtractorsFactoryRegistry;

    public SupplierTestConfigurationFactory(StatsApiReader statsApiReader) {
        this.statsApiReader = statsApiReader;
        this.dataExtractorsFactoryRegistry = new DataExtractorsFactoryRegistry(getSupplierDataExtractors());
    }



    private List<String> getAmdIgnorableParams() {
        List<String> ignorableParams = new ArrayList<>();
        ignorableParams.add(PassengerDetailsParams.DOB.getDisplayName());
        return ignorableParams;
    }

    private Map<String, SupplierDataExtractorsFactory> getSupplierDataExtractors() {
        Map<String, SupplierDataExtractorsFactory> map = new HashMap<>();
        map.put("indigo", new NavitaireDataExtractorFactory());
        map.put("spicejet", new NavitaireDataExtractorFactory());
        map.put("air_asia", new AirAsiaDataExtractorFactory());
        map.put("radixx", new NavitaireDataExtractorFactory());
        map.put("amadeus", new AmadeusDataExtractoryFactory());
        map.put("galileo", new GalileoDataExtractorFactory());
        return map;
    }


    @Override
    public List<TestCase> prepareTestCase(String tripId) {
        List<TestCase> testCaseList = new ArrayList<>();
        Ctxmlreader CTXmlReader = fetchTripXmlReaderObject(tripId);
        Map<String, SupplierDataExtractorsFactory> supplierDataExtractors = getSupplierDataExtractors();

        if (CTXmlReader.getApplicableSuppliers().size() == 0) {
            return testCaseList;
        }
        for (String supp : CTXmlReader.getApplicableSuppliers()) {
            if (!supplierDataExtractors.containsKey(supp.toLowerCase())) {
                return testCaseList;
            }
        }
        testCaseList.add(
                new PassengerValidationTestCase(Testcases.PASSENGER_DETAILS_VALIDATION.getDispalyName(),
                        dataExtractorsFactoryRegistry,
                        CTXmlReader,
                        IgnorableParameterFactory.getSupplierPassengerIgnorableParams()));
        testCaseList.add(
                new MealAndBaggageValidationTestCase(Testcases.MEAL_AND_BAGGAGE_VALIDATION.getDispalyName(),
                        dataExtractorsFactoryRegistry,
                        CTXmlReader, fetchJourneywiseBaggageEnabledSupplier()));
        testCaseList.add(
                new FlightDetailsValidationTestCase(Testcases.FLIGHT_DETAILS_VALIDATION.getDispalyName(),
                        dataExtractorsFactoryRegistry,
                        CTXmlReader
                ));
        testCaseList.add(
                new BookingAmountValidationTestCase(Testcases.BOOKING_AMOUNT_VALIDATION.getDispalyName(),
                        dataExtractorsFactoryRegistry,
                        CTXmlReader));
        testCaseList.add(
                new CustomerDataValidationTestCase(Testcases.CUSTOMER_DETAILS_VALIDATION.getDispalyName(),
                        dataExtractorsFactoryRegistry,
                        CTXmlReader,
                        IgnorableParameterFactory.getSupplierCustomerDetailsIgnorableParams()));
        return testCaseList;
    }

    private List<String> fetchJourneywiseBaggageEnabledSupplier() {
        List<String> journeywiseBaggageEnabledSuppliers = new ArrayList<>();
        journeywiseBaggageEnabledSuppliers.add(AirSupplier.INDIGO.getDisplayName());
        return journeywiseBaggageEnabledSuppliers;
    }

    private Ctxmlreader fetchTripXmlReaderObject(String tripId) {
        TestContext testContext = prepareTextContext(tripId);
        return (new Ctxmlreader(testContext));
    }

    @Override
    public TestContext prepareTextContext(String tripId) {
        try {

            String json = statsApiReader.readStatsHeadAsRaw(tripId);
            TestContext testContext = new TestContext();
            testContext.addAttribute("tripId", tripId);
            StatsResponse statsResponse = new StatsResponse(json);
            testContext.addAttribute("statsObject", statsResponse);
            testContext.addAttribute("itineraryId", fetchItineraryId(statsResponse));
            Map<String, Integer> noOfPaxMap = noOfPax(statsResponse);
            SearchCriteria searchCriteria = new SearchCriteria(isInternational(statsResponse), noOfPaxMap.get("adt"), noOfPaxMap.get("chd"), noOfPaxMap.get("inf"));
            testContext.addAttribute("searchCriteria", searchCriteria);
            return testContext;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Map<String, Integer> noOfPax(StatsResponse statsResponse) {
        Map<String, Integer> response = statsResponse.getPaxCount();
        return response;
    }

    private boolean isInternational(StatsResponse statsResponse) {
        Map<String, String> response = statsResponse.getAirPrepaymentCallInAirCtCalls();
        return Boolean.valueOf(response.get("intl") !=null ? response.get("intl") : "false");
    }

    private String fetchItineraryId(StatsResponse json) {
        return json.getJsonNode("air_api_calls").get(0).findValue("id").textValue();
    }
}
