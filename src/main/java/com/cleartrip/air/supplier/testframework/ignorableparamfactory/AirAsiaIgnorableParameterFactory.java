package com.cleartrip.air.supplier.testframework.ignorableparamfactory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class AirAsiaIgnorableParameterFactory {

    public static Supplier< List<String>> getAirAsiaCustomerDetailsIgnorableParams() {
        return () -> new ArrayList<>();
    }
}
