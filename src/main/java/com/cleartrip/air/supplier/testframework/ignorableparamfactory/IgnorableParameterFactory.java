package com.cleartrip.air.supplier.testframework.ignorableparamfactory;

import com.cleartrip.air.supplier.enums.CustomerDetailsParams;
import com.cleartrip.air.supplier.enums.PassengerDetailsParams;
import com.cleartrip.air.supplier.enums.PassengerType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class IgnorableParameterFactory {


    public static Map<String, Supplier<List<String>>> getSupplierCustomerDetailsIgnorableParams() {

        /* For each supplier we are putting a 'supplier'(functional Interface) to fetch the list of
        ignorable params. */
        Map<String, Supplier< List<String>>> supplierBasedParams = new HashMap<>();
        supplierBasedParams.put("indigo", IndigoIgnorableParameterFactory.getIndigoCustomerDetailsIgnorableParams());
        supplierBasedParams.put("galileo", GalileoIgnorableParameterFactory.getGalileoCustomerDetailsIgnorableParams());
        supplierBasedParams.put("radixx", RadixxIgnorableParameterFactory.getRadixxCustomerDetailsIgnorableParams());
        supplierBasedParams.put("spicejet", SpicejetIgnorableParameterFactory.getSpiceJetCustomerDetailsIgnorableParams());
        supplierBasedParams.put("amadeus", AmadeusIgnorableParameterFactory.getAmadeusCustomerDetailsIgnorableParams());
        supplierBasedParams.put("air_asia", AirAsiaIgnorableParameterFactory.getAirAsiaCustomerDetailsIgnorableParams());
        return supplierBasedParams;
    }

    public static Map<String, Function<PassengerType, List<String>>> getSupplierPassengerIgnorableParams() {
        /* For each supplier we are putting a 'function'(functional Interface) where the input will be passenger type,
         to fetch the list of
         ignorable params for the perticular passenger type. */
        Map<String, Function<PassengerType, List<String>>> supplierBasedParams = new HashMap<>();
        supplierBasedParams.put("indigo", IndigoIgnorableParameterFactory.getNavitairePassengerDetailsIgnorableParams());
        supplierBasedParams.put("spicejet", SpicejetIgnorableParameterFactory.getNavitairePassengerDetailsIgnorableParams());
        supplierBasedParams.put("radixx", RadixxIgnorableParameterFactory.getNavitairePassengerDetailsIgnorableParams());
        supplierBasedParams.put("galileo", GalileoIgnorableParameterFactory.getGalileoPassengerDetailsIgnorableParams());
        supplierBasedParams.put("amadeus", AmadeusIgnorableParameterFactory.getAmadeusPassengerDetailsIgnorableParams());
        return supplierBasedParams;
    }


}
