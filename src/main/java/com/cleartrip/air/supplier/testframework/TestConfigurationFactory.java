package com.cleartrip.air.supplier.testframework;

import com.cleartrip.air.supplier.testcases.TestCase;

import java.util.List;

public interface TestConfigurationFactory {

    public List<TestCase> prepareTestCase(String tripID);

    public TestContext prepareTextContext(String stringObjectMap);
}
