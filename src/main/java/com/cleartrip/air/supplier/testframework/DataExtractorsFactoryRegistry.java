package com.cleartrip.air.supplier.testframework;

import com.cleartrip.air.supplier.dataextractors.factory.SupplierDataExtractorsFactory;

import java.util.Map;

public class DataExtractorsFactoryRegistry {

    private Map<String, SupplierDataExtractorsFactory> supplierDataExtractorsFactoryMap;

    public DataExtractorsFactoryRegistry(Map<String, SupplierDataExtractorsFactory> supplierDataExtractorsFactoryMap) {
        this.supplierDataExtractorsFactoryMap = supplierDataExtractorsFactoryMap;
    }

    public SupplierDataExtractorsFactory getFactory(String supplier) {
        return supplierDataExtractorsFactoryMap.get(supplier.toLowerCase());
    }

}
