package com.cleartrip.air.supplier.testframework.ignorableparamfactory;

import com.cleartrip.air.supplier.enums.CustomerDetailsParams;
import com.cleartrip.air.supplier.enums.PassengerDetailsParams;
import com.cleartrip.air.supplier.enums.PassengerType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class AmadeusIgnorableParameterFactory {


    public static Supplier<List<String>> getAmadeusCustomerDetailsIgnorableParams() {
        return () -> getAmadeusCustomerIgnorableParamsList();
    }

    public static Function<PassengerType, List<String>> getAmadeusPassengerDetailsIgnorableParams() {
        Map<PassengerType, List<String>> passengerWiseIgnorableParams = getAmadeusPassengerWiseIgnorableParams();
        return passengerType -> passengerWiseIgnorableParams.get(passengerType);
    }

    private static Map<PassengerType, List<String>> getAmadeusPassengerWiseIgnorableParams() {
        Map<PassengerType, List<String>> passengerWiseIgnorableParams = new HashMap<>();
        passengerWiseIgnorableParams.put(PassengerType.ADT, getAdultIgnorableParams());
        return passengerWiseIgnorableParams;
    }

    private static List<String> getAdultIgnorableParams() {
        List<String> ignorableParams = new ArrayList<>();
        ignorableParams.add(PassengerDetailsParams.TRAVELLER_TYPE.getDisplayName());
        ignorableParams.add(PassengerDetailsParams.PASSPORT.getDisplayName());
        ignorableParams.add(PassengerDetailsParams.TITLE.getDisplayName());
        return ignorableParams;
    }

    private static List<String> getAmadeusPassengersIgnorableParamList() {
        List<String> ignorableParams = new ArrayList<>();
        ignorableParams.add(PassengerDetailsParams.TRAVELLER_TYPE.getDisplayName());
        return ignorableParams;
    }

    private static List<String> getAmadeusCustomerIgnorableParamsList() {
        List<String> ignorableParams = new ArrayList<>();
        ignorableParams.add(CustomerDetailsParams.FIRST_NAME.getDisplayName());
        ignorableParams.add(CustomerDetailsParams.LAST_NAME.getDisplayName());
        ignorableParams.add(CustomerDetailsParams.MOBILE_NO.getDisplayName());
        ignorableParams.add(CustomerDetailsParams.TITLE.getDisplayName());

        return ignorableParams;
    }

}
