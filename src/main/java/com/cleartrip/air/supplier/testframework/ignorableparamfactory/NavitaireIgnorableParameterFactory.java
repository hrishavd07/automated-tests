package com.cleartrip.air.supplier.testframework.ignorableparamfactory;

import com.cleartrip.air.supplier.enums.PassengerDetailsParams;
import com.cleartrip.air.supplier.enums.PassengerType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

public class NavitaireIgnorableParameterFactory {

    public static Function<PassengerType, List<String>> getNavitairePassengerDetailsIgnorableParams() {
        Map<PassengerType, List<String>> passengerWiseIgnorableParams = getNavitairePassengerWiseIgnorableParams();
        return passengerType -> passengerWiseIgnorableParams.get(passengerType);
    }

    private static Map<PassengerType, List<String>> getNavitairePassengerWiseIgnorableParams() {
        Map<PassengerType, List<String>> passengerWiseIgnorableParams = new HashMap<>();
        passengerWiseIgnorableParams.put(PassengerType.ADT, getAdultIgnorableParams());
        return passengerWiseIgnorableParams;
    }

    private static List<String> getAdultIgnorableParams() {
        List<String> ignorableParams = new ArrayList<>();
        ignorableParams.add(PassengerDetailsParams.DOB.getDisplayName());
        ignorableParams.add(PassengerDetailsParams.PASSPORT.getDisplayName());
        return ignorableParams;
    }
}
