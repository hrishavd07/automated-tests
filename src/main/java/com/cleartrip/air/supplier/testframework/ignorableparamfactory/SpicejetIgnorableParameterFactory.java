package com.cleartrip.air.supplier.testframework.ignorableparamfactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class SpicejetIgnorableParameterFactory extends NavitaireIgnorableParameterFactory {

    public static Supplier <List<String>> getSpiceJetCustomerDetailsIgnorableParams() {
        return () -> new ArrayList<>();
    }
}
