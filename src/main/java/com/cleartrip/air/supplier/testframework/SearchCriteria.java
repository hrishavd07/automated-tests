package com.cleartrip.air.supplier.testframework;

public class SearchCriteria {

    boolean isInternational;
    int adultCount;
    int chdCount;
    int infCount;

    public boolean isInternational() {
        return isInternational;
    }

    public int getAdultCount() {
        return adultCount;
    }

    public int getChdCount() {
        return chdCount;
    }

    public int getInfCount() {
        return infCount;
    }

    public SearchCriteria(boolean isInternational, int adultCount, int chdCount, int infCount) {
        this.isInternational = isInternational;
        this.adultCount = adultCount;
        this.chdCount = chdCount;
        this.infCount = infCount;
    }
}
