package com.cleartrip.air.supplier.testframework.ignorableparamfactory;

import com.cleartrip.air.supplier.enums.CustomerDetailsParams;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class IndigoIgnorableParameterFactory extends NavitaireIgnorableParameterFactory {

    public static Supplier <List<String>> getIndigoCustomerDetailsIgnorableParams() {
        return () -> getIndigoCustomerIgnorableParamsList();
    }

    private static List<String> getIndigoCustomerIgnorableParamsList() {
        List<String> ignorableParams = new ArrayList<>();
        ignorableParams.add(CustomerDetailsParams.FIRST_NAME.getDisplayName());
        ignorableParams.add(CustomerDetailsParams.LAST_NAME.getDisplayName());
        ignorableParams.add(CustomerDetailsParams.MOBILE_NO.getDisplayName());
        ignorableParams.add(CustomerDetailsParams.TITLE.getDisplayName());

        return ignorableParams;
    }
}
