package com.cleartrip.air.supplier;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class StatsResponse {

    private final String statsJson;
    private final JsonNode jsonNode;

    public StatsResponse(String statsJson) throws IOException {
        this.statsJson = statsJson;
        this.jsonNode = parseJson(statsJson);
    }

    private JsonNode parseJson(String statsJson) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(statsJson);
        return jsonNode;
    }


    public Map<String, String> getAirPrepaymentCallInAirCtCalls() {
        return getCallDetails("air_ct_calls",
                "AIR_PREPAYMENT",
                "tid",
                "itinerary",
                "intl");
    }

    public Map<String, String> getAirBookCallInAirCTCalls(){
        return getCallDetails("air_ct_calls",
                "AIR_BOOK",
                "tid",
                "itinerary",
                "intl");


    }

    public Map<String, String> getCallDetails(String callValue, String eventValue, String tidValue, String itineraryValue, String intl){
        Map<String, String> map = new HashMap<>();
        JsonNode air_ct_calls = getJsonNode(callValue);
        for (JsonNode node : air_ct_calls) {
            if (node.findValue("event").textValue().equals(eventValue)) {
                map.put("tid", node.findValue(tidValue).textValue());
                map.put("itinerary", node.findValue(itineraryValue).textValue());
                map.put("intl", node.findValue(intl).asText());
            }
        }
        return map;
    }

    public JsonNode getJsonNode(String callValue) {
        return jsonNode.findValue(callValue);
    }

    public Map<String, HashSet<String>> getApiCallDetails(JsonNode air_api_calls, String event, String api, String itineraryId){
        Map<String, HashSet<String>> map = new HashMap<>();
        for (JsonNode node : air_api_calls) {
            if ((node.findValue("event").textValue().equalsIgnoreCase(event)) && (node.findValue("api").textValue().equalsIgnoreCase(api))) {
                if(map.containsKey("tid"+"_"+node.findValue("supplier").textValue())){
                    map.get("tid"+"_"+node.findValue("supplier").textValue()).add( node.findValue("tid").textValue());
                }
                else{
                    HashSet<String> tidSet = new HashSet<>();
                    tidSet.add(node.findValue("tid").textValue());
                    map.put("tid"+"_"+node.findValue("supplier").textValue(), tidSet);
                }
            }
            if(StringUtils.isEmpty(itineraryId)){
                itineraryId = node.findValue("id").textValue();
            }
        }
        return map;
    }

    public Map<String, String> getAirSeatSellOneCallInAirCtCalls() {
        return getCallDetails("air_ct_calls",
                "AIR_SEATSELL1",
                "tid",
                "itinerary",
                "intl");
    }

    public Map<String,Integer> getPaxCount(){
        return getpaxDetails("air_ct_calls",
                "AIR_PREPAYMENT");
    }

    private Map<String, Integer> getpaxDetails(String callValue, String event) {
        Map<String, Integer> map = new HashMap<>();
        JsonNode air_ct_calls = getJsonNode(callValue);
        for (JsonNode node : air_ct_calls) {
            if(node.findValue("event").textValue().equalsIgnoreCase(event)){
                map.put("adt",node.findValue("adt").intValue());
                map.put("chd",node.findValue("chd").intValue());
                map.put("inf",node.findValue("inf").intValue());
            }
        }
    return  map;
    }

}

