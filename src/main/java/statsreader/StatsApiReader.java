package statsreader;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import httpreqreshandler.HttpReqResHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class StatsApiReader {
    private final HttpReqResHandler httpReqResHandler;
    private ObjectMapper mapper;

    public StatsApiReader() {
        httpReqResHandler = new HttpReqResHandler();
        mapper = new ObjectMapper();
    }

    public String readStatsHeadAsRaw(String tripID) {
        return httpReqResHandler.getResponseBody(tripID);
    }

    public JsonNode readStatsHead(String tripID) throws IOException {
        String body = httpReqResHandler.getResponseBody(tripID);
        return mapper.readTree(body);
    }

    public void readSteps(String itineraryId, String eventName, String colomnName, String tID) {
        String body = httpReqResHandler.getResponseBody(itineraryId, eventName, colomnName, tID);
        System.out.println(body);
    }
}
