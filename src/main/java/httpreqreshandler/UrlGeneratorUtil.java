package httpreqreshandler;

public class UrlGeneratorUtil {
    private static final String baseURL = "http://172.17.12.171:9080/summary/bookStats?tripId=";
    private static final String baseStepURL = "http://172.17.12.171/showbookresult.php?itineraryId=";
    private static final String EVENT = "&event=";
    private static final String COLUMN = "&column=";
    private static final String TID = "&tid=";

    public static String getHeadStatsURL(String tripID){
        return baseURL + tripID;
    }

    public static String getEventBasedStatsURL(String itineraryId, String eventName, String columnName, String tID) {
        return baseStepURL + itineraryId + EVENT + eventName + COLUMN + columnName + TID + tID;
    }
}
