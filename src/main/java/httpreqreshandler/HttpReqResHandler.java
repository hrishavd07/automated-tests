package httpreqreshandler;

import httprequestsender.HttpRequestSender;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpReqResHandler {
    private final HttpRequestSender httpRequestSender;
    private final ResponseHandler<String> responseHandler;
    private static final String USERNAME = "aa";
    private static final String PASSWORD = "aa";
    private static final Logger LOGGER = LogManager.getLogger(HttpReqResHandler.class);

    public HttpReqResHandler() {
        httpRequestSender = new HttpRequestSender();
        responseHandler = new BasicResponseHandler();
    }

    public String getResponseBody(String tripID) {
        CloseableHttpResponse httpResponse = httpRequestSender.makeHttpCall(UrlGeneratorUtil.getHeadStatsURL(tripID));
        String body;
        try {
            body = responseHandler.handleResponse(httpResponse);
        } catch (Exception e) {
            LOGGER.error("error while handling response of stats head", e);
            throw new RuntimeException(e);
        }
        return body;
    }

    public String getResponseBody(String itineraryId, String eventName, String columnName, String tID) {
        CloseableHttpResponse httpResponse = httpRequestSender.makeHttpCallWithUserNamePassword(
                UrlGeneratorUtil.getEventBasedStatsURL(itineraryId, eventName, columnName, tID),
                USERNAME,
                PASSWORD);
        String body;
        try {
            body = responseHandler.handleResponse(httpResponse);
        } catch (Exception e) {
            LOGGER.error("error while handling response of stats head", e);
            throw new RuntimeException(e);
        }
        return body;
    }
}
